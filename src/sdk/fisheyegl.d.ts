// 首先定义 FisheyeGlOptions 和 Distorter 类型，这部分与之前相同
interface FisheyeGlOptions {
  fov: {
    x: number;
    y: number;
  };
  lens: {
    a: number;
    b: number;
    Fx: number;
    Fy: number;
    scale: number;
  };
  selector: string;
  video: string;
}

interface Distorter {
  options: FisheyeGlOptions;
  gl: any; // 根据实际情况替换 any
  lens: {
    a: number;
    b: number;
    Fx: number;
    Fy: number;
    scale: number;
  };
  run: () => void;
  setImage: (image: any) => void; // 根据实际情况替换 any
}

// 修改 FisheyeGl 的声明方式，确保 TypeScript 将其视为一个函数
declare const FisheyeGl: (options: FisheyeGlOptions) => Distorter;

export { FisheyeGlOptions, Distorter, FisheyeGl };
