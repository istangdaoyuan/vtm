var Ye = Object.defineProperty;
var qe = (a, e, s) => e in a ? Ye(a, e, { enumerable: !0, configurable: !0, writable: !0, value: s }) : a[e] = s;
var Ne = (a, e, s) => (qe(a, typeof e != "symbol" ? e + "" : e, s), s);
import { ref as ve, reactive as He, defineComponent as Oe, watch as Pe, computed as ge, withDirectives as Ue, openBlock as ze, createElementBlock as $e, createElementVNode as je, normalizeStyle as Ke, vShow as Be, onMounted as Je, onUnmounted as De, createVNode as Qe, unref as Ze, createBlock as et } from "vue";
function tt(a, e) {
  const s = ve(""), h = ve("#");
  let p = null;
  return { rtcMediaPlayer: e, sessionId: s, simulatorUrl: h, startPlay: () => (p && p.close(), p = new SrsRtcPlayerAsync(), e.value && (e.value.srcObject = p.stream), p.play(a).then((l) => {
    s.value = l.sessionid, h.value = `${l.simulator}?drop=1&username=${l.sessionid}`;
  }).catch((l) => {
    p.close(), e.value && (e.value.style.display = "none"), console.log("链接失败"), console.error(l);
  })), stopPlay: () => {
    p && p.close();
  } };
}
const rt = `
  attribute vec2 a_position;
  attribute vec2 a_texCoord;
  varying vec2 v_texCoord;
  uniform vec2 u_scale;
  void main(void) {
      gl_Position = vec4(a_position, 0.0, 1.0);
      v_texCoord = a_texCoord;
  }
    `, at = `
  precision highp float;
  // 传入的纹理坐标和采样的纹理
  uniform sampler2D u_image;
  varying highp vec2 v_texCoord;
  uniform vec2 u_pixelSize;

  float gaussian(float x, float sigma) {
    return exp(-(x * x) / (2.0 * sigma * sigma));
  }

  vec3 checkAndReplaceGreen(vec3 color) {
    if (color.r < 0.6 && color.g > 0.6 && color.b < 0.6) {
        return vec3(0.0, 0.0, 0.0); // 替换为黑色
    }
    return color;
}

// 辅助函数：计算边缘强度
float edgeStrength(vec3 color, vec3 sampleColor) {
  return smoothstep(0.0, 1.0, distance(color, sampleColor));
}

vec3 rgbToHsl(vec3 color) {
float maxColor = max(max(color.r, color.g), color.b);
float minColor = min(min(color.r, color.g), color.b);
float delta = maxColor - minColor;
float h, s, l;
l = (maxColor + minColor) / 2.0;

if (delta == 0.0) {
    h = 0.0;
    s = 0.0;
} else {
    s = (l < 0.5) ? (delta / (maxColor + minColor)) : (delta / (2.0 - maxColor - minColor));
    if (color.r == maxColor) {
        h = (color.g - color.b) / delta + (color.g < color.b ? 6.0 : 0.0);
    } else if (color.g == maxColor) {
        h = (color.b - color.r) / delta + 2.0;
    } else {
        h = (color.r - color.g) / delta + 4.0;
    }
    h /= 6.0;
}
return vec3(h, s, l);
}

  void main() {
    // 从纹理中获取颜色
    vec4 color = texture2D(u_image, v_texCoord);
    // 定义绿色的颜色向量
    vec3 greenScreenColor = vec3(0.0, 1.0, 0.0);
    // 计算当前颜色与绿幕颜色的差异
    float colorDifference = distance(color.rgb, greenScreenColor);
    // 设置羽化效果的阈值和范围
    float threshold = 0.0001; // 阈值，越小越敏感
    float thresholdGaosi = 0.6; // 阈值，越小越敏感
    float featherRange = 0.75; // 羽化范围

    // vec3 modifiedColor = checkAndReplaceGreen(color.rgb);

    // 模糊效果
    vec3 blurColor = vec3(0.0);
    float total = 0.0;
    float edgeTotal = 0.0;
     // 定义绿色的色相范围
    float greenHueStart = 0.32;
    float greenHueEnd = 0.34;
    // for (float x = -1.0; x <= 1.0; x++) {
    //     for (float y = -1.0; y <= 1.0; y++) {
    //         vec2 samplePos = v_texCoord + vec2(x, y) * u_pixelSize;
    //         vec4 sampleColor = texture2D(u_image, samplePos);
    //         vec3 hslSampleColor = rgbToHsl(sampleColor.rgb);
    //         vec3 adjustedSampleColor = checkAndReplaceGreen(sampleColor.rgb);

    //         // 计算边缘强度并累加
    //         float edge = edgeStrength(modifiedColor, adjustedSampleColor);
    //         edgeTotal += edge;

    //         // 加权平均
    //         blurColor += adjustedSampleColor * edge;
    //         total += edge;
    //     }
    // }

    // if (total > 0.0) {
    //     blurColor /= total;
    // } else {
    //     blurColor = modifiedColor;
    // }

    vec3 hsl = rgbToHsl(color.rgb);
    // 检查颜色是否为绿色
    // if (hsl.x >= greenHueStart && hsl.x <= greenHueEnd) {
    if (color.r < 0.6 && color.g > 0.6 && color.b < 0.6) {
        // 将 alpha 设置为 0
        gl_FragColor = vec4(0.0,0.0,0.0, 0.0);
    } else {
      if (colorDifference < (threshold + featherRange)) {
        // 判断当前像素是否在羽化范围内
          // 在羽化范围内，逐渐调整alpha值
          float alpha = (colorDifference - threshold) / featherRange;
          alpha = clamp(alpha, 0.0, 1.0); // 确保alpha值在0到1之间
          gl_FragColor = vec4(0.0,0.0,0.0, 0.5);
      } else {
      // 检测高反差区域
        // if (distance(color, vec4(blurColor, 1.0)) > thresholdGaosi) {
        //     // gl_FragColor = blurredColor; // 在高反差区域应用高斯滤波
        //     // gl_FragColor = vec4(blurColor, 1.0);
        //     // gl_FragColor = color;
        //     gl_FragColor=vec4(0.0,0.0,0.0,1.0);
        // } else {
        //   gl_FragColor = color;
        // }
        gl_FragColor = color;
      }
    }
}

    `, nt = `
    precision mediump float;
    uniform sampler2D u_image;
    varying vec2 v_texCoord;
    void main() {
      vec4 color = texture2D(u_image, vec2(v_texCoord.x, v_texCoord.y));
      float alpha = texture2D(u_image, v_texCoord+vec2(0, -0.5)).r;
      gl_FragColor = vec4(color.rgb, alpha);
    }
      `, ot = [
  0,
  1,
  // 左下角
  1,
  1,
  // 右下角
  0,
  0,
  // 左上角
  1,
  0
  // 右上角
], it = [0, 1, 1, 1, 0, 0.5, 1, 0.5];
class st {
  constructor(e) {
    const s = {
      src: "",
      autoplay: !0,
      loop: !0,
      canvas: null,
      // 默认透明视频展示大小
      width: 375,
      height: 300,
      onError: function() {
      },
      onPlay: function() {
      }
    };
    this.options = {
      ...s,
      ...e
    }, this.radio = window.devicePixelRatio, this.initVideo(), this.initWebgl(), window.requestAnimationFrame(() => {
      this.drawFrame();
    });
  }
  initVideo() {
    console.log("initVideo");
    const { onPlay: e, onError: s, loop: h, src: p, videoId: i } = this.options, C = document.getElementById(i);
    C.setAttribute("x-webkit-airplay", "true"), C.setAttribute("webkit-playsinline", "true"), C.setAttribute("playsinline", "true"), C.style.display = "none", this.video = C;
  }
  resetVideoHide() {
    this.video.style.display = "none";
  }
  resetVideo() {
    this.video.style.display = "block";
  }
  drawFrame() {
    this.playing && this.drawWebglFrame(), window.requestAnimationFrame(() => {
      this.drawFrame();
    });
  }
  drawWebglFrame() {
    const e = this.gl;
    this.video.readyState >= this.video.HAVE_ENOUGH_DATA && (e.texImage2D(
      e.TEXTURE_2D,
      0,
      e.RGB,
      e.RGB,
      e.UNSIGNED_BYTE,
      this.video
    ), e.drawArrays(e.TRIANGLE_STRIP, 0, 4));
  }
  play() {
    this.playing = !0, this.resetVideoHide();
  }
  pause() {
    this.playing = !1, this.resetVideo(), this.gl.clear(this.gl.COLOR_BUFFER_BIT | this.gl.DEPTH_BUFFER_BIT);
  }
  initWebgl() {
    this.canvas = this.options.canvas;
    const e = this.canvas.getContext("webgl");
    console.log(
      this.canvas.width,
      this.canvas.height,
      "================alpha config"
    ), e.viewport(0, 0, this.canvas.width, this.canvas.height);
    const s = this._initShaderProgram(e);
    e.linkProgram(s), e.useProgram(s);
    const h = this._initBuffer(e);
    e.bindBuffer(e.ARRAY_BUFFER, h.position);
    const p = e.getAttribLocation(s, "a_position");
    e.enableVertexAttribArray(p), e.enable(e.BLEND), e.blendFunc(e.SRC_ALPHA, e.ONE_MINUS_SRC_ALPHA), e.vertexAttribPointer(p, 2, e.FLOAT, !1, 0, 0), e.bindBuffer(e.ARRAY_BUFFER, h.texture);
    const i = e.getAttribLocation(s, "a_texCoord");
    e.enableVertexAttribArray(i), e.vertexAttribPointer(i, 2, e.FLOAT, !1, 0, 0);
    const C = this._initTexture(e);
    e.bindTexture(e.TEXTURE_2D, C);
    const l = e.getUniformLocation(s, "u_scale");
    e.uniform2fv(l, [this.radio, this.radio]), this.gl = e;
  }
  _createShader(e, s, h) {
    const p = e.createShader(s);
    return e.shaderSource(p, h), e.compileShader(p), e.getShaderParameter(p, e.COMPILE_STATUS) || console.error(e.getShaderInfoLog(p)), p;
  }
  _initShaderProgram(e) {
    const s = this._createShader(e, e.VERTEX_SHADER, rt), h = this._createShader(
      e,
      e.FRAGMENT_SHADER,
      this.options.useUeTransparent ? nt : at
    ), p = e.createProgram();
    return e.attachShader(p, s), e.attachShader(p, h), e.linkProgram(p), p;
  }
  _initBuffer(e) {
    const s = new Float32Array([
      -1,
      1,
      1,
      1,
      -1,
      -1,
      1,
      -1
    ]), h = e.createBuffer();
    e.bindBuffer(e.ARRAY_BUFFER, h), e.bufferData(e.ARRAY_BUFFER, s, e.STATIC_DRAW);
    const p = e.createBuffer(), i = new Float32Array(
      this.options.useUeTransparent ? it : ot
    );
    return e.bindBuffer(e.ARRAY_BUFFER, p), e.bufferData(e.ARRAY_BUFFER, i, e.STATIC_DRAW), {
      position: h,
      texture: p
    };
  }
  _initTexture(e) {
    const s = e.createTexture();
    return e.bindTexture(e.TEXTURE_2D, s), e.pixelStorei(e.UNPACK_FLIP_Y_WEBGL, 1), e.texParameteri(e.TEXTURE_2D, e.TEXTURE_MIN_FILTER, e.NEAREST), e.texParameteri(e.TEXTURE_2D, e.TEXTURE_WRAP_S, e.CLAMP_TO_EDGE), e.texParameteri(e.TEXTURE_2D, e.TEXTURE_WRAP_T, e.CLAMP_TO_EDGE), s;
  }
}
const lt = `#version 300 es
  layout (location = 0) in vec4 a_position;
  layout (location = 1) in vec2 a_texCoord;
  out vec2 v_texCoord;
  void main () {
    gl_Position = a_position;
    v_texCoord = a_texCoord;
  }
`, ct = `#version 300 es
precision mediump float;
out vec4 FragColor;
in vec2 v_texCoord;

uniform sampler2D frameTexture;
uniform vec3 keyColor;

// 色度的相似度计算
uniform float similarity;
// 透明度的平滑度计算
uniform float smoothness;
// 降低绿幕饱和度，提高抠图准确度
uniform float spill;

vec2 RGBtoUV(vec3 rgb) {
  return vec2(
    rgb.r * -0.169 + rgb.g * -0.331 + rgb.b *  0.5    + 0.5,
    rgb.r *  0.5   + rgb.g * -0.419 + rgb.b * -0.081  + 0.5
  );
}

void main() {
  // 获取当前像素的rgba值
  vec4 rgba = texture(frameTexture, v_texCoord);
  // 计算当前像素与绿幕像素的色度差值
  vec2 chromaVec = RGBtoUV(rgba.rgb) - RGBtoUV(keyColor);
  // 计算当前像素与绿幕像素的色度距离（向量长度）, 越相像则色度距离越小
  float chromaDist = sqrt(dot(chromaVec, chromaVec));
  // 设置了一个相似度阈值，baseMask为负，则表明是绿幕，为正则表明不是绿幕
  float baseMask = chromaDist - similarity;
  // 如果baseMask为负数，fullMask等于0；baseMask为正数，越大，则透明度越低
  float fullMask = pow(clamp(baseMask / smoothness, 0., 1.), 1.5);
  rgba.a = fullMask; // 设置透明度
  // 如果baseMask为负数，spillVal等于0；baseMask为整数，越小，饱和度越低
  float spillVal = pow(clamp(baseMask / spill, 0., 1.), 1.5);
  float desat = clamp(rgba.r * 0.2126 + rgba.g * 0.7152 + rgba.b * 0.0722, 0., 1.); // 计算当前像素的灰度值
  rgba.rgb = mix(vec3(desat, desat, desat), rgba.rgb, spillVal);
  FragColor = rgba;
}
`, ut = [-1, 1, -1, -1, 1, -1, 1, -1, 1, 1, -1, 1], ft = [0, 1, 0, 0, 1, 0, 1, 0, 1, 1, 0, 1];
function vt(a, e, s) {
  const h = Ve(a, a.VERTEX_SHADER, e), p = Ve(a, a.FRAGMENT_SHADER, s), i = a.createProgram();
  if (a.attachShader(i, h), a.attachShader(i, p), a.linkProgram(i), !a.getProgramParameter(i, a.LINK_STATUS))
    throw Error(
      a.getProgramInfoLog(i) ?? "Unable to initialize the shader program"
    );
  return i;
}
function Ve(a, e, s) {
  const h = a.createShader(e);
  if (a.shaderSource(h, s), a.compileShader(h), !a.getShaderParameter(h, a.COMPILE_STATUS)) {
    const p = a.getShaderInfoLog(h);
    throw a.deleteShader(h), Error(p ?? "An error occurred compiling the shaders");
  }
  return h;
}
function dt(a, e, s) {
  a.bindTexture(a.TEXTURE_2D, s), a.texImage2D(a.TEXTURE_2D, 0, a.RGBA, a.RGBA, a.UNSIGNED_BYTE, e), a.drawArrays(a.TRIANGLES, 0, 6);
}
function ht(a) {
  const e = a.createTexture();
  if (e == null)
    throw Error("Create WebGL texture error");
  a.bindTexture(a.TEXTURE_2D, e);
  const s = 0, h = a.RGBA, p = 1, i = 1, C = 0, l = a.RGBA, u = a.UNSIGNED_BYTE, g = new Uint8Array([0, 0, 255, 255]);
  return a.texImage2D(
    a.TEXTURE_2D,
    s,
    h,
    p,
    i,
    C,
    l,
    u,
    g
  ), a.texParameteri(a.TEXTURE_2D, a.TEXTURE_MAG_FILTER, a.LINEAR), a.texParameteri(a.TEXTURE_2D, a.TEXTURE_MIN_FILTER, a.LINEAR), a.texParameteri(a.TEXTURE_2D, a.TEXTURE_WRAP_S, a.CLAMP_TO_EDGE), a.texParameteri(a.TEXTURE_2D, a.TEXTURE_WRAP_T, a.CLAMP_TO_EDGE), e;
}
function mt(a) {
  const e = "document" in globalThis ? globalThis.document.createElement("canvas") : new OffscreenCanvas(a.width, a.height);
  e.width = a.width, e.height = a.height;
  const s = e.getContext("webgl2", {
    premultipliedAlpha: !1,
    alpha: !0
  });
  if (s == null)
    throw Error("Cant create gl context");
  const h = vt(s, lt, ct);
  s.useProgram(h), s.uniform3fv(
    s.getUniformLocation(h, "keyColor"),
    a.keyColor.map((u) => u / 255)
  ), s.uniform1f(
    s.getUniformLocation(h, "similarity"),
    a.similarity
  ), s.uniform1f(
    s.getUniformLocation(h, "smoothness"),
    a.smoothness
  ), s.uniform1f(s.getUniformLocation(h, "spill"), a.spill);
  const p = s.createBuffer();
  s.bindBuffer(s.ARRAY_BUFFER, p), s.bufferData(s.ARRAY_BUFFER, new Float32Array(ut), s.STATIC_DRAW);
  const i = s.getAttribLocation(h, "a_position");
  s.vertexAttribPointer(
    i,
    2,
    s.FLOAT,
    !1,
    Float32Array.BYTES_PER_ELEMENT * 2,
    0
  ), s.enableVertexAttribArray(i);
  const C = s.createBuffer();
  s.bindBuffer(s.ARRAY_BUFFER, C), s.bufferData(
    s.ARRAY_BUFFER,
    new Float32Array(ft),
    s.STATIC_DRAW
  );
  const l = s.getAttribLocation(h, "a_texCoord");
  return s.vertexAttribPointer(
    l,
    2,
    s.FLOAT,
    !1,
    Float32Array.BYTES_PER_ELEMENT * 2,
    0
  ), s.enableVertexAttribArray(l), s.pixelStorei(s.UNPACK_FLIP_Y_WEBGL, 1), { cvs: e, gl: s };
}
function pt(a) {
  return a instanceof VideoFrame ? { width: a.codedWidth, height: a.codedHeight } : { width: a.width, height: a.height };
}
function gt(a) {
  const s = new OffscreenCanvas(1, 1).getContext("2d");
  s.drawImage(a, 0, 0);
  const {
    data: [h, p, i]
  } = s.getImageData(0, 0, 1, 1);
  return [h, p, i];
}
const wt = (a) => {
  let e = null, s = null, h = a.keyColor, p = null;
  return async (i) => {
    if ((e == null || s == null || p == null) && (h == null && (h = gt(i)), { cvs: e, gl: s } = mt({
      ...pt(i),
      keyColor: h,
      ...a
    }), p = ht(s)), dt(s, i, p), globalThis.VideoFrame != null && i instanceof globalThis.VideoFrame) {
      const C = new VideoFrame(e, {
        alpha: "keep",
        timestamp: i.timestamp,
        duration: i.duration ?? void 0
      });
      return i.close(), C;
    }
    return createImageBitmap(e, {
      imageOrientation: i instanceof ImageBitmap ? "flipY" : "none"
    });
  };
}, yt = () => {
  const a = He({
    statusConfig: {
      close: !0,
      init: !1,
      isAlpha: !1,
      //   视频是否开始播放
      streamVideoPlay: !1,
      // 是否发送语音数据
      sendMicrophone: !0
    },
    feAlphaConfig: {
      similarity: 0.2,
      smoothness: 0.01,
      spill: 0.3
    },
    alphaSendMessage: {
      sceneImageUrl: "https://www.yilanmeta.com/back-api/v1/files/shu999.jpg",
      screenOrientation: "vertical",
      currentScene: "shu999",
      vedioStreamIsTransparent: !0,
      code: 205
    },
    normalSendMessage: {
      sceneImageUrl: "heng1.jpg",
      screenOrientation: "vertical",
      currentScene: "heng1",
      vedioStreamIsTransparent: !1,
      code: 205
    },
    size: {
      width: 0,
      height: 0
    },
    srsConfig: {
      pushPort: 0,
      pullPort: 0
    },
    dom: {
      video: "video-" + Math.random().toString(36).substr(2, 9),
      canvas: "canvas-" + Math.random().toString(36).substr(2, 9)
    },
    wsInfo: null,
    useUeTransparent: !0,
    sendToUe: !0,
    workerPathKey: ""
  }), e = () => {
    a.statusConfig.init || (a.statusConfig.init = !0, a.statusConfig.isAlpha = !1, a.statusConfig.close = !1, console.log("startVideoStream", a.statusConfig.init));
  }, s = () => {
    a.statusConfig.init || (a.statusConfig.init = !0, a.statusConfig.close = !1, i());
  }, h = (d) => {
    a.size = {
      width: d.width,
      height: d.height
    }, a.srsConfig = {
      pushPort: d.pushPort,
      pullPort: d.pullPort
    }, a.useUeTransparent = d.useUeTransparent, a.sendToUe = d.sendToUe, a.workerPathKey = d.workerPathKey, a.feAlphaConfig = d.feAlphaConfig || a.feAlphaConfig;
  }, p = () => {
    a.statusConfig.close = !0, a.statusConfig.init = !1;
  }, i = () => {
    a.statusConfig.isAlpha = !0, g();
  }, C = () => {
    a.statusConfig.isAlpha = !1, m();
  }, l = (d) => {
    a.statusConfig.streamVideoPlay = d;
  }, u = (d) => {
    a.wsInfo = d;
  }, g = () => {
    const d = a.alphaSendMessage;
    a.wsInfo && a.wsInfo.sendMessage(d, !0);
  }, m = () => {
    const d = a.normalSendMessage;
    a.wsInfo && a.wsInfo.sendMessage(d, !0);
  };
  return {
    state: a,
    setWsInfo: u,
    startAlphaVideoStream: s,
    closeVideoStream: p,
    changeStreamToAlpha: i,
    changeStreamToNormal: C,
    changeStreamVideoPlay: l,
    startVideoStream: e,
    syncData: h
  };
}, Z = yt();
function St(a) {
  return Z.state.useUeTransparent ? Ct(a) : _t(a, Z.state.feAlphaConfig);
}
function Ct(a) {
  let e = null;
  const s = () => {
    e = new st({
      videoId: Z.state.dom.video,
      canvas: a.value,
      useUeTransparent: !0
    }), e.play(), console.log("init ===========AlphaVideo");
  };
  return {
    play: () => {
      setTimeout(() => {
        e ? e.play() : s();
      }, 0);
    },
    clear: () => {
      e.pause();
    }
  };
}
function _t(a, e) {
  let s = !1;
  const h = wt(e);
  return {
    play: async () => {
      s = !0;
      const C = document.getElementById(Z.state.dom.video), l = a.value;
      l.width = C.videoWidth, l.height = C.videoHeight;
      const u = l.getContext("2d");
      if ("requestVideoFrameCallback" in HTMLVideoElement.prototype) {
        const m = async (d, k) => {
          if (s !== !1)
            try {
              const I = await createImageBitmap(C), H = new VideoFrame(I, { timestamp: k.presentationTime });
              await g(H);
            } catch (I) {
              console.error("Error processing video feTouming frame:", I);
            } finally {
              C.requestVideoFrameCallback(m);
            }
        };
        C.requestVideoFrameCallback(m);
      } else
        console.error("Your browser does not support requestVideoFrameCallback.");
      const g = async (m) => {
        const d = await h(m);
        u == null || u.clearRect(0, 0, l.width, l.height), u == null || u.drawImage(d, 0, 0);
      };
    },
    clear: () => {
      s = !1;
      const C = a.value, l = C.getContext("2d");
      l && l.clearRect(0, 0, C.width, C.height);
    }
  };
}
class xt {
  constructor() {
    Ne(this, "events", {});
  }
  $on(e, s) {
    this.events[e] || (this.events[e] = [], console.log(this.events[e])), this.events[e].push(s);
  }
  $off(e, s) {
    this.events[e] && (this.events[e] = this.events[e].filter((h) => h !== s));
  }
  $emit(e, ...s) {
    if (this.events[e])
      for (const h of this.events[e])
        h(...s);
  }
}
const de = new xt(), bt = /* @__PURE__ */ Oe({
  __name: "alphaPlay",
  props: {
    show: { type: Boolean }
  },
  setup(a) {
    const e = a, s = ve(null), {
      play: h,
      clear: p
    } = St(s);
    de.$on("canvasSize", (l) => {
      i(e.show), s.value && (console.log(Z.state.size.height, "store.state.size.height"), Z.state.size.height ? (s.value.style.width = Z.state.size.width + "px", s.value.style.height = Z.state.size.height + "px") : (s.value.style.width = l.width + "px", s.value.style.height = l.height + "px"));
    }), Pe(() => e.show, (l) => {
      i(l);
    });
    const i = (l) => {
      l ? h() : p();
    }, C = ge(() => Z.state.size.width && Z.state.size.height ? {
      width: Z.state.size.width + "px",
      height: Z.state.size.height + "px"
    } : {});
    return (l, u) => Ue((ze(), $e("div", null, [
      je("canvas", {
        style: Ke(C.value),
        ref_key: "canvasRef",
        ref: s
      }, null, 4)
    ], 512)), [
      [Be, e.show]
    ]);
  }
}), Tt = {
  class: "sdk-video-warp",
  style: { "pointer-events": "none" }
}, At = ["id"], Mt = /* @__PURE__ */ Oe({
  __name: "index",
  props: {
    stop: {
      type: Boolean,
      default: !1
    },
    webrtcurl: {
      type: String,
      default: ""
    }
  },
  setup(a) {
    const e = a, s = ge(() => Z.state.statusConfig.isAlpha), h = ve(null);
    let p = {
      sessionId: "",
      simulatorUrl: "",
      startPlay: () => {
      },
      stopPlay: () => {
      }
    };
    const i = ve(!1);
    Pe(() => e.webrtcurl, async (u) => {
      if (console.log(u, "webrtcur=========="), !!u) {
        console.log(e.webrtcurl, "webrtcurl"), p = tt(u, h);
        try {
          await p.startPlay();
        } catch {
          de.$emit("playError"), i.value = !0;
        }
      }
    }), Pe(() => e.stop, (u) => {
      u ? (p.stopPlay(), i.value = !0) : p.startPlay();
    });
    const C = () => {
      const u = h.value, g = document.getElementById(Z.state.dom.canvas);
      g && u && (console.log(u == null ? void 0 : u.videoWidth, u == null ? void 0 : u.videoHeight, "videoElement?.videoWidth, videoElement?.videoHeight"), (u == null ? void 0 : u.videoWidth) < (u == null ? void 0 : u.videoHeight) && (g.width = u == null ? void 0 : u.videoWidth, g.height = u == null ? void 0 : u.videoHeight), h.value && (h.value.style.width = (u == null ? void 0 : u.videoWidth) + "px", h.value.style.height = (u == null ? void 0 : u.videoHeight) + "px")), de.$emit("canvasSize", g ? { width: g.width, height: g.height } : null);
    }, l = () => {
      const u = h.value, g = ["fullscreenchange", "webkitfullscreenchange", "mozfullscreenchange", "MSFullscreenChange"];
      function m(d) {
        (document.fullscreenElement || document.webkitFullscreenElement || document.mozFullScreenElement || document.msFullscreenElement) && (document.exitFullscreen ? document.exitFullscreen() : document.webkitExitFullscreen ? document.webkitExitFullscreen() : document.mozCancelFullScreen ? document.mozCancelFullScreen() : document.msExitFullscreen && document.msExitFullscreen(), d.preventDefault());
      }
      g.forEach((d) => {
        u == null || u.addEventListener(d, m, !1);
      }), u == null || u.addEventListener("playing", () => {
        console.log("=====playing=="), Z.changeStreamVideoPlay(!0);
      }), De(() => {
        console.log("=====onUnmounted=="), g.forEach((d) => {
          u == null || u.removeEventListener(d, m, !1);
        }), u == null || u.removeEventListener("resize", C), Z.changeStreamVideoPlay(!1);
      });
    };
    return Je(() => {
      var u, g;
      l(), (u = h.value) == null || u.addEventListener("resize", C), (g = h.value) == null || g.addEventListener("playing", () => {
        console.log("=====playing=="), de.$emit("playing");
      });
    }), De(() => {
      p.stopPlay();
    }), (u, g) => (ze(), $e("div", Tt, [
      Ue(Qe(bt, { show: s.value }, null, 8, ["show"]), [
        [Be, s.value && !e.stop]
      ]),
      Ue(je("video", {
        id: Ze(Z).state.dom.video,
        ref_key: "rtcMediaPlayer",
        ref: h,
        playsinline: "",
        class: "sdk-video-video",
        autoplay: ""
      }, null, 8, At), [
        [Be, !s.value && !e.stop]
      ])
    ]));
  }
});
let Fe;
const Rt = new Uint8Array(16);
function Et() {
  if (!Fe && (Fe = typeof crypto < "u" && crypto.getRandomValues && crypto.getRandomValues.bind(crypto), !Fe))
    throw new Error("crypto.getRandomValues() not supported. See https://github.com/uuidjs/uuid#getrandomvalues-not-supported");
  return Fe(Rt);
}
const ye = [];
for (let a = 0; a < 256; ++a)
  ye.push((a + 256).toString(16).slice(1));
function It(a, e = 0) {
  return ye[a[e + 0]] + ye[a[e + 1]] + ye[a[e + 2]] + ye[a[e + 3]] + "-" + ye[a[e + 4]] + ye[a[e + 5]] + "-" + ye[a[e + 6]] + ye[a[e + 7]] + "-" + ye[a[e + 8]] + ye[a[e + 9]] + "-" + ye[a[e + 10]] + ye[a[e + 11]] + ye[a[e + 12]] + ye[a[e + 13]] + ye[a[e + 14]] + ye[a[e + 15]];
}
const Pt = typeof crypto < "u" && crypto.randomUUID && crypto.randomUUID.bind(crypto), We = {
  randomUUID: Pt
};
function Ge(a, e, s) {
  if (We.randomUUID && !e && !a)
    return We.randomUUID();
  a = a || {};
  const h = a.random || (a.rng || Et)();
  if (h[6] = h[6] & 15 | 64, h[8] = h[8] & 63 | 128, e) {
    s = s || 0;
    for (let p = 0; p < 16; ++p)
      e[s + p] = h[p];
    return e;
  }
  return It(h);
}
function Lt(a, e) {
  let h = 0;
  const p = ve(null), i = ve(null), C = ve(!1), l = ve(!1), u = ve(""), g = He({ value: "", shoot: !1 }), m = ve(!0), d = ve(""), k = ve(""), I = ve(!1), H = ve(!0);
  let M = {}, R = {
    product: e.product,
    did: e.did,
    branch: e.branch,
    vocativeWord: e.vocativeWord,
    speechSubjects: e.speechSubjects,
    voiceConfig: e.voiceConfig,
    isCustom: e.isCustom
  };
  const X = ve(!1), K = e.imagePre, U = e.urlPre, B = ge(() => e.isMobile), J = ge(() => e.background), re = ge(() => e.human), ne = ge(() => e.screenOrientation), fe = ge(() => e.knowledgeRoleId), oe = ge(() => e.knowledgService), ie = ge(() => e.currentVirtualTitle), A = ge(() => e.vocativeStrategy), b = ge(() => e.needKnowledge), he = ge(() => e.sendToUe), me = ge(() => e.workerPathKey), xe = ge(() => e.chatProvider);
  let Se, ce = null, Ce = null;
  const we = () => {
    const v = {
      201: (c) => {
        g.value = c.userSay, m.value = !0;
      },
      202: (c) => {
        g.value = c.userSay, de.$emit("userSay", g.value, c), d.value = c.virtualSay, c.intention >= 0 ? (ce && (ce(), ce = null, Ce = null), m.value = !1, g.shoot = !0, c.extParams && de.$emit("extParams", c.extParams)) : (g.shoot = !1, Ce && (Ce(), Ce = null, ce = null));
      },
      203: (c) => {
        de.$emit("startsay", c.virtualSay, c), d.value = c.virtualSay, m.value = !1;
      },
      204: (c) => {
        de.$emit("endsay"), ce && Ce && (ce(), ce = null, Ce = null), u.value = "", g.value = c.userSay, d.value = c.virtualSay, m.value = !0;
      },
      205: (c) => {
        g.value = c.userSay, d.value = c.virtualSay, l.value = !0;
      },
      206: () => {
        m.value = !1;
      },
      // 展示实时字幕
      212: (c) => {
        var w;
        const S = c.userSay;
        c.isFullSentence && de.$emit("userSay", c.userSay, c), (w = c.userSay) != null && w.trim() && (g.value = c.userSay, de.$emit("realTimerSay", c.userSay, c)), m.value && (g.value = S);
      },
      213: () => {
        de.$emit("endAnimation");
      },
      404: () => {
        d.value = "连接已断开";
      }
    };
    h < 1 ? (i.value = new WebSocket(a), i.value.onopen = () => {
      C.value = !0;
    }, i.value.onmessage = (c) => {
      h = 1;
      try {
        const S = JSON.parse(c.data);
        const w = v[S.code];
        w ? w(S) : (p.value = c.data, I.value = !0);
      } catch (S) {
        console.error(S);
      }
    }, i.value.onerror = (c) => {
      console.error(`WebSocket error: ${c}`);
    }, i.value.onclose = () => {
      C.value = !1, d.value = "连接已断开", console.log("WebSocket connection closed");
    }, i.value.onclose = te) : (p.value = "服务器连接已断开, 请刷新页面重试", console.log("Maximum reconnect attempts exceeded"));
  };
  function te(v) {
    v.wasClean && console.log("wasClean"), C.value = !1, console.log("WebSocket connection closed"), d.value = "连接已断开", v.wasClean || (h++, console.log(
      `Attempt to reconnect (${h}/1)`
    ), Se = setTimeout(we, 2e3));
  }
  const se = ge(() => {
    const v = J.value;
    return B.value ? v && v.includes("shu") : v && v.includes("heng");
  });
  let y = ve(e.defaultImage);
  const Me = (v, c) => {
    var _, F;
    const S = se.value ? K + v : K + ((_ = y.value) == null ? void 0 : _.image), w = se.value ? v : (F = y.value) == null ? void 0 : F.image;
    console.log(y.value, "defaultImage"), M.sceneImageUrl = "https://www.yilanmeta.com/" + S, M.currentScene = w, M.currentVirtual = c, M.currentVirtualTitle = ie.value, M.virtualRoleUk = c, M.knowledgeRoleId = b.value ? fe.value : "", M.vedioStreamIsTransparent = !1, M.screenOrientation = ne.value ? ne.value : "horizontal", M.vocativeStrategy = A.value, M.sendToUe = he.value === void 0 ? !0 : he.value, console.log(C.value, "isConnected");
  }, t = document.location.protocol === "https:", n = location.host, r = () => {
    if (!X.value) {
      X.value = !0;
      const v = `/live/${kt(10)}`, c = `rtmp://${U}:${Z.state.srsConfig.pushPort}${v}`;
      k.value = t ? `webrtc://${n}${v}` : `webrtc://${U}:${Z.state.srsConfig.pullPort}${v}`, M = {
        ...R,
        chatProvider: xe.value || "zhipu",
        version: "1.0.0",
        platform: "windows",
        screenOrientation: ne.value,
        user: e.user,
        knowledgService: oe.value,
        userAccount: e.user,
        vedioStreamIsTransparent: !1,
        sceneImageIsPreset: !1,
        rtmpAddress: c,
        agentIdManageServiceId: Ge(),
        commodityIdManageServiceId: Ge(),
        asrProvider: "yilan",
        nlpProvider: "yilan",
        allPipeline: !0,
        connecting: !0,
        consuming: !1,
        appUserId: "1"
      };
    }
    me.value && (M.workerPathKey = me.value), Me(e.background, e.human), Z.state.statusConfig.isAlpha && (M = {
      ...M,
      ...Z.state.alphaSendMessage
    }), M.product && i.value.send(JSON.stringify({ ...M, code: 205 }));
  };
  return Pe(
    C,
    async (v) => {
      v && r();
    },
    { deep: !0 }
  ), we(), De(() => {
    i.value && i.value.close(), Se && clearTimeout(Se);
  }), {
    close: () => {
      var v;
      (v = i.value) == null || v.close(), d.value = "", g.value = "", u.value = "";
    },
    isConnected: C,
    ws: i,
    globalSendMessage: M,
    isOk: l,
    sendMessage: (v, c) => {
      const { update: S, reset: w, watch: _ } = c || {};
      if (i.value && X.value) {
        v.yiwiseBotId !== void 0 && (v.yiwiseBotId ? M.yiwiseBotId = v.yiwiseBotId : delete M.yiwiseBotId), S && Object.keys(v).map((G) => {
          M[G] = v[G];
        });
        let F;
        if (_) {
          const G = Ft();
          F = G.promise, ce = G.resolve, Ce = G.reject;
        }
        return w && Me(J.value, re.value), C.value && i.value.send(JSON.stringify({ ...M, ...v })), F;
      }
    },
    errorModalVisible: I,
    error: p,
    microStatus: u,
    peopleText: g,
    aiText: d,
    clientCanSpeak: m,
    webrtcurl: k,
    abledSend: H
  };
}
function kt(a) {
  const e = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz", s = e.length;
  let h = "";
  for (let p = 0; p < a; p++) {
    const i = Math.floor(Math.random() * s), C = e.charAt(i);
    h += C;
  }
  return h;
}
function Ft() {
  let a = () => {
    throw new Error("Promise resolve function not yet initialized");
  }, e = () => {
    throw new Error("Promise reject function not yet initialized");
  };
  return { promise: new Promise((h, p) => {
    a = h, e = p;
  }), resolve: a, reject: e };
}
function Dt(a) {
  return a && a.__esModule && Object.prototype.hasOwnProperty.call(a, "default") ? a.default : a;
}
var Xe = { exports: {} };
(function(a) {
  (function(e) {
    var s = typeof window == "object" && !!window.document, h = s ? window : Object;
    e(h, s), a.exports && (a.exports = h.Recorder);
  })(function(e, s) {
    var h = function() {
    }, p = function(t) {
      return typeof t == "number";
    }, i = function(t) {
      return new xe(t);
    }, C = i.LM = "2024-04-09 19:15", l = "https://github.com/xiangyuecn/Recorder", u = "Recorder", g = "getUserMedia", m = "srcSampleRate", d = "sampleRate", k = "bitRate", I = "catch", H = e[u];
    if (H && H.LM == C) {
      H.CLog(H.i18n.$T("K8zP::重复导入{1}", 0, u), 3);
      return;
    }
    i.IsOpen = function() {
      var t = i.Stream;
      if (t) {
        var n = t.getTracks && t.getTracks() || t.audioTracks || [], r = n[0];
        if (r) {
          var o = r.readyState;
          return o == "live" || o == r.LIVE;
        }
      }
      return !1;
    }, i.BufferSize = 4096, i.Destroy = function() {
      b(u + " Destroy"), ie();
      for (var t in M)
        M[t]();
    };
    var M = {};
    i.BindDestroy = function(t, n) {
      M[t] = n;
    }, i.Support = function() {
      if (!s)
        return !1;
      var t = navigator.mediaDevices || {};
      return t[g] || (t = navigator, t[g] || (t[g] = t.webkitGetUserMedia || t.mozGetUserMedia || t.msGetUserMedia)), !(!t[g] || (i.Scope = t, !i.GetContext()));
    }, i.GetContext = function(t) {
      if (!s)
        return null;
      var n = window.AudioContext;
      if (n || (n = window.webkitAudioContext), !n)
        return null;
      var r = i.Ctx;
      if ((!r || r.state == "closed") && (r = i.Ctx = new n(), i.NewCtxs = i.NewCtxs || [], i.BindDestroy("Ctx", function() {
        var o = i.Ctx;
        o && o.close && (R(o), i.Ctx = 0);
        var f = i.NewCtxs;
        i.NewCtxs = [];
        for (var v = 0; v < f.length; v++)
          R(f[v]);
      })), t && r.close)
        try {
          r = new n(), i.NewCtxs.push(r);
        } catch (o) {
          b("GetContext tryNew Error", 1, o);
        }
      return r;
    }, i.CloseNewCtx = function(t) {
      if (t && t != i.Ctx) {
        R(t);
        for (var n = i.NewCtxs || [], r = n.length, o = 0; o < n.length; o++)
          if (n[o] == t) {
            n.splice(o, 1);
            break;
          }
        b(y("mSxV::剩{1}个GetContext未close", 0, r + "-1=" + n.length), n.length ? 3 : 0);
      }
    };
    var R = function(t) {
      if (t && t.close) {
        t._isC = 1;
        try {
          t.close();
        } catch (n) {
          b("ctx close err", 1, n);
        }
      }
    }, X = i.ResumeCtx = function(t, n, r, o) {
      var f = 0, v = 0, c = 0, S = 0, w = "EventListener", _ = "ResumeCtx ", F = function(P, T) {
        v && G(), f || (f = 1, P && o(P, S), T && r(S)), T && (!t._LsSC && t["add" + w] && t["add" + w]("statechange", O), t._LsSC = 1, c = 1);
      }, G = function(P) {
        if (!(P && v)) {
          v = P ? 1 : 0;
          for (var T = ["focus", "mousedown", "mouseup", "touchstart", "touchend"], E = 0; E < T.length; E++)
            window[(P ? "add" : "remove") + w](T[E], O, !0);
        }
      }, O = function() {
        var P = t.state, T = K(P);
        if (!f && !n(T ? ++S : S))
          return F();
        T ? (c && b(_ + "sc " + P, 3), G(1), t.resume().then(function() {
          c && b(_ + "sc " + t.state), F(0, 1);
        })[I](function(E) {
          b(_ + "error", 1, E), K(t.state) || F(E.message || "error");
        })) : P == "closed" ? (c && !t._isC && b(_ + "sc " + P, 1), F("ctx closed")) : F(0, 1);
      };
      O();
    }, K = i.CtxSpEnd = function(t) {
      return t == "suspended" || t == "interrupted";
    }, U = function(t) {
      var n = t.state, r = "ctx.state=" + n;
      return K(n) && (r += y("nMIy::（注意：ctx不是running状态，rec.open和start至少要有一个在用户操作(触摸、点击等)时进行调用，否则将在rec.start时尝试进行ctx.resume，可能会产生兼容性问题(仅iOS)，请参阅文档中runningContext配置）")), r;
    }, B = "ConnectEnableWebM";
    i[B] = !0;
    var J = "ConnectEnableWorklet";
    i[J] = !1;
    var re = function(t, n) {
      var r = t.BufferSize || i.BufferSize, o = t.Stream, f = o._RC || o._c || i.GetContext(!0);
      o._c = f;
      var v = function(N) {
        var W = o._m = f.createMediaStreamSource(o), L = f.destination, Y = "createMediaStreamDestination";
        f[Y] && (L = o._d = f[Y]()), W.connect(N), N.connect(L);
      }, c, S, w, _ = "", F = o._call, G = function(N) {
        for (var W in F) {
          for (var L = N.length, Y = new Int16Array(L), Q = 0, ee = 0; ee < L; ee++) {
            var ae = Math.max(-1, Math.min(1, N[ee]));
            ae = ae < 0 ? ae * 32768 : ae * 32767, Y[ee] = ae, Q += Math.abs(ae);
          }
          for (var le in F)
            F[le](Y, Q);
          return;
        }
      }, O = "ScriptProcessor", P = "audioWorklet", T = u + " " + P, E = "RecProc", $ = "MediaRecorder", j = $ + ".WebM.PCM", D = f.createScriptProcessor || f.createJavaScriptNode, q = y("ZGlf::。由于{1}内部1秒375次回调，在移动端可能会有性能问题导致回调丢失录音变短，PC端无影响，暂不建议开启{1}。", 0, P), z = function() {
        S = o.isWorklet = !1, fe(o), b(y("7TU0::Connect采用老的{1}，", 0, O) + se.get(
          i[J] ? y("JwCL::但已设置{1}尝试启用{2}", 2) : y("VGjB::可设置{1}尝试启用{2}", 2),
          [u + "." + J + "=true", P]
        ) + _ + q, 3);
        var N = o._p = D.call(f, r, 1, 1);
        v(N), N.onaudioprocess = function(W) {
          var L = W.inputBuffer.getChannelData(0);
          G(L);
        };
      }, x = function() {
        c = o.isWebM = !1, oe(o), S = o.isWorklet = !D || i[J];
        var N = window.AudioWorkletNode;
        if (!(S && f[P] && N)) {
          z();
          return;
        }
        var W = function() {
          var ae = function(ue) {
            return ue.toString().replace(/^function|DEL_/g, "").replace(/\$RA/g, T);
          }, le = "class " + E + " extends AudioWorkletProcessor{";
          return le += "constructor " + ae(function(ue) {
            DEL_super(ue);
            var _e = this, pe = ue.processorOptions.bufferSize;
            _e.bufferSize = pe, _e.buffer = new Float32Array(pe * 2), _e.pos = 0, _e.port.onmessage = function(be) {
              be.data.kill && (_e.kill = !0, $C.log("$RA kill call"));
            }, $C.log("$RA .ctor call", ue);
          }), le += "process " + ae(function(ue, _e, pe) {
            var be = this, Re = be.bufferSize, Ee = be.buffer, Te = be.pos;
            if (ue = (ue[0] || [])[0] || [], ue.length) {
              Ee.set(ue, Te), Te += ue.length;
              var Ie = ~~(Te / Re) * Re;
              if (Ie) {
                this.port.postMessage({ val: Ee.slice(0, Ie) });
                var Ae = Ee.subarray(Ie, Te);
                Ee = new Float32Array(Re * 2), Ee.set(Ae), Te = Ae.length, be.buffer = Ee;
              }
              be.pos = Te;
            }
            return !be.kill;
          }), le += '}try{registerProcessor("' + E + '", ' + E + ')}catch(e){$C.error("' + T + ' Reg Error",e)}', le = le.replace(/\$C\./g, "console."), "data:text/javascript;base64," + btoa(unescape(encodeURIComponent(le)));
        }, L = function() {
          return S && o._na;
        }, Y = o._na = function() {
          w !== "" && (clearTimeout(w), w = setTimeout(function() {
            w = 0, L() && (b(y("MxX1::{1}未返回任何音频，恢复使用{2}", 0, P, O), 3), D && z());
          }, 500));
        }, Q = function() {
          if (L()) {
            var ae = o._n = new N(f, E, {
              processorOptions: { bufferSize: r }
            });
            v(ae), ae.port.onmessage = function(le) {
              w && (clearTimeout(w), w = ""), L() ? G(le.data.val) : S || b(y("XUap::{1}多余回调", 0, P), 3);
            }, b(y("yOta::Connect采用{1}，设置{2}可恢复老式{3}", 0, P, u + "." + J + "=false", O) + _ + q, 3);
          }
        }, ee = function() {
          if (L()) {
            if (f[E]) {
              Q();
              return;
            }
            var ae = W();
            f[P].addModule(ae).then(function(le) {
              L() && (f[E] = 1, Q(), w && Y());
            })[I](function(le) {
              b(P + ".addModule Error", 1, le), L() && z();
            });
          }
        };
        X(f, function() {
          return L();
        }, ee, ee);
      }, V = function() {
        var N = window[$], W = "ondataavailable", L = "audio/webm; codecs=pcm";
        c = o.isWebM = i[B];
        var Y = N && W in N.prototype && N.isTypeSupported(L);
        if (_ = Y ? "" : y("VwPd::（此浏览器不支持{1}）", 0, j), !n || !c || !Y) {
          x();
          return;
        }
        var Q = function() {
          return c && o._ra;
        };
        o._ra = function() {
          w !== "" && (clearTimeout(w), w = setTimeout(function() {
            Q() && (b(y("vHnb::{1}未返回任何音频，降级使用{2}", 0, $, P), 3), x());
          }, 500));
        };
        var ee = Object.assign({ mimeType: L }, i.ConnectWebMOptions), ae = o._r = new N(o, ee), le = o._rd = { sampleRate: f[d] };
        ae[W] = function(ue) {
          var _e = new FileReader();
          _e.onloadend = function() {
            if (Q()) {
              var pe = Se(new Uint8Array(_e.result), le);
              if (!pe)
                return;
              if (pe == -1) {
                x();
                return;
              }
              w && (clearTimeout(w), w = ""), G(pe);
            } else
              c || b(y("O9P7::{1}多余回调", 0, $), 3);
          }, _e.readAsArrayBuffer(ue.data);
        }, ae.start(~~(r / 48)), b(y("LMEm::Connect采用{1}，设置{2}可恢复使用{3}或老式{4}", 0, j, u + "." + B + "=false", P, O));
      };
      V();
    }, ne = function(t) {
      t._na && t._na(), t._ra && t._ra();
    }, fe = function(t) {
      t._na = null, t._n && (t._n.port.postMessage({ kill: !0 }), t._n.disconnect(), t._n = null);
    }, oe = function(t) {
      if (t._ra = null, t._r) {
        try {
          t._r.stop();
        } catch (n) {
          b("mr stop err", 1, n);
        }
        t._r = null;
      }
    }, ie = function(t) {
      t = t || i;
      var n = t == i, r = t.Stream;
      r && (r._m && (r._m.disconnect(), r._m = null), !r._RC && r._c && i.CloseNewCtx(r._c), r._RC = null, r._c = null, r._d && (A(r._d.stream), r._d = null), r._p && (r._p.disconnect(), r._p.onaudioprocess = r._p = null), fe(r), oe(r), n && A(r)), t.Stream = 0;
    }, A = i.StopS_ = function(t) {
      for (var n = t.getTracks && t.getTracks() || t.audioTracks || [], r = 0; r < n.length; r++) {
        var o = n[r];
        o.stop && o.stop();
      }
      t.stop && t.stop();
    };
    i.SampleData = function(t, n, r, o, f) {
      var v = "SampleData";
      o || (o = {});
      var c = o.index || 0, S = o.offset || 0, w = o.filter;
      if (w && w.fn && w.sr != n && (w = null, b(y("d48C::{1}的filter采样率变了，重设滤波", 0, v), 3)), !w) {
        var _ = r > n * 3 / 4 ? 0 : r / 2 * 3 / 4;
        w = { fn: _ ? i.IIRFilter(!0, n, _) : 0 };
      }
      w.sr = n;
      var F = w.fn, G = o.frameNext || [];
      f || (f = {});
      var O = f.frameSize || 1;
      f.frameType && (O = f.frameType == "mp3" ? 1152 : 1);
      var P = t.length;
      c > P + 1 && b(y("tlbC::{1}似乎传入了未重置chunk {2}", 0, v, c + ">" + P), 3);
      for (var T = 0, E = c; E < P; E++)
        T += t[E].length;
      T = Math.max(0, T - Math.floor(S));
      var $ = n / r;
      $ > 1 ? T = Math.floor(T / $) : ($ = 1, r = n), T += G.length;
      for (var j = new Int16Array(T), D = 0, E = 0; E < G.length; E++)
        j[D] = G[E], D++;
      for (; c < P; c++) {
        for (var q = t[c], E = S, z = q.length, x = F && F.Embed, V = 0, N = 0, W = 0, L = 0, Y = 0, Q = 0; Y < z; Y++, Q++) {
          if (Q < z && (x ? (W = q[Q], L = x.b0 * W + x.b1 * x.x1 + x.b0 * x.x2 - x.a1 * x.y1 - x.a2 * x.y2, x.x2 = x.x1, x.x1 = W, x.y2 = x.y1, x.y1 = L) : L = F ? F(q[Q]) : q[Q]), V = N, N = L, Q == 0) {
            Y--;
            continue;
          }
          var ee = Math.floor(E);
          if (Y == ee) {
            var ae = Math.ceil(E), le = E - ee, ue = V, _e = ae < z ? N : ue, pe = ue + (_e - ue) * le;
            pe > 32767 ? pe = 32767 : pe < -32768 && (pe = -32768), j[D] = pe, D++, E += $;
          }
        }
        S = Math.max(0, E - z);
      }
      G = null;
      var be = j.length % O;
      if (be > 0) {
        var Re = (j.length - be) * 2;
        G = new Int16Array(j.buffer.slice(Re)), j = new Int16Array(j.buffer.slice(0, Re));
      }
      return {
        index: c,
        offset: S,
        filter: w,
        frameNext: G,
        sampleRate: r,
        data: j
      };
    }, i.IIRFilter = function(t, n, r) {
      var o = 2 * Math.PI * r / n, f = Math.sin(o), v = Math.cos(o), c = f / 2, S = 1 + c, w = -2 * v / S, _ = (1 - c) / S;
      if (t)
        var F = (1 - v) / 2 / S, G = (1 - v) / S;
      else
        var F = (1 + v) / 2 / S, G = -(1 + v) / S;
      var O = 0, P = 0, T = 0, E = 0, $ = 0, j = function(D) {
        return T = F * D + G * O + F * P - w * E - _ * $, P = O, O = D, $ = E, E = T, T;
      };
      return j.Embed = { x1: 0, x2: 0, y1: 0, y2: 0, b0: F, b1: G, a1: w, a2: _ }, j;
    }, i.PowerLevel = function(t, n) {
      var r = t / n || 0, o;
      return r < 1251 ? o = Math.round(r / 1250 * 10) : o = Math.round(Math.min(100, Math.max(0, (1 + Math.log(r / 1e4) / Math.log(10)) * 100))), o;
    }, i.PowerDBFS = function(t) {
      var n = Math.max(0.1, t || 0), r = 32767;
      return n = Math.min(n, r), n = 20 * Math.log(n / r) / Math.log(10), Math.max(-100, Math.round(n));
    }, i.CLog = function(t, n) {
      if (typeof console == "object") {
        var r = /* @__PURE__ */ new Date(), o = ("0" + r.getMinutes()).substr(-2) + ":" + ("0" + r.getSeconds()).substr(-2) + "." + ("00" + r.getMilliseconds()).substr(-3), f = this && this.envIn && this.envCheck && this.id, v = ["[" + o + " " + u + (f ? ":" + f : "") + "]" + t], c = arguments, S = i.CLog, w = 2, _ = S.log || console.log;
        for (p(n) ? _ = n == 1 ? S.error || console.error : n == 3 ? S.warn || console.warn : _ : w = 1; w < c.length; w++)
          v.push(c[w]);
        he ? _ && _("[IsLoser]" + v[0], v.length > 1 ? v : "") : _.apply(console, v);
      }
    };
    var b = function() {
      i.CLog.apply(this, arguments);
    }, he = !0;
    try {
      he = !console.log.apply;
    } catch {
    }
    var me = 0;
    function xe(t) {
      var n = this;
      n.id = ++me, Me();
      var r = {
        type: "mp3",
        onProcess: h
        //fn(buffers,powerLevel,bufferDuration,bufferSampleRate,newBufferIdx,asyncEnd) buffers=[[Int16,...],...]：缓冲的PCM数据，为从开始录音到现在的所有pcm片段；powerLevel：当前缓冲的音量级别0-100，bufferDuration：已缓冲时长，bufferSampleRate：缓冲使用的采样率（当type支持边录边转码(Worker)时，此采样率和设置的采样率相同，否则不一定相同）；newBufferIdx:本次回调新增的buffer起始索引；asyncEnd:fn() 如果onProcess是异步的(返回值为true时)，处理完成时需要调用此回调，如果不是异步的请忽略此参数，此方法回调时必须是真异步（不能真异步时需用setTimeout包裹）。onProcess返回值：如果返回true代表开启异步模式，在某些大量运算的场合异步是必须的，必须在异步处理完成时调用asyncEnd(不能真异步时需用setTimeout包裹)，在onProcess执行后新增的buffer会全部替换成空数组，因此本回调开头应立即将newBufferIdx到本次回调结尾位置的buffer全部保存到另外一个数组内，处理完成后写回buffers中本次回调的结尾位置。
        //*******高级设置******
        //,sourceStream:MediaStream Object
        //可选直接提供一个媒体流，从这个流中录制、实时处理音频数据（当前Recorder实例独享此流）；不提供时为普通的麦克风录音，由getUserMedia提供音频流（所有Recorder实例共享同一个流）
        //比如：audio、video标签dom节点的captureStream方法（实验特性，不同浏览器支持程度不高）返回的流；WebRTC中的remote流；自己创建的流等
        //注意：流内必须至少存在一条音轨(Audio Track)，比如audio标签必须等待到可以开始播放后才会有音轨，否则open会失败
        //,runningContext:AudioContext
        //可选提供一个state为running状态的AudioContext对象(ctx)；默认会在rec.open时自动创建一个新的ctx，无用户操作（触摸、点击等）时调用rec.open的ctx.state可能为suspended，会在rec.start时尝试进行ctx.resume，如果也无用户操作ctx.resume可能不会恢复成running状态（目前仅iOS上有此兼容性问题），导致无法去读取媒体流，这时请提前在用户操作时调用Recorder.GetContext(true)来得到一个running状态AudioContext（用完需调用CloseNewCtx(ctx)关闭）
        //,audioTrackSet:{ deviceId:"",groupId:"", autoGainControl:true, echoCancellation:true, noiseSuppression:true }
        //普通麦克风录音时getUserMedia方法的audio配置参数，比如指定设备id，回声消除、降噪开关；注意：提供的任何配置值都不一定会生效
        //由于麦克风是全局共享的，所以新配置后需要close掉以前的再重新open
        //更多参考: https://developer.mozilla.org/en-US/docs/Web/API/MediaTrackConstraints
        //,disableEnvInFix:false 内部参数，禁用设备卡顿时音频输入丢失补偿功能
        //,takeoffEncodeChunk:NOOP //fn(chunkBytes) chunkBytes=[Uint8,...]：实时编码环境下接管编码器输出，当编码器实时编码出一块有效的二进制音频数据时实时回调此方法；参数为二进制的Uint8Array，就是编码出来的音频数据片段，所有的chunkBytes拼接在一起即为完整音频。本实现的想法最初由QQ2543775048提出
        //当提供此回调方法时，将接管编码器的数据输出，编码器内部将放弃存储生成的音频数据；如果当前编码器或环境不支持实时编码处理，将在open时直接走fail逻辑
        //因此提供此回调后调用stop方法将无法获得有效的音频数据，因为编码器内没有音频数据，因此stop时返回的blob将是一个字节长度为0的blob
        //大部分录音格式编码器都支持实时编码（边录边转码），比如mp3格式：会实时的将编码出来的mp3片段通过此方法回调，所有的chunkBytes拼接到一起即为完整的mp3，此种拼接的结果比mock方法实时生成的音质更加，因为天然避免了首尾的静默
        //不支持实时编码的录音格式不可以提供此回调（wav格式不支持，因为wav文件头中需要提供文件最终长度），提供了将在open时直接走fail逻辑
      };
      for (var o in t)
        r[o] = t[o];
      n.set = r;
      var f = r[k], v = r[d];
      (f && !p(f) || v && !p(v)) && n.CLog(y.G("IllegalArgs-1", [y("VtS4::{1}和{2}必须是数值", 0, d, k)]), 1, t), r[k] = +f || 16, r[d] = +v || 16e3, n.state = 0, n._S = 9, n.Sync = { O: 9, C: 9 };
    }
    i.Sync = {
      /*open*/
      O: 9,
      /*close*/
      C: 9
    }, i.prototype = xe.prototype = {
      CLog: b,
      _streamStore: function() {
        return this.set.sourceStream ? this : i;
      },
      _streamCtx: function() {
        var t = this._streamStore().Stream;
        return t && t._c;
      },
      open: function(t, n) {
        var r = this, o = r.set, f = r._streamStore(), v = 0;
        t = t || h;
        var c = function(x, V) {
          V = !!V, r.CLog(y("5tWi::录音open失败：") + x + ",isUserNotAllow:" + V, 1), v && i.CloseNewCtx(v), n && n(x, V);
        };
        r._streamTag = g;
        var S = function() {
          r.CLog("open ok, id:" + r.id + " stream:" + r._streamTag), t(), r._SO = 0;
        }, w = f.Sync, _ = ++w.O, F = w.C;
        r._O = r._O_ = _, r._SO = r._S;
        var G = function() {
          if (F != w.C || !r._O) {
            var x = y("dFm8::open被取消");
            return _ == w.O ? r.close() : x = y("VtJO::open被中断"), c(x), !0;
          }
        };
        if (!s) {
          c(y.G("NonBrowser-1", ["open"]) + y("EMJq::，可尝试使用RecordApp解决方案") + "(" + l + "/tree/master/app-support-sample)");
          return;
        }
        var O = r.envCheck({ envName: "H5", canProcess: !0 });
        if (O) {
          c(y("A5bm::不能录音：") + O);
          return;
        }
        if (o.sourceStream) {
          if (r._streamTag = "set.sourceStream", !i.GetContext()) {
            c(y("1iU7::不支持此浏览器从流中获取录音"));
            return;
          }
          ie(f);
          var P = r.Stream = o.sourceStream;
          P._RC = o.runningContext, P._call = {};
          try {
            re(f);
          } catch (x) {
            ie(f), c(y("BTW2::从流中打开录音失败：") + x.message);
            return;
          }
          S();
          return;
        }
        var T = function(x, V) {
          try {
            window.top.a;
          } catch {
            c(y("Nclz::无权录音(跨域，请尝试给iframe添加麦克风访问策略，如{1})", 0, 'allow="camera;microphone"'));
            return;
          }
          /Permission|Allow/i.test(x) ? c(y("gyO5::用户拒绝了录音权限"), !0) : window.isSecureContext === !1 ? c(y("oWNo::浏览器禁止不安全页面录音，可开启https解决")) : /Found/i.test(x) ? c(V + y("jBa9::，无可用麦克风")) : c(V);
        };
        if (i.IsOpen()) {
          S();
          return;
        }
        if (!i.Support()) {
          T("", y("COxc::此浏览器不支持录音"));
          return;
        }
        var E = o.runningContext;
        E || (E = v = i.GetContext(!0));
        var $ = function(x) {
          setTimeout(function() {
            x._call = {};
            var V = i.Stream;
            V && (ie(), x._call = V._call), i.Stream = x, x._c = E, x._RC = o.runningContext, !G() && (i.IsOpen() ? (V && r.CLog(y("upb8::发现同时多次调用open"), 1), re(f, 1), S()) : c(y("Q1GA::录音功能无效：无音频流")));
          }, 100);
        }, j = function(x) {
          var V = x.name || x.message || x.code + ":" + x;
          r.CLog(y("xEQR::请求录音权限错误"), 1, x), T(V, y("bDOG::无法录音：") + V);
        }, D = o.audioTrackSet || {};
        D[d] = E[d];
        var q = { audio: D };
        try {
          var z = i.Scope[g](q, $, j);
        } catch (x) {
          r.CLog(g, 3, x), q = { audio: !0 }, z = i.Scope[g](q, $, j);
        }
        r.CLog(g + "(" + JSON.stringify(q) + ") " + U(E) + y("RiWe::，未配置noiseSuppression和echoCancellation时浏览器可能会自动打开降噪和回声消除，移动端可能会降低系统播放音量（关闭录音后可恢复），请参阅文档中audioTrackSet配置") + "(" + l + ") LM:" + C + " UA:" + navigator.userAgent), z && z.then && z.then($)[I](j);
      },
      close: function(t) {
        t = t || h;
        var n = this, r = n._streamStore();
        n._stop();
        var o = " stream:" + n._streamTag, f = r.Sync;
        if (n._O = 0, n._O_ != f.O) {
          n.CLog(y("hWVz::close被忽略（因为同时open了多个rec，只有最后一个会真正close）") + o, 3), t();
          return;
        }
        f.C++, ie(r), n.CLog("close," + o), t();
      },
      mock: function(t, n) {
        var r = this;
        return r._stop(), r.isMock = 1, r.mockEnvInfo = null, r.buffers = [t], r.recSize = t.length, r._setSrcSR(n), r._streamTag = "mock", r;
      },
      _setSrcSR: function(t) {
        var n = this, r = n.set, o = r[d];
        o > t ? r[d] = t : o = 0, n[m] = t, n.CLog(m + ": " + t + " set." + d + ": " + r[d] + (o ? " " + y("UHvm::忽略") + ": " + o : ""), o ? 3 : 0);
      },
      envCheck: function(t) {
        var n, r = this, o = r.set, f = "CPU_BE";
        if (!n && !i[f] && typeof Int8Array == "function" && !new Int8Array(new Int32Array([1]).buffer)[0] && (Me(f), n = y("Essp::不支持{1}架构", 0, f)), !n) {
          var v = o.type, c = r[v + "_envCheck"];
          o.takeoffEncodeChunk && (c ? t.canProcess || (n = y("7uMV::{1}环境不支持实时处理", 0, t.envName)) : n = y("2XBl::{1}类型不支持设置takeoffEncodeChunk", 0, v) + (r[v] ? "" : y("LG7e::(未加载编码器)"))), !n && c && (n = r[v + "_envCheck"](t, o));
        }
        return n || "";
      },
      envStart: function(t, n) {
        var r = this, o = r.set;
        if (r.isMock = t ? 1 : 0, r.mockEnvInfo = t, r.buffers = [], r.recSize = 0, t && (r._streamTag = "env$" + t.envName), r.state = 1, r.envInLast = 0, r.envInFirst = 0, r.envInFix = 0, r.envInFixTs = [], r._setSrcSR(n), r.engineCtx = 0, r[o.type + "_start"]) {
          var f = r.engineCtx = r[o.type + "_start"](o);
          f && (f.pcmDatas = [], f.pcmSize = 0);
        }
      },
      envResume: function() {
        this.envInFixTs = [];
      },
      envIn: function(t, n) {
        var r = this, o = r.set, f = r.engineCtx;
        if (r.state != 1) {
          r.state || r.CLog("envIn at state=0", 3);
          return;
        }
        var v = r[m], c = t.length, S = i.PowerLevel(n, c), w = r.buffers, _ = w.length;
        w.push(t);
        var F = w, G = _, O = Date.now(), P = Math.round(c / v * 1e3);
        r.envInLast = O, r.buffers.length == 1 && (r.envInFirst = O - P);
        var T = r.envInFixTs;
        T.splice(0, 0, { t: O, d: P });
        for (var E = O, $ = 0, j = 0; j < T.length; j++) {
          var D = T[j];
          if (O - D.t > 3e3) {
            T.length = j;
            break;
          }
          E = D.t, $ += D.d;
        }
        var q = T[1], z = O - E, x = z - $;
        if (x > z / 3 && (q && z > 1e3 || T.length >= 6)) {
          var V = O - q.t - P;
          if (V > P / 5) {
            var N = !o.disableEnvInFix;
            if (r.CLog("[" + O + "]" + se.get(y(N ? "4Kfd::补偿{1}ms" : "bM5i::未补偿{1}ms", 1), [V]), 3), r.envInFix += V, N) {
              var W = new Int16Array(V * v / 1e3);
              c += W.length, w.push(W);
            }
          }
        }
        var L = r.recSize, Y = c, Q = L + Y;
        if (r.recSize = Q, f) {
          var ee = i.SampleData(w, v, o[d], f.chunkInfo);
          f.chunkInfo = ee, L = f.pcmSize, Y = ee.data.length, Q = L + Y, f.pcmSize = Q, w = f.pcmDatas, _ = w.length, w.push(ee.data), v = ee[d];
        }
        var ae = Math.round(Q / v * 1e3), le = w.length, ue = F.length, _e = function() {
          for (var Te = pe ? 0 : -Y, Ie = w[0] == null, Ae = _; Ae < le; Ae++) {
            var ke = w[Ae];
            ke == null ? Ie = 1 : (Te += ke.length, f && ke.length && r[o.type + "_encode"](f, ke));
          }
          if (Ie && f) {
            var Ae = G;
            for (F[0] && (Ae = 0); Ae < ue; Ae++)
              F[Ae] = null;
          }
          Ie && (Te = pe ? Y : 0, w[0] = null), f ? f.pcmSize += Te : r.recSize += Te;
        }, pe = 0, be = "rec.set.onProcess";
        try {
          pe = o.onProcess(w, S, ae, v, _, _e);
        } catch (Te) {
          console.error(be + y("gFUF::回调出错是不允许的，需保证不会抛异常"), Te);
        }
        var Re = Date.now() - O;
        if (Re > 10 && r.envInFirst - O > 1e3 && r.CLog(be + y("2ghS::低性能，耗时{1}ms", 0, Re), 3), pe === !0) {
          for (var Ee = 0, j = _; j < le; j++)
            w[j] == null ? Ee = 1 : w[j] = new Int16Array(0);
          Ee ? r.CLog(y("ufqH::未进入异步前不能清除buffers"), 3) : f ? f.pcmSize -= Y : r.recSize -= Y;
        } else
          _e();
      },
      start: function() {
        var t = this, n = 1;
        if (t.set.sourceStream ? t.Stream || (n = 0) : i.IsOpen() || (n = 0), !n) {
          t.CLog(y("6WmN::start失败：未open"), 1);
          return;
        }
        var r = t._streamCtx();
        if (t.CLog(y("kLDN::start 开始录音，") + U(r) + " stream:" + t._streamTag), t._stop(), t.envStart(null, r[d]), t.state = 3, t._SO && t._SO + 1 != t._S) {
          t.CLog(y("Bp2y::start被中断"), 3);
          return;
        }
        t._SO = 0;
        var o = function() {
          t.state == 3 && (t.state = 1, t.resume());
        }, f = "AudioContext resume: ";
        X(r, function(v) {
          return v && t.CLog(f + "wait..."), t.state == 3;
        }, function(v) {
          v && t.CLog(f + r.state), o();
        }, function(v) {
          t.CLog(f + r.state + y("upkE::，可能无法录音：") + v, 1), o();
        });
      },
      pause: function() {
        var t = this, n = t._streamStore().Stream;
        t.state && (t.state = 2, t.CLog("pause"), n && delete n._call[t.id]);
      },
      resume: function() {
        var t = this, n = t._streamStore().Stream, r = "resume", o = r + "(wait ctx)";
        if (t.state == 3)
          t.CLog(o);
        else if (t.state) {
          t.state = 1, t.CLog(r), t.envResume(), n && (n._call[t.id] = function(v, c) {
            t.state == 1 && t.envIn(v, c);
          }, ne(n));
          var f = t._streamCtx();
          f && X(f, function(v) {
            return v && t.CLog(o + "..."), t.state == 1;
          }, function(v) {
            v && t.CLog(o + f.state), ne(n);
          }, function(v) {
            t.CLog(o + f.state + "[err]" + v, 1);
          });
        }
      },
      _stop: function(t) {
        var n = this, r = n.set;
        n.isMock || n._S++, n.state && (n.pause(), n.state = 0), !t && n[r.type + "_stop"] && (n[r.type + "_stop"](n.engineCtx), n.engineCtx = 0);
      },
      stop: function(t, n, r) {
        var o = this, f = o.set, v, c = o.envInLast - o.envInFirst, S = c && o.buffers.length;
        o.CLog(y("Xq4s::stop 和start时差:") + (c ? c + "ms " + y("3CQP::补偿:") + o.envInFix + "ms envIn:" + S + " fps:" + (S / c * 1e3).toFixed(1) : "-") + " stream:" + o._streamTag + " (" + l + ") LM:" + C);
        var w = function() {
          o._stop(), r && o.close();
        }, _ = function(D) {
          o.CLog(y("u8JG::结束录音失败：") + D, 1), n && n(D), w();
        }, F = function(D, q, z) {
          var x = "blob", V = "arraybuffer", N = "dataType", W = "DefaultDataType", L = o[N] || i[W] || x, Y = N + "=" + L, Q = D instanceof ArrayBuffer, ee = 0, ae = Q ? D.byteLength : D.size;
          if (L == V ? Q || (ee = 1) : L == x ? typeof Blob != "function" ? ee = y.G("NonBrowser-1", [Y]) + y("1skY::，请设置{1}", 0, u + "." + W + '="' + V + '"') : (Q && (D = new Blob([D], { type: q })), D instanceof Blob || (ee = 1), q = D.type || q) : ee = y.G("NotSupport-1", [Y]), o.CLog(y("Wv7l::结束录音 编码花{1}ms 音频时长{2}ms 文件大小{3}b", 0, Date.now() - v, z, ae) + " " + Y + "," + q), ee) {
            _(ee != 1 ? ee : y("Vkbd::{1}编码器返回的不是{2}", 0, f.type, L) + ", " + Y);
            return;
          }
          if (f.takeoffEncodeChunk)
            o.CLog(y("QWnr::启用takeoffEncodeChunk后stop返回的blob长度为0不提供音频数据"), 3);
          else if (ae < Math.max(50, z / 5)) {
            _(y("Sz2H::生成的{1}无效", 0, f.type));
            return;
          }
          t && t(D, z, q), w();
        };
        if (!o.isMock) {
          var G = o.state == 3;
          if (!o.state || G) {
            _(y("wf9t::未开始录音") + (G ? y("Dl2c::，开始录音前无用户交互导致AudioContext未运行") : ""));
            return;
          }
        }
        o._stop(!0);
        var O = o.recSize;
        if (!O) {
          _(y("Ltz3::未采集到录音"));
          return;
        }
        if (!o[f.type]) {
          _(y("xGuI::未加载{1}编码器，请尝试到{2}的src/engine内找到{1}的编码器并加载", 0, f.type, u));
          return;
        }
        if (o.isMock) {
          var P = o.envCheck(o.mockEnvInfo || { envName: "mock", canProcess: !1 });
          if (P) {
            _(y("AxOH::录音错误：") + P);
            return;
          }
        }
        var T = o.engineCtx;
        if (o[f.type + "_complete"] && T) {
          var j = Math.round(T.pcmSize / f[d] * 1e3);
          v = Date.now(), o[f.type + "_complete"](T, function(q, z) {
            F(q, z, j);
          }, _);
          return;
        }
        if (v = Date.now(), !o.buffers[0]) {
          _(y("xkKd::音频buffers被释放"));
          return;
        }
        var E = i.SampleData(o.buffers, o[m], f[d]);
        f[d] = E[d];
        var $ = E.data, j = Math.round($.length / f[d] * 1e3);
        o.CLog(y("CxeT::采样:{1} 花:{2}ms", 0, O + "->" + $.length, Date.now() - v)), setTimeout(function() {
          v = Date.now(), o[f.type]($, function(D, q) {
            F(D, q, j);
          }, function(D) {
            _(D);
          });
        });
      }
    };
    var Se = function(t, n) {
      n.pos || (n.pos = [0], n.tracks = {}, n.bytes = []);
      var r = n.tracks, o = [n.pos[0]], f = function() {
        n.pos[0] = o[0];
      }, v = n.bytes.length, c = new Uint8Array(v + t.length);
      if (c.set(n.bytes), c.set(t, v), n.bytes = c, !n._ht) {
        if (we(c, o), te(c, o), !ce(we(c, o), [24, 83, 128, 103]))
          return;
        for (we(c, o); o[0] < c.length; ) {
          var S = we(c, o), w = te(c, o), _ = [0], F = 0;
          if (!w)
            return;
          if (ce(S, [22, 84, 174, 107])) {
            for (; _[0] < w.length; ) {
              var G = we(w, _), O = te(w, _), P = [0], T = { channels: 0, sampleRate: 0 };
              if (ce(G, [174]))
                for (; P[0] < O.length; ) {
                  var E = we(O, P), $ = te(O, P), j = [0];
                  if (ce(E, [215])) {
                    var D = Ce($);
                    T.number = D, r[D] = T;
                  } else if (ce(E, [131])) {
                    var D = Ce($);
                    D == 1 ? T.type = "video" : D == 2 ? (T.type = "audio", F || (n.track0 = T), T.idx = F++) : T.type = "Type-" + D;
                  } else if (ce(E, [134])) {
                    for (var q = "", z = 0; z < $.length; z++)
                      q += String.fromCharCode($[z]);
                    T.codec = q;
                  } else if (ce(E, [225]))
                    for (; j[0] < $.length; ) {
                      var x = we($, j), V = te($, j);
                      if (ce(x, [181])) {
                        var D = 0, N = new Uint8Array(V.reverse()).buffer;
                        V.length == 4 ? D = new Float32Array(N)[0] : V.length == 8 ? D = new Float64Array(N)[0] : b("WebM Track !Float", 1, V), T[d] = Math.round(D);
                      } else
                        ce(x, [98, 100]) ? T.bitDepth = Ce(V) : ce(x, [159]) && (T.channels = Ce(V));
                    }
                }
            }
            n._ht = 1, b("WebM Tracks", r), f();
            break;
          }
        }
      }
      var W = n.track0;
      if (W) {
        if (W.bitDepth == 16 && /FLOAT/i.test(W.codec) && (W.bitDepth = 32, b("WebM 16->32 bit", 3)), W[d] != n[d] || W.bitDepth != 32 || W.channels < 1 || !/(\b|_)PCM\b/i.test(W.codec))
          return n.bytes = [], n.bad || b("WebM Track Unexpected", 3, n), n.bad = 1, -1;
        for (var L = [], Y = 0; o[0] < c.length; ) {
          var G = we(c, o), O = te(c, o);
          if (!O)
            break;
          if (ce(G, [163])) {
            var Q = O[0] & 15, T = r[Q];
            if (!T)
              b("WebM !Track" + Q, 1, r);
            else if (T.idx === 0) {
              for (var ee = new Uint8Array(O.length - 4), z = 4; z < O.length; z++)
                ee[z - 4] = O[z];
              L.push(ee), Y += ee.length;
            }
          }
          f();
        }
        if (Y) {
          var ae = new Uint8Array(c.length - n.pos[0]);
          ae.set(c.subarray(n.pos[0])), n.bytes = ae, n.pos[0] = 0;
          for (var ee = new Uint8Array(Y), z = 0, le = 0; z < L.length; z++)
            ee.set(L[z], le), le += L[z].length;
          var N = new Float32Array(ee.buffer);
          if (W.channels > 1) {
            for (var ue = [], z = 0; z < N.length; )
              ue.push(N[z]), z += W.channels;
            N = new Float32Array(ue);
          }
          return N;
        }
      }
    }, ce = function(t, n) {
      if (!t || t.length != n.length)
        return !1;
      if (t.length == 1)
        return t[0] == n[0];
      for (var r = 0; r < t.length; r++)
        if (t[r] != n[r])
          return !1;
      return !0;
    }, Ce = function(t) {
      for (var n = "", r = 0; r < t.length; r++) {
        var o = t[r];
        n += (o < 16 ? "0" : "") + o.toString(16);
      }
      return parseInt(n, 16) || 0;
    }, we = function(t, n, r) {
      var o = n[0];
      if (!(o >= t.length)) {
        var f = t[o], v = ("0000000" + f.toString(2)).substr(-8), c = /^(0*1)(\d*)$/.exec(v);
        if (c) {
          var S = c[1].length, w = [];
          if (!(o + S > t.length)) {
            for (var _ = 0; _ < S; _++)
              w[_] = t[o], o++;
            return r && (w[0] = parseInt(c[2] || "0", 2)), n[0] = o, w;
          }
        }
      }
    }, te = function(t, n) {
      var r = we(t, n, 1);
      if (r) {
        var o = Ce(r), f = n[0], v = [];
        if (o < 2147483647) {
          if (f + o > t.length)
            return;
          for (var c = 0; c < o; c++)
            v[c] = t[f], f++;
        }
        return n[0] = f, v;
      }
    }, se = i.i18n = {
      lang: "zh-CN",
      alias: { "zh-CN": "zh", "en-US": "en" },
      locales: {},
      data: {},
      put: function(t, n) {
        var r = u + ".i18n.put: ", o = t.overwrite;
        o = o == null || o;
        var f = t.lang;
        if (f = se.alias[f] || f, !f)
          throw new Error(r + "set.lang?");
        var v = se.locales[f];
        v || (v = {}, se.locales[f] = v);
        for (var c = /^([\w\-]+):/, S, w = 0; w < n.length; w++) {
          var F = n[w];
          if (S = c.exec(F), !S) {
            b(r + "'key:'? " + F, 3, t);
            continue;
          }
          var _ = S[1], F = F.substr(_.length + 1);
          !o && v[_] || (v[_] = F);
        }
      },
      get: function() {
        return se.v_G.apply(null, arguments);
      },
      v_G: function(t, n, r) {
        n = n || [], r = r || se.lang, r = se.alias[r] || r;
        var o = se.locales[r], f = o && o[t] || "";
        return !f && r != "zh" ? r == "en" ? se.v_G(t, n, "zh") : se.v_G(t, n, "en") : (se.lastLang = r, f == "=Empty" ? "" : f.replace(/\{(\d+)(\!?)\}/g, function(v, c, S) {
          return c = +c || 0, v = n[c - 1], (c < 1 || c > n.length) && (v = "{?}", b("i18n[" + t + "] no {" + c + "}: " + f, 3)), S ? "" : v;
        }));
      },
      $T: function() {
        return se.v_T.apply(null, arguments);
      },
      v_T: function() {
        for (var t = arguments, n = "", r = [], o = 0, f = u + ".i18n.$T:", v = /^([\w\-]*):/, c, S = 0; S < t.length; S++) {
          var w = t[S];
          if (S == 0) {
            if (c = v.exec(w), n = c && c[1], !n)
              throw new Error(f + "0 'key:'?");
            w = w.substr(n.length + 1);
          }
          if (o === -1)
            r.push(w);
          else {
            if (o)
              throw new Error(f + " bad args");
            if (w === 0)
              o = -1;
            else if (p(w)) {
              if (w < 1)
                throw new Error(f + " bad args");
              o = w;
            } else {
              var _ = S == 1 ? "en" : S ? "" : "zh";
              if (c = v.exec(w), c && (_ = c[1] || _, w = w.substr(c[1].length + 1)), !c || !_)
                throw new Error(f + S + " 'lang:'?");
              se.put({ lang: _, overwrite: !1 }, [n + ":" + w]);
            }
          }
        }
        return n ? o > 0 ? n : se.v_G(n, r) : "";
      }
    }, y = se.$T;
    y.G = se.get, y("NonBrowser-1::非浏览器环境，不支持{1}", 1), y("IllegalArgs-1::参数错误：{1}", 1), y("NeedImport-2::调用{1}需要先导入{2}", 2), y("NotSupport-1::不支持：{1}", 1), i.TrafficImgUrl = "//ia.51.la/go1?id=20469973&pvFlag=1";
    var Me = i.Traffic = function(t) {
      if (s) {
        t = t ? "/" + u + "/Report/" + t : "";
        var n = i.TrafficImgUrl;
        if (n) {
          var r = i.Traffic, o = /^(https?:..[^\/#]*\/?)[^#]*/i.exec(location.href) || [], f = o[1] || "http://file/", v = (o[0] || f) + t;
          if (n.indexOf("//") == 0 && (/^https:/i.test(v) ? n = "https:" + n : n = "http:" + n), t && (n = n + "&cu=" + encodeURIComponent(f + t)), !r[v]) {
            r[v] = 1;
            var c = new Image();
            c.src = n, b("Traffic Analysis Image: " + (t || u + ".TrafficImgUrl=" + i.TrafficImgUrl));
          }
        }
      }
    };
    H && (b(y("8HO5::覆盖导入{1}", 0, u), 1), H.Destroy()), e[u] = i;
  });
})(Xe);
var Ut = Xe.exports;
const Le = /* @__PURE__ */ Dt(Ut);
(function(a) {
  var e = typeof window == "object" && !!window.document, s = e ? window : Object, h = s.Recorder, p = h.i18n;
  a(h, p, p.$T, e);
})(function(a, e, s, h) {
  a.prototype.enc_pcm = {
    stable: !0,
    fast: !0,
    getTestMsg: function() {
      return s("fWsN::pcm为未封装的原始音频数据，pcm音频文件无法直接播放，可用Recorder.pcm2wav()转码成wav播放；支持位数8位、16位（填在比特率里面），采样率取值无限制");
    }
  };
  var p = function(l) {
    var u = l.bitRate, g = u == 8 ? 8 : 16;
    u != g && a.CLog(s("uMUJ::PCM Info: 不支持{1}位，已更新成{2}位", 0, u, g), 3), l.bitRate = g;
  };
  a.prototype.pcm = function(l, u, g) {
    var m = this.set;
    p(m);
    var d = i(l, m.bitRate);
    u(d.buffer, "audio/pcm");
  };
  var i = function(l, u) {
    if (u == 8)
      for (var g = l.length, m = new Uint8Array(g), d = 0; d < g; d++) {
        var k = (l[d] >> 8) + 128;
        m[d] = k;
      }
    else {
      l = new Int16Array(l);
      var m = new Uint8Array(l.buffer);
    }
    return m;
  };
  a.pcm2wav = function(l, u, g) {
    l.blob || (l = { blob: l });
    var m = l.blob, d = l.sampleRate || 16e3, k = l.bitRate || 16;
    if ((!l.sampleRate || !l.bitRate) && a.CLog(s("KmRz::pcm2wav必须提供sampleRate和bitRate"), 3), !a.prototype.wav) {
      g(s.G("NeedImport-2", ["pcm2wav", "src/engine/wav.js"]));
      return;
    }
    var I = function(M, R) {
      var X;
      if (k == 8) {
        var K = new Uint8Array(M);
        X = new Int16Array(K.length);
        for (var U = 0; U < K.length; U++)
          X[U] = K[U] - 128 << 8;
      } else
        X = new Int16Array(M);
      var B = a({
        type: "wav",
        sampleRate: d,
        bitRate: k
      });
      R && (B.dataType = "arraybuffer"), B.mock(X, d).stop(function(J, re, ne) {
        u(J, re, ne);
      }, g);
    };
    if (m instanceof ArrayBuffer)
      I(m, 1);
    else {
      var H = new FileReader();
      H.onloadend = function() {
        I(H.result);
      }, H.readAsArrayBuffer(m);
    }
  }, a.prototype.pcm_envCheck = function(l, u) {
    return "";
  }, a.prototype.pcm_start = function(l) {
    return p(l), { set: l, memory: new Uint8Array(5e5), mOffset: 0 };
  };
  var C = function(l, u) {
    var g = u.length;
    if (l.mOffset + g > l.memory.length) {
      var m = new Uint8Array(l.memory.length + Math.max(5e5, g));
      m.set(l.memory.subarray(0, l.mOffset)), l.memory = m;
    }
    l.memory.set(u, l.mOffset), l.mOffset += g;
  };
  a.prototype.pcm_stop = function(l) {
    l && l.memory && (l.memory = null);
  }, a.prototype.pcm_encode = function(l, u) {
    if (l && l.memory) {
      var g = l.set, m = i(u, g.bitRate);
      g.takeoffEncodeChunk ? g.takeoffEncodeChunk(m) : C(l, m);
    }
  }, a.prototype.pcm_complete = function(l, u, g, m) {
    if (l && l.memory) {
      m && this.pcm_stop(l);
      var d = l.memory.buffer.slice(0, l.mOffset);
      u(d, "audio/pcm");
    } else
      g(s("sDkA::pcm编码器未start"));
  };
});
(function(a) {
  var e = typeof window == "object" && !!window.document, s = e ? window : Object, h = s.Recorder, p = h.i18n;
  a(h, p, p.$T, e);
})(function(a, e, s, h) {
  a.LibFFT = function(p) {
    var i, C, l, u, g, m, d, k, I = function(M) {
      i = Math.round(Math.log(M) / Math.log(2)), C = 1 << i, l = (C << 2) * Math.sqrt(2), u = [], g = [], m = [0], d = [0], k = [];
      var R, X, K, U;
      for (R = 0; R < C; R++) {
        for (K = R, X = 0, U = 0; X != i; X++)
          U <<= 1, U |= K & 1, K >>>= 1;
        k[R] = U;
      }
      var B, J = 2 * Math.PI / C;
      for (R = (C >> 1) - 1; R > 0; R--)
        B = R * J, d[R] = Math.cos(B), m[R] = Math.sin(B);
    }, H = function(M) {
      var R, X, K, U, B = 1, J = i - 1, re, ne, fe, oe;
      for (R = 0; R != C; R++)
        u[R] = M[k[R]], g[R] = 0;
      for (R = i; R != 0; R--) {
        for (X = 0; X != B; X++)
          for (re = d[X << J], ne = m[X << J], K = X; K < C; K += B << 1)
            U = K + B, fe = re * u[U] - ne * g[U], oe = re * g[U] + ne * u[U], u[U] = u[K] - fe, g[U] = g[K] - oe, u[K] += fe, g[K] += oe;
        B <<= 1, J--;
      }
      X = C >> 1;
      var ie = new Float64Array(X);
      for (ne = l, re = -l, R = X; R != 0; R--)
        fe = u[R], oe = g[R], fe > re && fe < ne && oe > re && oe < ne ? ie[R - 1] = 0 : ie[R - 1] = Math.round(fe * fe + oe * oe);
      return ie;
    };
    return I(p), { transform: H, bufferSize: C };
  };
});
(function(a) {
  var e = typeof window == "object" && !!window.document, s = e ? window : Object, h = s.Recorder, p = h.i18n;
  a(h, p, p.$T, e);
})(function(a, e, s, h) {
  var p = function(l) {
    return new C(l);
  }, i = "FrequencyHistogramView", C = function(l) {
    var u = this, g = {
      /*
      		elem:"css selector" //自动显示到dom，并以此dom大小为显示大小
      			//或者配置显示大小，手动把frequencyObj.elem显示到别的地方
      		,width:0 //显示宽度
      		,height:0 //显示高度
      		
      H5环境以上配置二选一
      		
      		compatibleCanvas: CanvasObject //提供一个兼容H5的canvas对象，需支持getContext("2d")，支持设置width、height，支持drawImage(canvas,...)
      		,width:0 //canvas显示宽度
      		,height:0 //canvas显示高度
      非H5环境使用以上配置
      		*/
      scale: 2,
      fps: 20,
      lineCount: 30,
      widthRatio: 0.6,
      spaceWidth: 0,
      minHeight: 0,
      position: -1,
      mirrorEnable: !1,
      stripeEnable: !0,
      stripeHeight: 3,
      stripeMargin: 6,
      fallDuration: 1e3,
      stripeFallDuration: 3500,
      linear: [0, "rgba(0,187,17,1)", 0.5, "rgba(255,215,0,1)", 1, "rgba(255,102,0,1)"],
      stripeLinear: null,
      shadowBlur: 0,
      shadowColor: "#bbb",
      stripeShadowBlur: -1,
      stripeShadowColor: "",
      fullFreq: !1,
      onDraw: function(K, U) {
      }
    };
    for (var m in l)
      g[m] = l[m];
    u.set = l = g;
    var d = "compatibleCanvas";
    if (l[d])
      var k = u.canvas = l[d];
    else {
      if (!h)
        throw new Error(s.G("NonBrowser-1", [i]));
      var I = l.elem;
      I && (typeof I == "string" ? I = document.querySelector(I) : I.length && (I = I[0])), I && (l.width = I.offsetWidth, l.height = I.offsetHeight);
      var H = u.elem = document.createElement("div");
      H.style.fontSize = 0, H.innerHTML = '<canvas style="width:100%;height:100%;"/>';
      var k = u.canvas = H.querySelector("canvas");
      I && (I.innerHTML = "", I.appendChild(H));
    }
    var M = l.scale, R = l.width * M, X = l.height * M;
    if (!R || !X)
      throw new Error(s.G("IllegalArgs-1", [i + " width=0 height=0"]));
    if (k.width = R, k.height = X, u.ctx = k.getContext("2d"), !a.LibFFT)
      throw new Error(s.G("NeedImport-2", [i, "src/extensions/lib.fft.js"]));
    u.fft = a.LibFFT(1024), u.lastH = [], u.stripesH = [];
  };
  C.prototype = p.prototype = {
    genLinear: function(l, u, g, m) {
      for (var d = l.createLinearGradient(0, g, 0, m), k = 0; k < u.length; )
        d.addColorStop(u[k++], u[k++]);
      return d;
    },
    input: function(l, u, g) {
      var m = this;
      m.sampleRate = g, m.pcmData = l, m.pcmPos = 0, m.inputTime = Date.now(), m.schedule();
    },
    schedule: function() {
      var l = this, u = l.set, g = Math.floor(1e3 / u.fps);
      l.timer || (l.timer = setInterval(function() {
        l.schedule();
      }, g));
      var m = Date.now(), d = l.drawTime || 0;
      if (m - l.inputTime > u.stripeFallDuration * 1.3) {
        clearInterval(l.timer), l.timer = 0, l.lastH = [], l.stripesH = [], l.draw(null, l.sampleRate);
        return;
      }
      if (!(m - d < g)) {
        l.drawTime = m;
        for (var k = l.fft.bufferSize, I = l.pcmData, H = l.pcmPos, M = new Int16Array(k), R = 0; R < k && H < I.length; R++, H++)
          M[R] = I[H];
        l.pcmPos = H;
        var X = l.fft.transform(M);
        l.draw(X, l.sampleRate);
      }
    },
    draw: function(l, u) {
      var g = this, m = g.set, d = g.ctx, k = m.scale, I = m.width * k, H = m.height * k, M = m.lineCount, R = g.fft.bufferSize, X = m.position, K = Math.abs(m.position), U = X == 1 ? 0 : H, B = H;
      K < 1 && (B = B / 2, U = B, B = Math.floor(B * (1 + K)), U = Math.floor(X > 0 ? U * (1 - K) : U * (1 + K)));
      var J = g.lastH, re = g.stripesH, ne = Math.ceil(B / (m.fallDuration / (1e3 / m.fps))), fe = Math.ceil(B / (m.stripeFallDuration / (1e3 / m.fps))), oe = m.stripeMargin * k, ie = 1 << (Math.round(Math.log(R) / Math.log(2) + 3) << 1), A = Math.log(ie) / Math.log(10), b = 20 * Math.log(32767) / Math.log(10), he = R / 2, me = he;
      m.fullFreq || (me = Math.min(he, Math.floor(he * 5e3 / (u / 2))));
      for (var xe = me == he, Se = xe ? M : Math.round(M * 0.8), ce = me / Se, Ce = xe ? 0 : (he - me) / (M - Se), we = 0, te = 0; te < M; te++) {
        var se = Math.ceil(we);
        te < Se ? we += ce : we += Ce;
        var y = Math.ceil(we);
        y == se && y++, y = Math.min(y, he);
        var Me = 0;
        if (l)
          for (var t = se; t < y; t++)
            Me = Math.max(Me, Math.abs(l[t]));
        var n = Me > ie ? Math.floor((Math.log(Me) / Math.log(10) - A) * 17) : 0, r = B * Math.min(n / b, 1);
        J[te] = (J[te] || 0) - ne, r < J[te] && (r = J[te]), r < 0 && (r = 0), J[te] = r;
        var o = re[te] || 0;
        if (r && r + oe > o)
          re[te] = r + oe;
        else {
          var f = o - fe;
          f < 0 && (f = 0), re[te] = f;
        }
      }
      d.clearRect(0, 0, I, H);
      var v = g.genLinear(d, m.linear, U, U - B), c = m.stripeLinear && g.genLinear(d, m.stripeLinear, U, U - B) || v, S = g.genLinear(d, m.linear, U, U + B), w = m.stripeLinear && g.genLinear(d, m.stripeLinear, U, U + B) || S, _ = m.mirrorEnable, F = _ ? M * 2 - 1 : M, G = m.widthRatio, O = m.spaceWidth * k;
      O != 0 && (G = (I - O * (F + 1)) / I);
      for (var te = 0; te < 2; te++) {
        var P = Math.max(1 * k, I * G / F), T = Math.floor(P), E = P - T, $ = (I - F * P) / (F + 1);
        if ($ > 0 && $ < 1)
          G = 1, $ = 0;
        else
          break;
      }
      for (var j = m.minHeight * k, D = _ ? (I - T) / 2 - $ : 0, q = 0; q < 2; q++) {
        q && (d.save(), d.scale(-1, 1));
        var z = q ? I : 0;
        d.shadowBlur = m.shadowBlur * k, d.shadowColor = m.shadowColor;
        for (var te = 0, x = D, V = 0, N, W, L, r; te < M; te++)
          x += $, N = Math.floor(x) - z, L = T, V += E, V >= 1 && (L++, V--), r = Math.max(J[te], j), U != 0 && (W = U - r, d.fillStyle = v, d.fillRect(N, W, L, r)), U != H && (d.fillStyle = S, d.fillRect(N, U, L, r)), x += L;
        if (m.stripeEnable) {
          var Y = m.stripeShadowBlur;
          d.shadowBlur = (Y == -1 ? m.shadowBlur : Y) * k, d.shadowColor = m.stripeShadowColor || m.shadowColor;
          for (var Q = m.stripeHeight * k, te = 0, x = D, V = 0, N, W, L, r; te < M; te++)
            x += $, N = Math.floor(x) - z, L = T, V += E, V >= 1 && (L++, V--), r = re[te], U != 0 && (W = U - r - Q, W < 0 && (W = 0), d.fillStyle = c, d.fillRect(N, W, L, Q)), U != H && (W = U + r, W + Q > H && (W = H - Q), d.fillStyle = w, d.fillRect(N, W, L, Q)), x += L;
        }
        if (q && d.restore(), !_)
          break;
      }
      l && m.onDraw(l, u);
    }
  }, a[i] = p;
});
Le.TrafficImgUrl = "";
Le.CLog = function() {
};
function Bt(a, e, s) {
  const h = ve(!1), p = ve(!0);
  let i;
  var C = 16e3, l = 16, u = 180, g = "pcm", m, d = 0, k, I, H = function(B, J, re) {
    var ne = Date.now();
    if (d == 0 && (d = ne, m = 0, k = 0, I = null), !(!re && ne - d < u)) {
      d = ne;
      var fe = ++k, oe = [], ie = 0;
      if (B.length > 0) {
        for (var A = Le.SampleData(
          B,
          J,
          C,
          I,
          { frameType: re ? "" : g }
        ), b = I ? I.index : 0; b < A.index; b++)
          B[b] = null;
        I = A, oe = A.data, ie = A.sampleRate;
      }
      if (oe.length == 0 || re && oe.length < 2e3) {
        M(fe, null, 0, null, re);
        return;
      }
      if (!re && m >= 2) {
        console.log("编码队列阻塞，已丢弃一帧", 1);
        return;
      }
      m++;
      var he = Date.now(), me = Le({
        type: g,
        sampleRate: C,
        //采样率
        bitRate: l
        //比特率
      });
      me.mock(oe, ie), me.stop(
        function(xe, Se) {
          m && m--, xe.encTime = Date.now() - he, M(fe, xe, Se, me, re);
        },
        function(xe) {
          m && m--, console.log("不应该出现的错误:" + xe, 1);
        }
      );
    }
  }, M = function(B, J, re, ne, fe) {
    if (J) {
      var oe = J, ie = new FileReader();
      ie.onloadend = function() {
        var A = (/.+;\s*base64\s*,\s*(.+)$/i.exec(
          ie.result
        ) || [])[1];
        a(A), oe = null, ie = null;
      }, ie.readAsDataURL(oe);
    }
    fe && console.log(
      "No." + (B < 100 ? ("000" + B).substr(-3) : B) + ":已停止传输"
    );
  };
  const R = Le({
    type: "unknown",
    bitRate: 16,
    sampleRate: 16e3,
    audioTrackSet: {
      deviceId: s || "",
      noiseSuppression: !0,
      //降噪（ANS）开关，不设置时由浏览器控制（一般为默认打开），设为true明确打开，设为false明确关闭
      echoCancellation: !0,
      //回声消除（AEC）开关，取值和降噪开关一样
      autoGainControl: !0
      //自动增益（AGC）开关，取值和降噪开关一样
    },
    onProcess: (B, J, re, ne) => {
      i && i.input(B[B.length - 1], J, ne), H(B, ne, !1), B = [];
    }
  }), X = () => {
    R.open(
      () => {
        console.log("rec.open success"), e.waveId && (i = Le.FrequencyHistogramView(e)), K(), h.value = !0, p.value = !0;
      },
      (B, J) => {
        console.log(B, J), p.value = !1;
      }
    );
  }, K = () => {
    R.start();
  };
  return {
    isUserAllow: p,
    audioOpen: X,
    audioStart: K,
    isOpenAudio: h,
    audioClose: () => {
      R.close(() => {
        console.log("rec.close success");
      });
    }
  };
}
const Nt = /* @__PURE__ */ Oe({
  __name: "index",
  props: {
    config: {}
  },
  emits: ["connect", "close", "extendevent", "endsay", "endAnimation", "startsay", "userSay", "size-change", "realTimerSay"],
  setup(a, { expose: e, emit: s }) {
    const h = a, p = s, i = ve({});
    Z.syncData(h.config), Pe(() => h.config, (A) => {
      Z.syncData(A);
    }, { deep: !0 });
    const C = h.config.pythonUrl, l = ge(() => Z.state.statusConfig.close), u = ge(() => {
      var A;
      return (A = i.value) == null ? void 0 : A.webrtcurl;
    });
    let g = !1, m = null;
    const d = ge(() => h.config.externalControlTime || 1e3), k = () => {
      m && clearTimeout(m), g = !0, m = setTimeout(() => {
        g = !1;
      }, d.value);
    };
    Pe(() => Z.state.statusConfig.init, (A) => {
      A && H();
    }), Pe(() => Z.state.statusConfig.close, (A) => {
      A && (I(), M());
    });
    let I = () => {
      console.log("方法未初始化");
    };
    const H = async () => {
      const A = h.config.waveConfig || {
        elem: ".ctrlProcessWave",
        //渐变色配置：[位置，css颜色，...] 位置: 取值0.0-1.0之间
        lineCount: 10,
        position: 0,
        minHeight: 2,
        fallDuration: 400,
        stripeEnable: !1,
        mirrorEnable: !0,
        linear: [0, "#b0c9f2", 1, "#b0c9f2"],
        waveId: ""
      };
      i.value = Lt(C, h.config);
      const { audioOpen: b, audioClose: he } = Bt((me) => {
        const xe = {
          webPageRawData: me,
          code: 207
        }, Se = h.config.disableAudio;
        (g || Se) && console.log("语音发送被阻止了", g, Se), i.value.ws && Z.state.statusConfig.sendMicrophone && !g && !Se && i.value.sendMessage(xe);
      }, A, h.config.audioDeviceId);
      Z.setWsInfo(i.value), I = he;
      try {
        await navigator.mediaDevices.getUserMedia({ audio: !0 }), b();
      } catch (me) {
        if (console.log(me.message), me.message.includes("Permission denied")) {
          console.error("请求麦克风权限被拒绝，请允许浏览器使用麦克风");
          return;
        }
      }
    }, M = () => {
      var A;
      (A = i.value) == null || A.close();
    };
    return e({
      startOpen: () => {
        Z.startVideoStream();
      },
      startAlpha: () => {
        Z.startAlphaVideoStream();
      },
      changeStreamToAlpha: () => {
        Z.changeStreamToAlpha();
      },
      changeStreamToNormal: () => {
        Z.changeStreamToNormal();
      },
      stopStream: () => {
        Z.closeVideoStream();
      },
      wsInfo: i,
      sendText: async (A) => {
        const b = {
          webPageRawData: A,
          code: 208
        };
        return k(), i.value.ws ? i.value.sendMessage(b, { watch: !0 }) : Promise.reject("未连接");
      },
      sendAudio: async (A, b) => {
        const he = {
          webPageRawData: A,
          code: 207
        };
        return k(), i.value.ws ? i.value.sendMessage(he, { watch: b }) : Promise.reject("未连接");
      },
      sendControls: async (A) => {
        const b = {
          extParams: A,
          code: 211,
          intention: 0
        };
        return k(), i.value.ws ? i.value.sendMessage(b, { watch: !0 }) : Promise.reject("未连接");
      },
      changeConfig: (A) => {
        const b = {
          sceneImageUrl: "https://www.yilanmeta.com/back-api/v1/files/shu999.jpg",
          screenOrientation: "vertical",
          currentScene: "shu999",
          vedioStreamIsTransparent: !0,
          ...A,
          code: 205
        };
        i.value.ws && i.value.sendMessage(b, !0);
      },
      sendArrayMico: (A) => {
        const b = {
          webPageRawData: A,
          code: 206
        };
        i.value.ws && i.value.sendMessage(b);
      },
      updateConifg: (A) => {
        i.value.ws && i.value.sendMessage(A, { update: !0 });
      }
    }), De(() => {
      var A;
      I(), M(), (A = i.value) == null || A.close();
    }), de.$on("userSay", (A, b) => {
      p("userSay", A, b);
    }), de.$on("realTimerSay", (A, b) => {
      p("realTimerSay", A, b);
    }), de.$on("startsay", (A, b) => {
      p("startsay", A, b);
    }), de.$on("endsay", () => {
      p("endsay");
    }), de.$on("endAnimation", () => {
      p("endAnimation");
    }), de.$on("playError", () => {
      p("close");
    }), de.$on("playing", () => {
      p("connect");
    }), de.$on("extParams", (A) => {
      try {
        p("extendevent", JSON.parse(A));
      } catch {
        p("extendevent", A);
      }
    }), de.$on("canvasSize", (A) => {
      console.log("sizeInfo", A), p("size-change", A);
    }), (A, b) => (ze(), et(Mt, {
      stop: l.value,
      webrtcurl: u.value
    }, null, 8, ["stop", "webrtcurl"]));
  }
});
export {
  Nt as default
};
