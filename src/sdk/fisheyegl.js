const vertexSrc = `
#ifdef GL_ES
precision highp float;
#endif
attribute vec3 aVertexPosition;
attribute vec2 aTextureCoord;
varying vec3 vPosition;
varying vec2 vTextureCoord;
void main(void) {
  vPosition = aVertexPosition;
  vTextureCoord = aTextureCoord;
  gl_Position = vec4(vPosition, 1.0);
}
`;
const fragmentSrc = `
#ifdef GL_ES
precision highp float;
#endif
uniform vec3 uLensS;
uniform vec2 uLensF;
uniform vec2 uFov;
uniform sampler2D uSampler;
varying vec3 vPosition;
varying vec2 vTextureCoord;
vec2 GLCoord2TextureCoord(vec2 glCoord) {
  return glCoord  * vec2(1.0, -1.0)/ 2.0 + vec2(0.5, 0.5);
}
void main(void){
  float scale = uLensS.z;
  vec3 vPos = vPosition;
  float Fx = uLensF.x;
  float Fy = uLensF.y;
  vec2 vMapping = vPos.xy;
  vMapping.x = vMapping.x + ((pow(vPos.y, 2.0)/scale)*vPos.x/scale)*-Fx;
  vMapping.y = vMapping.y + ((pow(vPos.x, 2.0)/scale)*vPos.y/scale)*-Fy;
  vMapping = vMapping * uLensS.xy;
  vMapping = GLCoord2TextureCoord(vMapping/scale);
  vec4 texture = texture2D(uSampler, vMapping);
  if(vMapping.x > 0.99 || vMapping.x < 0.01 || vMapping.y > 0.99 || vMapping.y < 0.01){
    texture = vec4(0.0, 0.0, 0.0, 1.0);
  }
  gl_FragColor = texture;
}
`;
export function FisheyeGl(options) {
  // Defaults:
  options = options || {};

  options.width = options.width || 1920;
  options.height = options.height || 1080;

  var model = options.model || {
    vertex: [-1.0, -1.0, 0.0, 1.0, -1.0, 0.0, 1.0, 1.0, 0.0, -1.0, 1.0, 0.0],
    indices: [0, 1, 2, 0, 2, 3, 2, 1, 0, 3, 2, 0],
    textureCoords: [0.0, 0.0, 1.0, 0.0, 1.0, 1.0, 0.0, 1.0]
  };

  var lens = options.lens || {
    a: 1.0,
    b: 1.0,
    Fx: 0.0,
    Fy: 0.0,
    scale: 1.5
  };
  var fov = options.fov || {
    x: 1.0,
    y: 1.0
  };
  var image = options.image || "images/barrel-distortion.png";

  var selector = options.selector || "#canvas";
  var canvas = document.querySelector(selector);
  var gl = canvas.getContext("webgl");

  // 1. 获取设备像素比例
  var devicePixelRatio = window.devicePixelRatio || 1;

  // 2. 调整canvas实际绘制大小
  canvas.width = options.width * devicePixelRatio;
  canvas.height = options.height * devicePixelRatio;

  // 4. 设置WebGL的视口
  gl.viewport(0, 0, canvas.width, canvas.height);

  var program = compileShader(gl, vertexSrc, fragmentSrc);
  gl.useProgram(program);

  var aVertexPosition = gl.getAttribLocation(program, "aVertexPosition");
  var aTextureCoord = gl.getAttribLocation(program, "aTextureCoord");
  var uSampler = gl.getUniformLocation(program, "uSampler");
  var uLensS = gl.getUniformLocation(program, "uLensS");
  var uLensF = gl.getUniformLocation(program, "uLensF");
  var uFov = gl.getUniformLocation(program, "uFov");

  var vertexBuffer, indexBuffer, textureBuffer;

  function createBuffers() {
    vertexBuffer = gl.createBuffer();
    gl.bindBuffer(gl.ARRAY_BUFFER, vertexBuffer);
    gl.bufferData(
      gl.ARRAY_BUFFER,
      new Float32Array(model.vertex),
      gl.STATIC_DRAW
    );
    gl.bindBuffer(gl.ARRAY_BUFFER, null);

    indexBuffer = gl.createBuffer();
    gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, indexBuffer);
    gl.bufferData(
      gl.ELEMENT_ARRAY_BUFFER,
      new Uint16Array(model.indices),
      gl.STATIC_DRAW
    );
    gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, null);

    textureBuffer = gl.createBuffer();
    gl.bindBuffer(gl.ARRAY_BUFFER, textureBuffer);
    gl.bufferData(
      gl.ARRAY_BUFFER,
      new Float32Array(model.textureCoords),
      gl.STATIC_DRAW
    );
    gl.bindBuffer(gl.ARRAY_BUFFER, null);
  }

  createBuffers();

  function compileShader(gl, vertexSrc, fragmentSrc) {
    var vertexShader = gl.createShader(gl.VERTEX_SHADER);
    gl.shaderSource(vertexShader, vertexSrc);
    gl.compileShader(vertexShader);

    _checkCompile(vertexShader);

    var fragmentShader = gl.createShader(gl.FRAGMENT_SHADER);
    gl.shaderSource(fragmentShader, fragmentSrc);
    gl.compileShader(fragmentShader);

    _checkCompile(fragmentShader);

    var program = gl.createProgram();

    gl.attachShader(program, vertexShader);
    gl.attachShader(program, fragmentShader);

    gl.linkProgram(program);

    return program;

    function _checkCompile(shader) {
      if (!gl.getShaderParameter(shader, gl.COMPILE_STATUS)) {
        throw new Error(gl.getShaderInfoLog(shader));
      }
    }
  }
  var video = document.querySelector(options.video || "#videoEle");

  function renderVideo() {
    if (video.readyState >= video.HAVE_ENOUGH_DATA) {
      gl.texImage2D(
        gl.TEXTURE_2D,
        0,
        gl.RGBA,
        gl.RGBA,
        gl.UNSIGNED_BYTE,
        video
      );
    }
  }
  function loadImage(gl, callback, texture) {
    texture = texture || gl.createTexture();

    gl.bindTexture(gl.TEXTURE_2D, texture);

    // gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGBA, gl.RGBA, gl.UNSIGNED_BYTE, img)

    gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.LINEAR);
    gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.LINEAR); //gl.NEAREST is also allowed, instead of gl.LINEAR, as neither mipmap.
    gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_S, gl.CLAMP_TO_EDGE); //Prevents s-coordinate wrapping (repeating).
    gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_T, gl.CLAMP_TO_EDGE); //Prevents t-coordinate wrapping (repeating).
    //gl.generateMipmap(gl.TEXTURE_2D);
    gl.bindTexture(gl.TEXTURE_2D, null);

    if (callback) callback(null, texture);
    return texture;
  }

  function loadImageFromUrl(gl, callback) {
    var texture = gl.createTexture();
    // var img = new Image()
    // img.addEventListener('load', function onload() {
    //   loadImage(gl, callback, texture)
    //   options.width = img.width
    //   options.height = img.height
    // })
    // img.src = url
    loadImage(gl, callback, texture);
    return texture;
  }
  window.run = run;
  let requestAnimationFrameId = null;
  function run(animate, callback) {
    options.runner();
    renderVideo();
    requestAnimationFrameId = window.requestAnimationFrame(run);
  }

  options.runner = function runner(dt) {
    console.log("rtun===>");
    gl.enableVertexAttribArray(aVertexPosition);

    gl.bindBuffer(gl.ARRAY_BUFFER, vertexBuffer);
    gl.vertexAttribPointer(aVertexPosition, 3, gl.FLOAT, false, 0, 0);

    gl.enableVertexAttribArray(aTextureCoord);

    gl.bindBuffer(gl.ARRAY_BUFFER, textureBuffer);
    gl.vertexAttribPointer(aTextureCoord, 2, gl.FLOAT, false, 0, 0);

    gl.activeTexture(gl.TEXTURE0);
    gl.bindTexture(gl.TEXTURE_2D, texture);
    gl.uniform1i(uSampler, 0);

    gl.uniform3fv(uLensS, [lens.a, lens.b, lens.scale]);
    gl.uniform2fv(uLensF, [lens.Fx, lens.Fy]);
    gl.uniform2fv(uFov, [fov.x, fov.y]);

    gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, indexBuffer);
    gl.drawElements(gl.TRIANGLES, model.indices.length, gl.UNSIGNED_SHORT, 0);
  };

  var texture;

  function setImage(imageUrl, callback) {
    texture = loadImageFromUrl(gl, function onImageLoad() {
      run();
    });
  }

  function destroy() {
    window.cancelAnimationFrame(requestAnimationFrameId);
    gl.deleteTexture(texture);
    gl.deleteBuffer(vertexBuffer);
    gl.deleteBuffer(indexBuffer);
    gl.deleteBuffer(textureBuffer);
    gl.deleteProgram(program);
  }

  setImage();

  // external API:
  var distorter = {
    options: options,
    gl: gl,
    lens: lens,
    fov: fov,
    run: run,
    setImage: setImage,
    destroy
  };

  return distorter;
}
