import { createRouter, createWebHashHistory } from "vue-router";

import Main from "../views/main/index.vue";
import Content from "../views/account-process/content.vue";
// import OpenAccount from "../views/open-account/index.vue";
// import IdentityAuthentication from "../views/account-process/identity-authentication.vue";
import IdentityAuthenticationOne from "../views/account-process/identity-authentication/IdCardTips.vue";
import IdentityAuthenticationTwo from "../views/account-process/identity-authentication/IdCardTipsTwo.vue";
import IdentityAuthenticationThree from "../views/account-process/identity-authentication/IdCardTipsThree.vue";
import IdentityAuthenticationFour from "../views/account-process/identity-authentication/IdCardTipsFour.vue";
import InformationOne from "../views/account-process/information-entry/one.vue";
import InformationTwo from "../views/account-process/information-entry/two.vue";
import InformationThree from "../views/account-process/information-entry/three.vue";
import BusinessSubmit from "../views/account-process/business-submit.vue";
// import AgreementSign from "../views/account-process/agreement-sign/index.vue";
import AgreementSignOne from "../views/account-process/agreement-sign/agreement-sign-one.vue";
import AgreementSignTwo from "../views/account-process/agreement-sign/agreement-sign-two.vue";
import WitnessRecordOne from "../views/account-process/witness-record/one.vue";
import BusinessApplicationOne from "../views/account-process/business-application/one.vue";
import BusinessApplicationTwo from "../views/account-process/business-application/two.vue";
import BusinessApplicationThree from "../views/account-process/business-application/three.vue";
import BusinessApplicationFour from "../views/account-process/business-application/four.vue";
import BusinessApplicationFive from "../views/account-process/business-application/five.vue";
import BusinessApplicationSix from "../views/account-process/business-application/six.vue";

import InvestorExhibit from "../views/investor-exhibit/index.vue";
const routes = [
  {
    path: "/main",
    name: "Main",
    component: Main,
    children: []
  },
  // {
  //   path: "/open-account",
  //   name: "OpenAccount",
  //   component: OpenAccount,
  //   children: []
  // },
  // {
  //   path: "/identity-authentication",
  //   name: "IdentityAuthentication",
  //   component: IdentityAuthentication,
  //   children: []
  // },
  {
    path: "/identity-authentication",
    name: "IdentityAuthentication",
    component: Content,
    children: [
      {
        path: "",
        component: IdentityAuthenticationOne
      },
      {
        path: "one",
        component: IdentityAuthenticationOne
      },
      {
        path: "two",
        component: IdentityAuthenticationTwo
      },
      {
        path: "three",
        component: IdentityAuthenticationThree
      },
      {
        path: "four",
        component: IdentityAuthenticationFour
      }
    ]
  },
  {
    path: "/information-entry",
    name: "InformationEntry",
    component: Content,
    children: [
      {
        path: "",
        component: InformationOne
      },
      {
        path: "one",
        component: InformationOne
      },
      {
        path: "two",
        component: InformationTwo
      },
      {
        path: "three",
        component: InformationThree
      }
    ]
  },
  {
    path: "/business-application",
    name: "BusinessApplication",
    component: Content,
    children: [
      {
        path: "",
        component: BusinessApplicationOne
      },
      {
        path: "one",
        component: BusinessApplicationOne
      },
      {
        path: "two",
        component: BusinessApplicationTwo
      },
      {
        path: "three",
        component: BusinessApplicationThree
      },
      {
        path: "four",
        component: BusinessApplicationFour
      },
      {
        path: "five",
        component: BusinessApplicationFive
      },
      {
        path: "six",
        component: BusinessApplicationSix
      }
    ]
  },
  {
    path: "/agreement-sign",
    name: "AgreementSign",
    component: Content,
    children: [
      {
        path: "",
        component: AgreementSignOne
      },
      {
        path: "one",
        component: AgreementSignOne
      },
      {
        path: "two",
        component: AgreementSignTwo
      }
    ]
  },
  {
    path: "/witness-record",
    name: "WitnessRecord",
    component: Content,
    children: [
      {
        path: "",
        component: WitnessRecordOne
      },
      {
        path: "one",
        component: WitnessRecordOne
      }
    ]
  },
  {
    path: "/business-submit",
    name: "BusinessSubmit",
    component: Content,
    children: [
      {
        path: "",
        component: BusinessSubmit
      }
    ]
  },
  {
    path: "/investor-exhibit",
    name: "InvestorExhibit",
    component: InvestorExhibit,
    children: [],
    props: true
  },
  {
    path: "/",
    redirect: "/main"
  }
];

const router = createRouter({
  history: createWebHashHistory(),
  routes
});

router.afterEach((to, from) => {
  const toDepth = to.path.split("/").length;
  const fromDepth = from.path.split("/").length;
  to.meta.transition = toDepth < fromDepth ? "slide-right" : "slide-left";
});

export default router;
