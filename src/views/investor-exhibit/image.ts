import smreadDarkIcon from "@/assets/images/edu/small/read_dark.png";
import smdrawDarkIcon from "@/assets/images/edu/small/draw_dark.png";
import smaudioDarkIcon from "@/assets/images/edu/small/audio_dark.png";
import smvideoDarkIcon from "@/assets/images/edu/small/video_dark.png";

import readLightIcon from "@/assets/images/edu/read_light.png";
import drawLightIcon from "@/assets/images/edu/draw_light.png";
import audioLightIcon from "@/assets/images/edu/audio_light.png";
import videoLightIcon from "@/assets/images/edu/video_light.png";
import readDarkIcon from "@/assets/images/edu/read_dark.png";
import drawDarkIcon from "@/assets/images/edu/draw_dark.png";
import audioDarkIcon from "@/assets/images/edu/audio_dark.png";
import videoDarkIcon from "@/assets/images/edu/video_dark.png";
import readData from "../../assets/lottie/edu/read.json";
import drawData from "../../assets/lottie/edu/draw.json";
import videoData from "../../assets/lottie/edu/video.json";
import audioData from "../../assets/lottie/edu/audio.json";

export const icons: any = {
  sm: {
    read: {
      light: readData,
      dark: smreadDarkIcon
    },
    draw: {
      light: drawData,
      dark: smdrawDarkIcon
    },
    audio: {
      light: audioData,
      dark: smaudioDarkIcon
    },
    video: {
      light: videoData,
      dark: smvideoDarkIcon
    }
  },
  md: {
    read: {
      light: readLightIcon,
      dark: readDarkIcon
    },
    draw: {
      light: drawLightIcon,
      dark: drawDarkIcon
    },
    audio: {
      light: audioLightIcon,
      dark: audioDarkIcon
    },
    video: {
      light: videoLightIcon,
      dark: videoDarkIcon
    }
  }
};

export const types = [
  {
    name: "阅享",
    subtitle: "Reading enjoyment",
    key: "read",
    lottie: icons.sm.read.light
  },
  {
    name: "视享",
    subtitle: "Visual enjoyment",
    key: "video",
    lottie: icons.sm.video.light
  },
  {
    name: "声享",
    subtitle: "Sound enjoyment",
    key: "audio",
    lottie: icons.sm.audio.light
  },
  {
    name: "绘享",
    subtitle: "Information hotspots",
    key: "draw",
    lottie: icons.sm.draw.light
  }
];
types.forEach((item) => {
  item.name = "财·" + item.name;
});

export const iconTypes = ["light", "dark"];
