import { defineStore } from "pinia";
import { Content, Menu, eduApis } from "./api";
import { types } from "./image";

const useEduStore = defineStore("eduStore", {
  actions: {
    init() {
      eduApis.fetchMenus().then((data) => {
        this.menuList = data.sort(
          (a, b) =>
            types.findIndex((type) => type.name === a.name) -
            types.findIndex((type) => type.name === b.name)
        );
        this.menuList = this.menuList.map((menu) => {
          if (!menu.children) {
            return menu;
          }
          const menuChild = menu.children.sort(
            (a, b) => a.sequence - b.sequence
          );
          return {
            ...menu,
            children: menuChild
          };
        });
        this.showMenuIds = this.menuList.map((menu: any) => ({
          parentId: menu.id,
          id: menu.children[0]?.id
        }));
        this.updateContents();
      });
    },

    updateShowMenuIds(menuId: number, id: number) {
      this.showMenuIds = this.showMenuIds.map((item) => {
        if (item.parentId === menuId) {
          return {
            parentId: menuId,
            id
          };
        }
        return item;
      });
    },
    updateContents() {
      this.showMenuIds.forEach(({ id }) => {
        this.updateContentById(id);
      });
    },
    updateContentById(id: number) {
      this.contents[id] = {
        id,
        loading: true,
        list: []
      };
      eduApis.fetchContent(id).then(({ data }) => {
        this.contents[id] = {
          id,
          loading: false,
          list: data.data.records
        };
      });
    },
    // pauseMedia(flag: string) {
    //   this.mediaFlag = flag;
    // },
    updateActiveContent(content: any) {
      this.activeContent = content;
    },
    updateCurrentContentIndex(key: any, val?: any) {
      if (typeof key === "string" && key === "reset") {
        this.currentContentIndex = [0, 0];
        return;
      }
      this.currentContentIndex[key] = val;
    },
    updateAudioRefInit(flag: boolean) {
      this.audioRefInit = flag;
    }
  },
  state() {
    return {
      // mediaFlag: "" as string,
      audioRefInit: false as boolean,
      currentContentIndex: [0, 0] as number[],
      activeContent: null as Content | null,
      menuList: [] as Menu[],
      showMenuIds: [] as { id: number; parentId: number }[],
      contents: {} as {
        [k: string]: {
          id: number;
          loading: boolean;
          list: Content[];
        };
      }
    };
  }
});

export default useEduStore;
