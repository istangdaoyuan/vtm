import { ref, onUnmounted, onMounted, computed } from "vue";
import eventbus from "../../../utils/eventbus";
import { useRouter } from "vue-router";

export function useAudioText(allText: string) {
  const chunks = ref([]);
  const textChunks = allText
    .split("。")
    .map((item) => {
      let result = [];
      if (item.length > 22) {
        // 按照22个字分割
        const sentenceList = item.split("");
        let currentChunk = "";
        sentenceList.forEach((word: string, index: any) => {
          if (currentChunk.length + word.length > 22) {
            result.push(currentChunk);
            currentChunk = word;
          } else {
            currentChunk += word;
          }
        });
        if (currentChunk) {
          result.push(currentChunk);
        }
        return result;
      } else {
        return item;
      }
    })
    .flat(2);
  // const textChunks = splitText(allText, 22);
  // 22字  5s
  // const textAllTime = (allText.length / 22) * 5;
  // console.log(textChunks, 'textChunkstextChunkstextChunks');
  const currentIndex = ref(0);
  // function splitText(text: string, maxLength: number) {
  //   const sentences = text.match(/[^。！？]+[。！？]?/g) || [];

  //   let currentChunk = "";
  //   sentences.forEach((sentence: string) => {
  //     const sentenceList = sentence.split("");
  //     sentenceList.forEach((word: string, index: any) => {
  //       if (currentChunk.length + word.length > maxLength) {
  //         chunks.value.push(currentChunk);
  //         currentChunk = word;
  //       } else {
  //         currentChunk += word;
  //       }
  //     });
  //   });
  //   if (currentChunk) {
  //     chunks.value.push(currentChunk);
  //   }

  //   return chunks.value;
  // }
  const readIng = ref(false);
  onMounted(() => {
    eventbus.$on("humanSayEnd", readNextChunk);
  });
  onUnmounted(() => {
    eventbus.$off("humanSayEnd", readNextChunk);
  });
  const router = useRouter();

  // const humanInteach = computed(
  //   () => router.currentRoute.value.path === "/investor-exhibit"
  // );
  // const read = (text: string) => {
  //   // 判断当前路由是不是正确的
  //   if (humanInteach.value === false) {
  //     return;
  //   }
  //   eventbus.$emit("breakHumanSay");
  //   eventbus.$emit("say", text);
  // };
  let timer: any;
  let readySay = false;
  const startRead = () => {
    readySay = true;
    currentIndex.value = 0;
    // read(allText);
    // timer = setInterval(() => {
    //   currentIndex.value++;
    //   if (currentIndex.value >= chunks.value.length - 1) {
    //     readIng.value = false;
    //   } else { }
    // }, 5200);
  };
  eventbus.$on("humanSayStart", () => {
    if (!readySay) return;
    readySay = false;
    readIng.value = true;
    readText(textChunks[currentIndex.value]);
  });
  const readText = (text: string) => {
    if (!text) {
      console.error("没有传入有效的text", text);
      text = "";
    }
    // 清除所有的中文、英文标点符号
    const pureText = text.replace(/[^\u4e00-\u9fa5a-zA-Z0-9]/g, "");
    const time = pureText.length * 260;
    timer = setTimeout(() => {
      currentIndex.value++;
      const nextText = textChunks[currentIndex.value];
      readText(nextText);
    }, time);
  };
  const stopRead = () => {
    currentIndex.value = 0;
    // eventbus.$emit("breakHumanSay");
    readIng.value = false;
    timer && clearTimeout(timer);
  };
  function readNextChunk() {
    readIng.value = false;
  }

  return {
    textChunks,
    startRead,
    stopRead,
    currentIndex
  };
}
