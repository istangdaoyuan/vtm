import { getInvestorMenuList, getInvestorContent } from "../../api";
export enum ContentType {
  /** 图文 */
  TEXT_N_IMG = 0,
  LONG_IMG = 1,
  VIDEO = 2,
  DOC = 3,
  AUDIO = 4
}
export enum ImageType {
  VERTICAL = 0,
  HORIZONTAL = 1
}
export interface Content {
  id: number;
  title: string;
  /** 0:图文，1:长图、2:视频、3:文档，4:音频 */
  type: ContentType;
  menuId: number;
  status?: any;
  sequence: number;
  /** 竖屏样式（0:样式1，1:样式2） */
  portraitType: number;
  /** 横屏样式（0:样式1，1:样式2） */
  landscapeType: number;
  /** 长图 url */
  longImg?: string;
  docUrl?: string;
  content: string;
  audioUrl?: string;
  /** 长图类型样式（0:竖向长图，1:横向长图） */
  picsContentType?: ImageType;
  videoUrl?: string;
  images: string;
  deleted?: number;
  updateTime: string;
  createTime: string;
  creator?: any;
  updator?: any;
}
interface MenuItem {
  id: number;
  name: string;
  level: number;
  parentId: number;
  sequence: number;
  createTime: string;
  updateTime: string;
  updator?: any;
  creator?: any;
}
export interface Menu extends MenuItem {
  children?: MenuItem[];
}

export const eduApis = {
  fetchContent(id: number) {
    return getInvestorContent(id);
  },
  fetchMenus() {
    return getInvestorMenuList({
      level: 0
    }).then((res: any) => {
      // 投资者教育
      const ROOT_ID = 2;
      const { data } = res.data;
      // convert res.data to tree struct by parentId
      data.forEach((cur: any) => {
        const parent = data.find(
          (item: any) => item.id === cur.parentId
        ) as Menu;
        if (parent) {
          if (!parent.children) {
            parent.children = [];
          }
          parent.children.push(cur);
        }
      });
      return (
        (data.find((item: any) => item.id === ROOT_ID) as Menu)?.children || []
      );
    });
  }
};
