import { onMounted, onBeforeUnmount, ref } from "vue";
import eventbus from "../../utils/eventbus";

// openSystem 打开投资者教育
// demonstrate 演示开户
export function useHumanSay(openSystem: () => void, demonstrate: () => void) {
  const needOpenSystem = ref(false);
  const needDemonstrate = ref(false);
  onMounted(() => {
    eventbus.$on("userSay", userSay);
    eventbus.$on("humanSayEnd", humanSayEnd);
  });
  onBeforeUnmount(() => {
    eventbus.$off("humanSayEnd", humanSayEnd);
    eventbus.$off("userSay", userSay);
  });
  const userSay = (text: string) => {
    console.log(text);
    // // 不是等待回答  跳过
    // if (!this.waitAnswer) return;
    // if (this.isSaying) {
    //   console.log("机器人播报中，不接受用户回答" + text);
    //   return;
    // }
    if (text.includes("投资者教育")) {
      // 播放动画打开投资者教育
      eventbus.$emit("say", "好的，这就为您打开", "OpenSystem");
      needOpenSystem.value = true;
    }
    if (
      text.includes("开户") ||
      text.includes("演示") ||
      text.includes("业务办理")
    ) {
      eventbus.$emit("say", "好的，这就为您演示", "Demonstrate");
      // 切换到开户页面
      needDemonstrate.value = true;
    }
  };
  const humanSayEnd = () => {
    if (needDemonstrate.value) {
      demonstrate();
      needDemonstrate.value = false;
    }
    if (needOpenSystem.value) {
      openSystem();
      needOpenSystem.value = false;
    }
  };
}
