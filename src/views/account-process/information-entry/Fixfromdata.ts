import { computed, ref } from "vue";

export function Fixfromdata(customerInformation: any) {
  const error = ref("");

  function computedErrorTip() {
    error.value = "";
    if (customerInformation.idCardType === "身份证") {
      if (
        !/(^\d{15}$)|(^\d{17}([0-9]|X)$)/.test(customerInformation.idCardNo)
      ) {
        error.value = "身份证号码不合法";
      }
    }

    if (!/^\d+$/.test(customerInformation.postalCode)) {
      error.value = "邮政编码必须为数字";
    }
    if (!/^\d+(\.\d{1})?$/.test(customerInformation.annualIncome)) {
      error.value = "年收入必须为数字，最多一位小数";
    }
  }
  return {
    error,
    computedErrorTip
  };
}
