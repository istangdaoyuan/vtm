import idNumberArea from "./idNumberArea.json";
// 根据身份证号前6位获取省市区
export function getAreaByCode(idCard: string) {
  const areaCode = idCard.substring(0, 6);
  const result = idNumberArea.find((item) => item.code + "" === areaCode);
  if (result) {
    return result.address;
  }
}
