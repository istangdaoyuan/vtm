export const idCardTypes = [
  {
    label: "身份证",
    value: "身份证"
  },
  {
    label: "护照",
    value: "护照"
  },
  {
    label: "军官证",
    value: "军官证"
  },
  {
    label: "港澳通行证",
    value: "港澳通行证"
  }
];

export const emailAddresses = [
  {
    label: "@qq.com",
    value: "@qq.com"
  },
  {
    label: "@163.com",
    value: "@163.com"
  },
  {
    label: "@sina.com",
    value: "@sina.com"
  },
  {
    label: "@126.com",
    value: "@126.com"
  },
  {
    label: "@sohu.com",
    value: "@sohu.com"
  },
  {
    label: "@aliyun.com",
    value: "@aliyun.com"
  }
];
export const secondContactRelations = [
  { label: "夫妻", value: "夫妻" },
  { label: "父子(父女)", value: "父子(父女)" },
  { label: "母子(母女)", value: "母子(母女)" },
  { label: "父亲(母亲)", value: "父亲(母亲)" },
  { label: "兄弟姐妹", value: "兄弟姐妹" },
  { label: "其他", value: "其他" }
];
export const educationBackgrounds = [
  { label: "小学", value: "小学" },
  { label: "初中", value: "初中" },
  { label: "高中", value: "高中" },
  { label: "专科", value: "专科" },
  { label: "本科", value: "本科" },
  { label: "硕士研究生", value: "硕士研究生" },
  { label: "博士研究生", value: "博士研究生" },
  { label: "其他", value: "其他" }
];

export const professionals = [
  { value: "国际组织工作人员", label: "国际组织工作人员" },
  { value: "生产制造及有关人员", label: "生产制造及有关人员" },
  {
    value: "其他社会生产和社会服务人员",
    label: "其他社会生产和社会服务人员"
  },
  { value: "居民、健康服务人员", label: "居民、健康服务人员" },
  { value: "废品、旧货回收服务人员", label: "废品、旧货回收服务人员" },
  {
    value: "艺术品或文物收藏行业服务人员",
    label: "艺术品或文物收藏行业服务人员"
  },
  { value: "典当、拍卖行业服务人员", label: "典当、拍卖行业服务人员" },
  {
    value: "珠宝、黄金等贵金属行业服务人员",
    label: "珠宝、黄金等贵金属行业服务人员"
  },
  {
    value: "旅游、住宿和餐饮服务人员",
    label: "旅游、住宿和餐饮服务人员"
  },
  {
    value: "人民警察、消防、应急救援人员",
    label: "人民警察、消防、应急救援人员"
  },
  {
    value: "民主党派、工商联、人民团体或社会组织等单位工作人员",
    label: "民主党派、工商联、人民团体或社会组织等单位工作人员"
  },
  {
    value: "党政机关、企事业单位行政工作人员",
    label: "党政机关、企事业单位行政工作人员"
  },
  { value: "其他专业技术人员", label: "其他专业技术人员" },
  { value: "宗教人士等特殊职业人员", label: "宗教人士等特殊职业人员" },
  {
    value: "法律、会计、审计、税务专业人员",
    label: "法律、会计、审计、税务专业人员"
  },
  { value: "工程、农业专业人员", label: "工程、农业专业人员" },
  { value: "科学研究及教学人员", label: "科学研究及教学人员" },
  {
    value:
      "社会组织(社会团体、基金会、社会服务机构、外国商会等)负责人及管理人员",
    label:
      "社会组织(社会团体、基金会、社会服务机构、外国商会等)负责人及管理人员"
  },
  {
    value: "人民团体或群众团体负责人及管理人员",
    label: "人民团体或群众团体负责人及管理人员"
  },
  {
    value: "民主党派和工商联负责人及管理人员",
    label: "民主党派和工商联负责人及管理人员"
  },
  {
    value: "交通运输、仓储、邮政业服务人员",
    label: "交通运输、仓储、邮政业服务人员"
  },
  { value: "新闻出版、文化专业人员", label: "新闻出版、文化专业人员" },
  { value: "文学艺术、体育专业人员", label: "文学艺术、体育专业人员" },
  { value: "卫生专业技术人员", label: "卫生专业技术人员" },
  { value: "房地产服务人员", label: "房地产服务人员" },
  {
    value: "文化、体育和娱乐服务人员",
    label: "文化、体育和娱乐服务人员"
  },
  {
    value: "信息运输、软件和信息技术服务人员",
    label: "信息运输、软件和信息技术服务人员"
  },
  { value: "批发与零售服务人员", label: "批发与零售服务人员" },
  {
    value: "经济和金融专业人员(证券从业人员除外)",
    label: "经济和金融专业人员(证券从业人员除外)"
  },
  { value: "离退休人员", label: "离退休人员" },
  { value: "证券从业人员", label: "证券从业人员" },
  { value: "学生", label: "学生" },
  { value: "军人", label: "军人" },
  { value: "无业", label: "无业" },
  {
    value: "个体工商户 (含淘宝店自营等)",
    label: "个体工商户 (含淘宝店自营等)"
  },
  {
    value: "农、林、牧、渔业生产及辅助人员",
    label: "农、林、牧、渔业生产及辅助人员"
  },
  {
    value: "企事业单位负责人及管理人员",
    label: "企事业单位负责人及管理人员"
  },
  {
    value: "党政机关负责人及管理人员",
    label: "党政机关负责人及管理人员"
  }
];
export const annualIncomes = [
  { value: "10w以下", label: "10w以下" },
  { value: "10~30w", label: "10~30w" },
  { value: "30~50w", label: "30~50w" },
  { value: "50~80w", label: "50~80w" },
  { value: "80w以上", label: "80w以上" }
];

export const customerInformation = {
  countyLevelCity: "",
  beneficiary: "",
  badRecord: "",
  chinaPeople: "",
  userName: "",
  idCardType: "",
  idCardNo: "",
  emailName: "",
  emailAddress: "",
  idCardAddress: "",
  province: "",
  city: "",
  district: "",
  addressDetail: "",
  postalCode: "",
  secondContactName: "",
  secondContactPhone: "",
  secondContactRelation: "",
  socialRelationType: "",
  educationBackground: "",
  occupation: "",
  OccupationalNotes: "",
  jobTitle: "",
  annualIncome: "",
  companyName: ""
};
