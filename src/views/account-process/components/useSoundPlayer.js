import diMp3 from "./di.mp3";

export function useSoundPlayer() {
  const audio = new Audio(diMp3);
  audio.addEventListener("ended", () => {
    audio.currentTime = 0;
  });
  const playSound = () => {
    audio.play();
  };

  return { playSound };
}
