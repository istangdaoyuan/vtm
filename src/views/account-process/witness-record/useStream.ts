import { onUnmounted, ref, reactive, onBeforeUnmount } from "vue";
import { fixWebmDuration } from "@fix-webm-duration/fix";
import { apiPostFile } from "../../../api";
import { useAppStore } from "../../../store";

const getDevices = async () => {
  const devices = await navigator.mediaDevices.enumerateDevices();
  return devices;
};

export function useStream() {
  const recordVideoPlay = reactive({
    play: false,
    show: true
  });
  const appStore = useAppStore();

  const isdev = process.env.NODE_ENV === "development";
  const targetCameraName = isdev
    ? window.config.hardware.mac
    : window.config.hardware.baseCamera;
  const bufferMy = ref<Blob[]>([]);
  const cameraStream = ref<MediaStream | null>(null);
  let localstream: MediaStream | null = null;
  let targetCamera;
  let audioContext: AudioContext | null;
  let cameraAudioSource: MediaStreamAudioSourceNode | null;
  let videoAudioSource: MediaStreamAudioSourceNode | null;
  let audioDestination: MediaStreamAudioDestinationNode | null;
  let recorderAv: MediaRecorder | null;
  let recorderAvFlag = ref(false);
  let duration = 0;
  let userInfo = {
    id: "",
    name: ""
  };

  const setUser = (id: string, name: string) => {
    userInfo.id = id;
    userInfo.name = name;
  };

  const getVideoStream = async () => {
    const devices = await getDevices();
    if (devices.length > 0) {
      targetCamera = devices.find(
        (device) => device.label === targetCameraName
      );
      if (!targetCamera) {
        console.error(
          `没有发现${(window as any).config.hardware.baseCamera}摄像头`
        );
        return;
      }
    } else {
      console.log("没有发现摄像头");
      return;
    }
    const audioInputDevices = devices.find(
      (device) => device.label === window.config.hardware.audioSay
    );
    // 获取摄像头和麦克风的流
    const constraints = {
      audio: {
        deviceId: audioInputDevices?.deviceId || "",
        noiseSuppression: true,
        echoCancellation: true
      },
      video: {
        deviceId: targetCamera.deviceId
      }
    };
    cameraStream.value = await navigator.mediaDevices.getUserMedia(constraints);

    // 获取视频元素的流，记录数字人语音
    const videoElement = document.querySelector(".sdk-video-video");
    const videoStream = (videoElement as any).captureStream();

    // 创建一个AudioContext用于混合音频
    audioContext = new AudioContext();

    // 创建两个源 - 一个来自localstream，一个来自videoStream
    cameraAudioSource = audioContext.createMediaStreamSource(
      cameraStream.value
    );
    videoAudioSource = audioContext.createMediaStreamSource(videoStream);

    // 创建一个目标用于混合音频
    audioDestination = audioContext.createMediaStreamDestination();

    // 将两个源连接到目标
    // 将两个音频源连接到目标
    cameraAudioSource.connect(audioDestination);
    videoAudioSource.connect(audioDestination);
    // 创建一个新的媒体流，包含摄像头的视频和混合后的音频
    localstream = new MediaStream([
      ...cameraStream.value.getVideoTracks(), // 摄像头的视频轨道
      ...audioDestination.stream.getAudioTracks() // 混合后的音频轨道
    ]);
    // 设置视频元素的源为混合后的流
    const videoplay = document.querySelector("#video");
    (videoplay as any).srcObject = localstream.clone();
  };

  const record = async () => {
    // 真正开始录制
    bufferMy.value.length = 0;
    const options = {
      mimeType: "video/webm;code=vp8"
    };
    try {
      if (!localstream) {
        console.error("录制失败：没有可用的媒体流");
        return;
      }
      recorderAv = new MediaRecorder(localstream, options);
      recorderAvFlag.value = true;
    } catch (error) {
      console.error(error, "失败");
      return;
    }
    // 当数据有效时触发的事件
    recorderAv.ondataavailable = function (e) {
      if (e && e.data && e.data.size > 0) {
        bufferMy.value.push(e.data);
      }
    };
    recorderAv.onerror = (event) => {
      console.error("录制错误:", event);
    };
    recorderAv.onstart = (event) => {
      console.log("录制开始", new Date().getTime());
    };
    recorderAv.start();
    console.log("开始录制", new Date().getTime());
    duration = new Date().getTime();
  };

  const uploadFileBlob = async (fileBolb: Blob) => {
    const formData = new FormData();
    const time = new Date().getTime() - duration;
    const fixBolb = await fixWebmDuration(fileBolb, time);
    formData.append("file", fixBolb);
    const res = await apiPostFile(formData);
    console.log(res);
    const url = res && res.data && res.data.data;
    appStore.uploadVideo(userInfo.id, url, userInfo.name);
  };

  const endRecord = () => {
    console.log("录制结束");
    recorderAv!.onstop = () => {
      setTimeout(async () => {
        await becomeVideo();
        cleanupResources();
      }, 10);
    };
    recorderAv!.stop();
  };

  // 将bufferMy生成mp4 给video
  const becomeVideo = async () => {
    const video: HTMLVideoElement | null = document.querySelector("#video");
    const blob = new Blob(bufferMy.value, { type: "video/webm" });
    // const readableStream = blob.stream();
    // uploadFile(blob);
    uploadFileBlob(blob);
    video!.srcObject = null;
    video!.src = window.URL.createObjectURL(blob);
    video!.controls = false;
    // video!.play();
    video!.pause();
    recordVideoPlay.show = true;
    video!.onended = () => {
      recordVideoPlay.play = false;
      recordVideoPlay.show = true;
    };
  };

  const stopMediaTracks = (stream: MediaStream) => {
    if (stream && stream.getTracks) {
      stream.getTracks().forEach((track) => {
        console.log(`Stopping track: ${track.kind}`);
        track.stop();
      });
    } else {
      console.warn("No tracks found in the provided media stream.");
    }
  };

  const cleanupResources = async () => {
    console.log("Cleaning up resources...");

    // 停止摄像头和音频的媒体轨道
    if (cameraStream.value) {
      stopMediaTracks(cameraStream.value);
    } else {
      console.warn("No cameraStream found.");
    }

    if (localstream) {
      stopMediaTracks(localstream);
    } else {
      console.warn("No localstream found.");
    }

    // 断开音频上下文中的连接并关闭它
    if (cameraAudioSource) {
      cameraAudioSource.disconnect();
      console.log("Disconnected cameraAudioSource.");
    } else {
      console.warn("No cameraAudioSource found.");
    }

    if (videoAudioSource) {
      videoAudioSource.disconnect();
      console.log("Disconnected videoAudioSource.");
    } else {
      console.warn("No videoAudioSource found.");
    }

    if (audioDestination) {
      audioDestination.disconnect();
      console.log("Disconnected audioDestination.");
    } else {
      console.warn("No audioDestination found.");
    }

    if (audioContext) {
      await audioContext.close();
      console.log("Closed audioContext.");
    } else {
      console.warn("No audioContext found.");
    }

    // 清理录制器
    if (recorderAv) {
      recorderAv.stop();
      recorderAv.stream.getTracks().forEach((track) => track.stop());
      console.log("Stopped recorderAv and its tracks.");
    } else {
      console.warn("No recorderAv found.");
    }

    recorderAv = null;
    recorderAvFlag.value = false;

    // 清理变量
    cameraStream.value = null;
    localstream = null;
    audioContext = null;
    cameraAudioSource = null;
    videoAudioSource = null;
    audioDestination = null;
    bufferMy.value.length = 0;

    console.log("Resources cleaned up.");
  };

  onBeforeUnmount(() => {
    const videoplay = document.querySelector("#video");
    if (videoplay) {
      (videoplay as any).srcObject = null;
    }
  });
  onUnmounted(() => {
    console.log("onUnmounted===>cleanupResources");
    cleanupResources();
  });

  const playRecordVideo = () => {
    const video: HTMLVideoElement | null = document.querySelector("#video");
    if (video!.paused) {
      // 暂停状态
      video!.play();
      recordVideoPlay.play = true;
      video!.muted = false;
      setTimeout(() => {
        recordVideoPlay.show = false;
      }, 300);
    } else {
      recordVideoPlay.play = false;
      recordVideoPlay.show = true;
      video?.pause();
    }
  };

  const startRecord = async () => {
    await getVideoStream();
    // 保存视频流到bufferMy
    record();
  };

  return {
    endRecord,
    startRecord,
    cleanupResources,
    playRecordVideo,
    recordVideoPlay,
    recorderAvFlag,
    setUser
  };
}

// 使用 Response 对象将 ReadableStream 转换为 Blob
async function streamToBlob(readableStream: ReadableStream) {
  const response = new Response(readableStream);
  const blob = await response.blob();
  return blob;
}
