/*
 * @Author: fanjs
 * @Date: 2023-12-17 16:31:26
 * @Description: 
 */

export const drawImage = (ctx: CanvasRenderingContext2D, sign: any) => {
  const { image, w, h } = sign;
  const x = Math.floor(sign.x);
  const y = Math.floor(sign.y);
  if (!image) return;

  // 边框不画
  // drawCross(ctx, x, y, w, h);
  // 如果加载过直接渲染
  if (image.complete) {
    ctx.drawImage(image, x, y, w, h);
    // ctx.restore();
  } else {
    // 渲染数据
    // eslint-disable-next-line func-names
    image.onload = function () {
      ctx.drawImage(image, x, y, w, h);
    };
  }
};
