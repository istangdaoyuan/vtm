import { computed, ref } from "vue";
import { apiPostFile } from "../../../api";

// 使用指定摄像头 读取摄像头视频流数据
export function useAssignCamera(id?: string) {
  const videoRef = ref<HTMLVideoElement | null>(null);
  const streamRef = ref<MediaStream | null>(null);
  const recorderRef = ref<MediaRecorder | null>(null);
  const saveRef = ref<boolean>(true);
  const hasStream = computed(() => !!streamRef.value);
  // 获取摄像头设备
  async function getDevices() {
    const devices = await navigator.mediaDevices.enumerateDevices();
    const videoList = devices.filter((device) => device.kind === "videoinput");
    return videoList;
  }

  // 初始化摄像头 获取特定摄像头的流
  const initCamera = async (cameraId: string) => {
    const constraints = {
      audio: {
        noiseSuppression: true,
        echoCancellation: true,
        deviceId: window.config.hardware.audioRecoredId
      },
      video: cameraId ? { deviceId: cameraId } : false
    };
    try {
      const stream = await navigator.mediaDevices.getUserMedia(constraints);
      streamRef.value = stream;
      if (id) {
        const video: HTMLVideoElement | null = document.getElementById(
          id
        ) as HTMLVideoElement;
        // console.log(id, "==========", video, streamRef.value);
        video.srcObject = stream;
      }
    } catch (error) {
      console.log("error", JSON.stringify(error, null, 2));
      console.error(error);
    }
  };

  // 释放摄像头
  const releaseCamera = (save: boolean) => {
    saveRef.value = save;
    if (recorderRef.value) {
      recorderRef.value.stop();
      recorderRef.value = null;
    }
    if (streamRef.value) {
      // console.log("close all tracks");
      streamRef.value.getTracks().forEach((track) => track.stop());
      streamRef.value = null;
    }
  };

  // 切换摄像头
  const switchCamera = async (cameraLabel: string) => {
    try {
      const cameras = await getDevices();
      if (cameras.length > 0) {
        const targetCamera = cameras.find(
          (camera) => camera.label === cameraLabel
        );
        if (!targetCamera) {
          console.error(`没有发现${cameraLabel}摄像头`);
          return;
        }
        releaseCamera(false);
        await initCamera(targetCamera.deviceId);
        // 使用 stream
      } else {
        console.log("ERROR：没有发现摄像头");
      }
    } catch (error) {
      console.error("获取摄像头失败:", error);
    }
  };
  async function startRecording(cameraLabel: string) {
    try {
      if (!streamRef.value) {
        await switchCamera(cameraLabel);
      }
      // 创建MediaRecorder实例
      recorderRef.value = new MediaRecorder(streamRef.value!);
      // 存储录制的数据块
      let chunks: BlobPart[] = [];

      // 当数据可用时，添加到chunks数组
      recorderRef.value.ondataavailable = (event) => chunks.push(event.data);

      // 录制停止时，处理录制的数据
      recorderRef.value.onstop = () => {
        if (!saveRef.value) {
          return;
        }
        uploadVideo(chunks);
      };

      // 开始录制
      recorderRef.value.start();

      // 停止录制示例（实际应用中需要根据具体场景触发）
      //   setTimeout(() => recorder.stop(), 5000); // 5秒后停止录制
    } catch (error) {
      console.error("录制失败:", error);
    }
  }

  async function uploadVideo(chunks: BlobPart[]) {
    // this.blob = new Blob(this.bufferMy, { type: "video/mp4" });
    const blob = new Blob(chunks, { type: "video/webm" });

    const file = new File([blob], Date.now() + ".mp4", {
      type: "video/webm"
    });
    const formData = new FormData();
    formData.append("file", file);
    const res = await apiPostFile(formData);
    console.log("上传视频结果", res);
    if (res && res.data && res.data.data) {
      return res.data.data;
    }
  }

  return {
    videoRef,
    streamRef,
    hasStream,
    getDevices,
    initCamera,
    releaseCamera,
    switchCamera,
    startRecording
  };
}
