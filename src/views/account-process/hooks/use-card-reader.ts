import { ref, onUnmounted } from "vue";

interface CardInfo {
  userName: string;
  sex: string;
  nation: string;
  birthday: string;
  idCardAddress: string;
  cardType: string;
  idCardNo: string;
  nationality: string;
  issuingAuthority: string;
  issuedData: string;
  idCardValidity: string;
  base64Photo: string;
}

interface MessageObject {
  ErrorString?: string;
  Name?: string;
  Sex?: string;
  Nation?: string;
  Birthday?: string;
  Address?: string;
  CardType?: string;
  IDNumber?: string;
  IDIssued?: string;
  IssuedData?: string;
  ValidDate?: string;
  Nationality?: string;
  Base64Photo?: string;
}

export function useCardReader() {
  const ws = ref<WebSocket | null>(null);
  const cardInfo = ref<CardInfo | null>(null);
  const errorMessage = ref<string | null>(null);
  const removeCard = ref(false);
  const connect = () => {
    return new Promise((resolve, reject) => {
      ws.value = new WebSocket(window.config.readCardWs);
      ws.value.onmessage = (evt) => {
        const obj: MessageObject = JSON.parse(evt.data);
        if (obj.ErrorString) {
          errorMessage.value = obj.ErrorString;
          if (cardInfo.value !== null) {
            stopReadCard();
          }
        } else if (obj.Name && obj.Base64Photo && cardInfo.value === null) {
          cardInfo.value = {
            userName: obj.Name,
            sex: obj.Sex!,
            nation: obj.Nation!,
            nationality: obj.Nationality! || "中国",
            birthday: obj.Birthday!,
            idCardAddress: obj.Address!,
            cardType: obj.CardType! === "居民身份证" ? "身份证" : "其他",
            idCardNo: obj.IDNumber!,
            issuingAuthority: obj.IDIssued!,
            issuedData: obj.IssuedData!,
            idCardValidity: obj.ValidDate!,
            base64Photo: JSON.parse(obj.Base64Photo).Base64
          };
        }
      };
      ws.value.onclose = () => {
        ws.value = null;
      };
      ws.value.onerror = (err) => {
        reject(err);
      };
      ws.value.onopen = () => {
        resolve(ws.value);
      };
    });
  };

  const startReadCard = async () => {
    // setTimeout(() => {
    //   cardInfo.value = {
    //     userName: "唐道远",
    //     sex: "男",
    //     nation: "瑶",
    //     nationality: null || "中国",
    //     birthday: "19950805",
    //     idCardAddress: "湖南省双牌县何家洞乡大竹江村",
    //     cardType: "居民身份证",
    //     idCardNo: "431123199508055531",
    //     issuingAuthority: "双牌县公安局",
    //     issuedData: "20201214",
    //     idCardValidity: "20301214",
    //     base64Photo:
    //       "data:image/png;base64,/9j/4AAQSkZJRgABAQEAYABgAAD/2wBDAAgGBgcGBQgHBwcJCQgKDBQNDAsLDBkSEw8UHRofHh0aHBwgJC4nICIsIxwcKDcpLDAxNDQ0Hyc5PTgyPC4zNDL/2wBDAQkJCQwLDBgNDRgyIRwhMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjL/wAARCAB+AGYDASIAAhEBAxEB/8QAHwAAAQUBAQEBAQEAAAAAAAAAAAECAwQFBgcICQoL/8QAtRAAAgEDAwIEAwUFBAQAAAF9AQIDAAQRBRIhMUEGE1FhByJxFDKBkaEII0KxwRVS0fAkM2JyggkKFhcYGRolJicoKSo0NTY3ODk6Q0RFRkdISUpTVFVWV1hZWmNkZWZnaGlqc3R1dnd4eXqDhIWGh4iJipKTlJWWl5iZmqKjpKWmp6ipqrKztLW2t7i5usLDxMXGx8jJytLT1NXW19jZ2uHi4+Tl5ufo6erx8vP09fb3+Pn6/8QAHwEAAwEBAQEBAQEBAQAAAAAAAAECAwQFBgcICQoL/8QAtREAAgECBAQDBAcFBAQAAQJ3AAECAxEEBSExBhJBUQdhcRMiMoEIFEKRobHBCSMzUvAVYnLRChYkNOEl8RcYGRomJygpKjU2Nzg5OkNERUZHSElKU1RVVldYWVpjZGVmZ2hpanN0dXZ3eHl6goOEhYaHiImKkpOUlZaXmJmaoqOkpaanqKmqsrO0tba3uLm6wsPExcbHyMnK0tPU1dbX2Nna4uPk5ebn6Onq8vP09fb3+Pn6/9oADAMBAAIRAxEAPwD3/NJuoIzRikAE8ZpM8ZzSmvKvi18S18L2S6ZpjBtRnBDOCCIl7/jTSJbOw8S+O9A8KxqdSvB5jjKxxDcxH0rg5/2g9BVsW9jdSL/eYBf0r5wvNQutQuWuLu4kmlY5LOck1XycVVhXPp+3+PPh2WHdNFcRt6KuRW7p3xb8KagsX+n+UzDkSIVAP1r5CyaAxB4NFgPuqw1mw1NN9ncxzL/eVgRV4nBFfFXhnxlqnhi9juLW4YxqeYmGVIr6r8D+NrLxlpCXEJC3KqPOj/un2pNDudXRTTmnCpGLRRRQMSjnNFFAjkfiH4wj8HeGpbwYa6k/dwI3Qse5x2FfIGo6jd6nqE15eztNPK25nY5Jr0347a/PqHjJtMWQ/ZrJQqjP8RALfzx+FeVrEW6CrWiFuyEk5py9KtiwlZCQvSr2m6Dc3m0iMlWIHFJyRapNmNhmbAFK0TjqK9p0L4ZWr2we6jy/utad18M9LbJEeP8AgNZutE0VCTPA1wB1wa6vwH42ufBetfaowZIJPllQnAI9fqK7C/8Ah7p0AbanI/2a841/TG0y+MQXEZOVpxqqWhM6TitT7N8P69Z+I9Gt9SsnJimXIB6j61rV4v8As9agZ/Dmo2zNkRXC7F9MrzXtFU0ZIKKKKgoKSloqhHyn8Z9O+zfEa5ZFx9pVZm9yeP6VS8OeEZLwxySRjyzXe/FvTUvfiLpyMM+ZBzx6Mf8ACtS3sJLSAQ2QGR0ycVlUqOOh0UaalqQ2Hw/00x/Mmc/7NdFpnhPTdNRVit0474rNitvFIXzI57ZVH8DMefyrd0y51EhUv44w/co2RWDk31O1U0jVSJETCrj6VBMhwcjirMreWpIGa5rUxrN5PstrpLaBv4gckVCs9ytURanCjFsda8i8caezyGYLwAc16W2h39tJ5j63PcEdQyjB/KsbXdMNzptxvGSEJGOe1a07KWhhWTa1LP7O0O3S9WkHU3CD/wAdr3avHvgDaGHwzfSY+/cDP4L/APXr2Guq9zz7WEooopWC4pNJnig0Y4pgeQ+P7bzPH+nzEfcjZCfYjIq8yyR2heIZcdK0fG1gDqtvd46DJPv0FR6cwlUBq5a3xHfQWmhx+oTeKZLZ3s52hbzAFRecj8eldForaiscAu3Z3CfvGY/xV0y26KpA71TuisJ25wWrO+lrHUo+ZJcXAMYAPUc1xviHT9avreQaddyQSiRfLwQAU7810UhZYi1W9PZLm2AJyQaS0YNHExadrdmYYpbuSZSvz7uuauakhi0x+zba7OaEYJNctrYUq0Y71ad5XM5JWsaPwgg+xaBcQYwGlD4+tekdq4v4f2jRabI7LjJXb9K7SuqJ59W19BKKKK0MBaKKQ9Kko57xbZyXWls0S5dWB/DvXJ6W5ypFelyIHiZCMgggj1rzjyDpuoy27cAN8n0rCrHqdeHn0OhVsisXUZWFwzBAzLwoPSr3nlYCRWTc6jCG/eSAN6ZrBHfG7eg2a8na1UG3jDEjIBNT6XvF38o2R9wOmay5NWtVQ/vKnsdZhkCqj5btQy5QklqdNduPKOK4+7SW71BIIgC7tgDPvW7NdN9nYsaj8MWgvde+0YzHGCGPv2q4K7OOrKyOx0SxfT9KghcASBfmx61ojNLRXUlY8+T5mLiiiiquTYKKKad2eOlIYuecVx3ja1EcUd+mAyMFY+xra8SeIrLwxo02pXzAJGpwo6sewAr56k+JGt+L/FiI8rw6Z82LVPujjjPqQamovdNKL949Rtb7z4dpPWsrU9Njkm80pu59Kzbe5ntJACTs9a37XUYJcM0g/OuLU9aE+VmLNZW7qY0tCSe5U1Y0zSYrdldkCsOwrfkv7NEP7ysS/wBZt1RjDJl+1Gpc63MibVtQEKMm6uy8CW6x6CtwPvXDF2/l/SvI5rie7ffIMiqvhn4oXvhfxdPpuoSvJpDyBQucmEADlfb1FbUdzz6/wn0fS1Ws722v7ZLi0mSaFwGV0OQQas11s4UFFFFQUVbrULSzgaa5uYoY15ZpHCgfnXBa38ZfDOnBorGZr+cdPKU+X/30ev4Zr5r1PW9V1e582/vJrhz1MjZqFX+XGMVsooz5jr/G/jnUvGt3HHcbYreNv3cMfQZ7+9XPCOgNYiOVwAXkJz+HH6D9a5DSYftGqwIecsP517SLHyo1AH3elTVjeNkaUpJSuaH9nJcQnIrPl0URfddk+grX0+7AUI5INX5Y4pe4OfSvPcZRPRjNSOTbSJZFz9pcj0OKbDoCmUFgT7kV1S2USn/61Pbyou9R7zKbijnbrTo4LZgvDfSvLPFWinzmv1yHzg4HXNew6hKkgbaa4/xDa+ZpMvHTmuqhCSd2cteatZHIeHPHuueFovLsLtvJzkxOcqfwr0vQfj7G5SLWdP8AL/vSw5P6GvDGGGqOVm2kCuxq5wpn2Dpvj3w1q9t9otdXtgncSuI2H1DYor48i4T5jg0VPIVzEhGKB1pz00VoZm74Zi3azA2OQf617hw64NePeBPLuPEcds68mJ3B+leuJlU6k0FIc0QVfl61HGbhW+ViPpUiOWbmpl+/WcoJvU1U2tiGSa6H/LRqgMkz/fcmr0/Q1WCg0uSK0sKVST3IDHuqrqNkJNNmDDjaf5VrJGKrajIVtpIx0wa0S0MmzwO7jMV1JGR904qA1o6woGq3H+/Wc1MRGy5NFPoqgP/Z"
    //   };
    //   setTimeout(() => {
    //     removeCard.value = true;
    //   }, 4000);
    // }, 4000);
    if (!ws.value) {
      await connect();
    }
    const Connectobj = {
      ConnectType: "USB",
      ConnectStr: "USB",
      FunctionCode: "boyayingjie_Continuity"
    };
    const json = JSON.stringify(Connectobj);
    ws.value?.send(json);
  };
  const mockSetCardInfo = () => {
    cardInfo.value = {
      userName: "唐道远",
      sex: "男",
      nation: "瑶",
      nationality: null || "中国",
      birthday: "19950805",
      idCardAddress: "河南省项城市永丰乡冯小庄村",
      cardType: "居民身份证",
      idCardNo: "412702199508055531",
      issuingAuthority: "双牌县公安局",
      issuedData: "20201214",
      idCardValidity: "20301214",
      base64Photo:
        "data:image/png;base64,/9j/4AAQSkZJRgABAQEAYABgAAD/2wBDAAgGBgcGBQgHBwcJCQgKDBQNDAsLDBkSEw8UHRofHh0aHBwgJC4nICIsIxwcKDcpLDAxNDQ0Hyc5PTgyPC4zNDL/2wBDAQkJCQwLDBgNDRgyIRwhMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjL/wAARCAB+AGYDASIAAhEBAxEB/8QAHwAAAQUBAQEBAQEAAAAAAAAAAAECAwQFBgcICQoL/8QAtRAAAgEDAwIEAwUFBAQAAAF9AQIDAAQRBRIhMUEGE1FhByJxFDKBkaEII0KxwRVS0fAkM2JyggkKFhcYGRolJicoKSo0NTY3ODk6Q0RFRkdISUpTVFVWV1hZWmNkZWZnaGlqc3R1dnd4eXqDhIWGh4iJipKTlJWWl5iZmqKjpKWmp6ipqrKztLW2t7i5usLDxMXGx8jJytLT1NXW19jZ2uHi4+Tl5ufo6erx8vP09fb3+Pn6/8QAHwEAAwEBAQEBAQEBAQAAAAAAAAECAwQFBgcICQoL/8QAtREAAgECBAQDBAcFBAQAAQJ3AAECAxEEBSExBhJBUQdhcRMiMoEIFEKRobHBCSMzUvAVYnLRChYkNOEl8RcYGRomJygpKjU2Nzg5OkNERUZHSElKU1RVVldYWVpjZGVmZ2hpanN0dXZ3eHl6goOEhYaHiImKkpOUlZaXmJmaoqOkpaanqKmqsrO0tba3uLm6wsPExcbHyMnK0tPU1dbX2Nna4uPk5ebn6Onq8vP09fb3+Pn6/9oADAMBAAIRAxEAPwD3/NJuoIzRikAE8ZpM8ZzSmvKvi18S18L2S6ZpjBtRnBDOCCIl7/jTSJbOw8S+O9A8KxqdSvB5jjKxxDcxH0rg5/2g9BVsW9jdSL/eYBf0r5wvNQutQuWuLu4kmlY5LOck1XycVVhXPp+3+PPh2WHdNFcRt6KuRW7p3xb8KagsX+n+UzDkSIVAP1r5CyaAxB4NFgPuqw1mw1NN9ncxzL/eVgRV4nBFfFXhnxlqnhi9juLW4YxqeYmGVIr6r8D+NrLxlpCXEJC3KqPOj/un2pNDudXRTTmnCpGLRRRQMSjnNFFAjkfiH4wj8HeGpbwYa6k/dwI3Qse5x2FfIGo6jd6nqE15eztNPK25nY5Jr0347a/PqHjJtMWQ/ZrJQqjP8RALfzx+FeVrEW6CrWiFuyEk5py9KtiwlZCQvSr2m6Dc3m0iMlWIHFJyRapNmNhmbAFK0TjqK9p0L4ZWr2we6jy/utad18M9LbJEeP8AgNZutE0VCTPA1wB1wa6vwH42ufBetfaowZIJPllQnAI9fqK7C/8Ah7p0AbanI/2a841/TG0y+MQXEZOVpxqqWhM6TitT7N8P69Z+I9Gt9SsnJimXIB6j61rV4v8As9agZ/Dmo2zNkRXC7F9MrzXtFU0ZIKKKKgoKSloqhHyn8Z9O+zfEa5ZFx9pVZm9yeP6VS8OeEZLwxySRjyzXe/FvTUvfiLpyMM+ZBzx6Mf8ACtS3sJLSAQ2QGR0ycVlUqOOh0UaalqQ2Hw/00x/Mmc/7NdFpnhPTdNRVit0474rNitvFIXzI57ZVH8DMefyrd0y51EhUv44w/co2RWDk31O1U0jVSJETCrj6VBMhwcjirMreWpIGa5rUxrN5PstrpLaBv4gckVCs9ytURanCjFsda8i8caezyGYLwAc16W2h39tJ5j63PcEdQyjB/KsbXdMNzptxvGSEJGOe1a07KWhhWTa1LP7O0O3S9WkHU3CD/wAdr3avHvgDaGHwzfSY+/cDP4L/APXr2Guq9zz7WEooopWC4pNJnig0Y4pgeQ+P7bzPH+nzEfcjZCfYjIq8yyR2heIZcdK0fG1gDqtvd46DJPv0FR6cwlUBq5a3xHfQWmhx+oTeKZLZ3s52hbzAFRecj8eldForaiscAu3Z3CfvGY/xV0y26KpA71TuisJ25wWrO+lrHUo+ZJcXAMYAPUc1xviHT9avreQaddyQSiRfLwQAU7810UhZYi1W9PZLm2AJyQaS0YNHExadrdmYYpbuSZSvz7uuauakhi0x+zba7OaEYJNctrYUq0Y71ad5XM5JWsaPwgg+xaBcQYwGlD4+tekdq4v4f2jRabI7LjJXb9K7SuqJ59W19BKKKK0MBaKKQ9Kko57xbZyXWls0S5dWB/DvXJ6W5ypFelyIHiZCMgggj1rzjyDpuoy27cAN8n0rCrHqdeHn0OhVsisXUZWFwzBAzLwoPSr3nlYCRWTc6jCG/eSAN6ZrBHfG7eg2a8na1UG3jDEjIBNT6XvF38o2R9wOmay5NWtVQ/vKnsdZhkCqj5btQy5QklqdNduPKOK4+7SW71BIIgC7tgDPvW7NdN9nYsaj8MWgvde+0YzHGCGPv2q4K7OOrKyOx0SxfT9KghcASBfmx61ojNLRXUlY8+T5mLiiiiquTYKKKad2eOlIYuecVx3ja1EcUd+mAyMFY+xra8SeIrLwxo02pXzAJGpwo6sewAr56k+JGt+L/FiI8rw6Z82LVPujjjPqQamovdNKL949Rtb7z4dpPWsrU9Njkm80pu59Kzbe5ntJACTs9a37XUYJcM0g/OuLU9aE+VmLNZW7qY0tCSe5U1Y0zSYrdldkCsOwrfkv7NEP7ysS/wBZt1RjDJl+1Gpc63MibVtQEKMm6uy8CW6x6CtwPvXDF2/l/SvI5rie7ffIMiqvhn4oXvhfxdPpuoSvJpDyBQucmEADlfb1FbUdzz6/wn0fS1Ws722v7ZLi0mSaFwGV0OQQas11s4UFFFFQUVbrULSzgaa5uYoY15ZpHCgfnXBa38ZfDOnBorGZr+cdPKU+X/30ev4Zr5r1PW9V1e582/vJrhz1MjZqFX+XGMVsooz5jr/G/jnUvGt3HHcbYreNv3cMfQZ7+9XPCOgNYiOVwAXkJz+HH6D9a5DSYftGqwIecsP517SLHyo1AH3elTVjeNkaUpJSuaH9nJcQnIrPl0URfddk+grX0+7AUI5INX5Y4pe4OfSvPcZRPRjNSOTbSJZFz9pcj0OKbDoCmUFgT7kV1S2USn/61Pbyou9R7zKbijnbrTo4LZgvDfSvLPFWinzmv1yHzg4HXNew6hKkgbaa4/xDa+ZpMvHTmuqhCSd2cteatZHIeHPHuueFovLsLtvJzkxOcqfwr0vQfj7G5SLWdP8AL/vSw5P6GvDGGGqOVm2kCuxq5wpn2Dpvj3w1q9t9otdXtgncSuI2H1DYor48i4T5jg0VPIVzEhGKB1pz00VoZm74Zi3azA2OQf617hw64NePeBPLuPEcds68mJ3B+leuJlU6k0FIc0QVfl61HGbhW+ViPpUiOWbmpl+/WcoJvU1U2tiGSa6H/LRqgMkz/fcmr0/Q1WCg0uSK0sKVST3IDHuqrqNkJNNmDDjaf5VrJGKrajIVtpIx0wa0S0MmzwO7jMV1JGR904qA1o6woGq3H+/Wc1MRGy5NFPoqgP/Z"
    };
    setTimeout(() => {
      removeCard.value = true;
    }, 1000);
  };
  const stopReadCard = () => {
    ws.value?.send("boyayingjie_Stop");
    setTimeout(() => {
      removeCard.value = true;
    }, 2000);
  };

  onUnmounted(() => {
    if (ws.value) {
      ws.value.close();
    }
  });

  return {
    connect,
    startReadCard,
    stopReadCard,
    cardInfo,
    mockSetCardInfo,
    errorMessage,
    removeCard
  };
}
