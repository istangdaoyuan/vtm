import eventbus from "../../../utils/eventbus";

const net = require("net");
// 创建一个 TCP 客户端
const client = new net.Socket();
const sleep = (time: number) => {
  return new Promise((resolve) => setTimeout(resolve, time));
};
const pressKey = (serializeData: { press: any }) => {
  // {"event":"ScanKeyPress","press":"48"} msgData======<>
  const sourceKey = serializeData.press;
  if (!sourceKey) return "";
  const keyMap: any = {
    48: "0",
    49: "1",
    50: "2",
    51: "3",
    52: "4",
    53: "5",
    54: "6",
    55: "7",
    56: "8",
    57: "9",
    46: ".",
    13: "Enter",
    8: "Backspace",
    27: "Esc"
  };
  if (!keyMap[sourceKey]) return "";
  return keyMap[sourceKey];
};
export async function useKeyBoard() {
  try {
    // 连接到服务器
    client.connect(12346, window.config.arrayServer, () => {
      console.log("SUCCES: ", "密码键盘连接成功.");
      initQidongqi();
    });
  } catch (error) {
    console.log(error);
  }

  // 接收到数据的处理
  client.on("data", async (data: any) => {
    try {
      const serializeData = JSON.parse(data);
      console.log("INFO：密码键盘数据 " + serializeData);
      eventbus.$emit("keyBoard", pressKey(serializeData));
      // eventbus.$emit("keyBoard", { pressKey: "1" });
    } catch (error) {
      console.log(error);
    }
  });

  // 连接关闭的处理
  client.on("close", () => {
    console.log("密码键盘关闭Connection closed by server.");
  });

  // 错误处理
  client.on("error", (err: { message: string }) => {
    console.error("Error: " + err.message);
  });

  async function initQidongqi() {
    // 发送消息到服务器
    const data = {
      msgEvent: "SUNSON_OpenCom",
      Param1: "5",
      Param2: "9600"
    };
    client.write(JSON.stringify(data));
    await sleep(1000);
    sendOpenData();
  }

  async function sendOpenData() {
    const data = {
      msgEvent: "SUNSON_UseEppPlainTextMode"
    };
    client.write(JSON.stringify(data));
    await sleep(1000);
    sendStaraData();
  }
  function sendStaraData() {
    const readData = {
      msgEvent: "SUNSON_Start"
    };
    client.write(JSON.stringify(readData));
  }
  return {
    client
  };
}
