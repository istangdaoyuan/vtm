/*
 * @Author: fanjs
 * @Date: 2023-12-17 16:30:12
 * @Description:
 */
import { ref, onMounted, watch } from "vue";
import { drawImage } from "./canvasTools";

export const useDrawSignInCanvas = (signList: any, isPdfEndLoadRef: any) => {
  const isShowAllPdfsRef = ref(false);
  const drawSign = async (showAllPdfs?: any) => {
    const isDrawAll = showAllPdfs;
    for (const sign of signList.value) {
      const { pdfCanvas, image } = sign;
      if (!pdfCanvas) continue;
      const ctx = pdfCanvas.getContext("2d");
      if (!isDrawAll) {
        ctx.beginPath();
      }
      if (image) {
        drawImage(ctx, sign);
      }
      if (!isDrawAll) {
        ctx.closePath();
      }
    }
  };

  const init = () => {
    if (isPdfEndLoadRef.value && !isShowAllPdfsRef.value) {
      isShowAllPdfsRef.value = true;
      drawSign(isShowAllPdfsRef.value);
    } else {
      drawSign();
    }
  };

  onMounted(() => {
    init();
  });

  watch(
    () => signList,
    () => {
      init();
    },
    { deep: true }
  );
};
