/*
 * @Author: fanjs
 * @Date: 2023-12-17 16:10:00
 * @Description: 
 */
/*
 * @Author: fanjs
 * @Date: 2023-12-09 18:41:56
 * @Description:
 */
import { ref, onMounted, onUnmounted, watch } from 'vue';
const throttling = (fn: any, wait: number, maxTimelong: number) => {
  let timeout = null as any;
  let startTime = Number(new Date());
  // eslint-disable-next-line func-names
  return function () {
    if (timeout !== null) clearTimeout(timeout);
    const curTime = Number(new Date());
    if (curTime - startTime >= maxTimelong) {
      fn();
      startTime = curTime;
    } else {
      timeout = setTimeout(fn, wait);
    }
  };
};
const createImage = (signSrc: string) => {
  if (signSrc) {
    const image = new Image();
    image.crossOrigin = '';
    image.src = signSrc.indexOf('http') !== -1 ? `${signSrc}?t=${Number(new Date())}` : signSrc;
    return image;
  }
  throw Error('图片地址为空');
};

const cloneCanvas = (oldCanvas: HTMLCanvasElement) => {
  // create a new canvas
  const newCanvas = document.createElement('canvas');
  // set dimensions
  newCanvas.className = 'sign-canvas';
  newCanvas.width = oldCanvas.width;
  newCanvas.height = oldCanvas.height;
  newCanvas.style.width = oldCanvas.style.width;
  newCanvas.style.height = oldCanvas.style.height;
  newCanvas.style.transform = oldCanvas.style.transform;
  const pageIndex = oldCanvas.getAttribute('aria-label')?.replace('Page ', '') || '';
  newCanvas.setAttribute('pageindex', `${Number(pageIndex) - 1}`);
  return newCanvas;
};

// 绘制当前日期文本为图片
function clipCanvasText(w: number, h: number, _key: string, textValue: string) {
  const CLIP_TEXT_SIZE = 20; // 画签章的字体大小

  return new Promise(resolve => {
    const canvas = document.createElement('canvas');
    const ctx = canvas.getContext('2d');
    canvas.width = w;
    canvas.height = h;
    if (!ctx) return

    let text: any = textValue;
    const date = new Date();
    if (_key === 'SIGNATURE_PERSON_CHARGE_YEAR') text = date.getFullYear() + '';
    if (_key === 'SIGNATURE_PERSON_CHARGE_MONTH') text = (date.getMonth() + 1) + '';
    if (_key === 'SIGNATURE_PERSON_CHARGE_DAY') text = date.getDate() + '';
    ctx.font = `${CLIP_TEXT_SIZE}px Arial`;
    ctx.textBaseline = 'middle';
    ctx.fillStyle = 'red';
    const textWidth = ctx.measureText(text).width;
    const x = (canvas.width - textWidth) / 2;
    ctx.fillText(text, x, CLIP_TEXT_SIZE);
    resolve(canvas.toDataURL('image/png'));
    // ctx.font = `${CLIP_TEXT_SIZE}px Arial`;
    // ctx.textBaseline = 'middle';
    // ctx.fillStyle = 'red'; // 用黑色
    // let text:any = '';
    // const date = new Date();
    // const isDate = _key.indexOf('SIGNATURE_PERSON_CHARGE') > -1;
    // if (isDate) {
    //   if (_key === 'SIGNATURE_PERSON_CHARGE_YEAR') text = date.getFullYear();
    //   if (_key === 'SIGNATURE_PERSON_CHARGE_MONTH') text = date.getMonth() + 1;
    //   if (_key === 'SIGNATURE_PERSON_CHARGE_DAY') text = date.getDate();
    // } else {
    //   text = textValue
    // }
    // const textWidth = ctx.measureText(text).width;
    // const x = (canvas.width - textWidth) / 2;
    // ctx.fillText(text, x, CLIP_TEXT_SIZE);
    // resolve(canvas.toDataURL('image/png'));
  });
}


// 插入回显图层
const insetCanvas = async (signList: any, echoSignList: any, canvas: HTMLCanvasElement, resultImg: any) => {
  if (!echoSignList.value) return;
  const signs = JSON.parse(echoSignList.value);
  // 懒加载的方法，遍历signs数组中的每项，比较页码是否相等
  for (let i = 0; i < signs.length; i++) {
    const sign = signs[i];
    if (sign.pageIndex === Number(canvas.getAttribute('pageindex'))) {
      /**
       * 判断 sign 里的 key 值，在这里替换渲染的图片
       * 1.电子签名: 先生成 再 替换成手写签名
       * 2.日期: 替换成当前年、月、日显示
       * 3.自定义文本：也重新生成图片
       */
      const isImage = sign.key === 'PARTY_SEAL';
      const isSign = sign.key === 'PARTY_SIGNATURE';
      const imgData = isImage ? sign.signSrc : (isSign && resultImg) ? resultImg : await clipCanvasText(sign.w, sign.h, sign.key, sign.options.text);
      const renderSign = { ...sign, pdfCanvas: canvas, image: createImage(imgData) };
      signList.value.push(renderSign);
    }
  }
};

// eslint-disable-next-line max-params
export const useCloneSignCanvas = (signList: any, echoSignList: any, isPdfBeginLoadRef: any, isPdfEndLoadRef: any, resultImg:any) => {
  const eventRef = ref(null);
  const scaleRef = ref(1);
  const initClone = () => {
    const domWrapper = document.querySelector('#viewerContainer') as any;
    function savePdfCanvasFirstImage() {
      const $pages: any = document.querySelectorAll('.page');
      const isAllLoaded = [...$pages].every(page => Boolean(page.children[0].children[0]));
      isPdfEndLoadRef.value = isAllLoaded;

      const showPages = [...$pages].filter(page => {
        const canvas = page.children[0].children[0];
        const isLoaded = page.getAttribute('data-loaded');
        return isLoaded && Boolean(canvas) && !canvas.isCatchFirstImage;
      });
      if (isAllLoaded && showPages.length === 0) {
        if (eventRef.value) {
          domWrapper?.removeEventListener('scroll', eventRef.value);
        }
        return;
      }
      if (showPages.length) {
        showPages.forEach($page => {
          const pdfCanvas = $page.children[0].children[0];
          const signCanvas = cloneCanvas(pdfCanvas);
          scaleRef.value = parseInt(pdfCanvas.style.width, 10) / pdfCanvas.width;
          $page.children[0].appendChild(signCanvas);
          pdfCanvas.isCatchFirstImage = true;
          // 插入回显的pdfCanvas === signCanvas
          insetCanvas(signList, echoSignList, signCanvas, resultImg);
        });
      }
    }

    if (isPdfBeginLoadRef.value && !isPdfEndLoadRef.value) {
      savePdfCanvasFirstImage();
      if (eventRef.value) {
        domWrapper?.removeEventListener('scroll', eventRef.value);
      }
      eventRef.value = throttling(savePdfCanvasFirstImage, 50, 500) as any;
      domWrapper?.addEventListener('scroll', eventRef.value);
    } else {
      console.log('已经全部渲染完毕');
      if (eventRef.value) {
        domWrapper?.removeEventListener('scroll', eventRef.value);
        eventRef.value = null;
      }
    }
  };

  onMounted(() => {
    initClone();
  });

  onUnmounted(() => {
    const domWrapper = document.querySelector('#viewerContainer');
    if (eventRef.value) {
      domWrapper?.removeEventListener('scroll', eventRef.value);
    }
  });

  watch(
    () => isPdfBeginLoadRef,
    () => {
      initClone();
    },
    { deep: true }
  );

  return scaleRef.value;
};
