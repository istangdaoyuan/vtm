// vue 倒计时hook
import { onBeforeUnmount, ref } from "vue";
export function useCountDown(initCount: number) {
  let timer: any = null;
  let count = ref(initCount);
  let runing = ref(false);
  const start = (callback: Function) => {
    if (timer) {
      clearInterval(timer);
      timer = null;
    }
    runing.value = true;
    timer = setInterval(() => {
      count.value--;
      if (count.value <= 0) {
        clearInterval(timer);
        timer = null;
        callback(count.value);
        reset();
      }
    }, 1000);
  };

  const stop = () => {
    if (timer) {
      clearInterval(timer);
      timer = null;
    }
    runing.value = false;
    reset();
  };

  const reset = () => {
    count.value = initCount;
  };
  // 生命周期清除定时器
  onBeforeUnmount(() => {
    if (timer) {
      clearInterval(timer);
      timer = null;
    }
  });

  return {
    start,
    stop,
    runing,
    reset,
    count
  };
}
