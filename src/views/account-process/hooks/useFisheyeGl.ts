import { FisheyeGl } from "../../../sdk/fisheyegl.js";
import { onUnmounted } from "vue";
export function useFisheyeGl({
  video,
  selector
}: {
  video: string;
  selector: string;
}) {
  let distorter: any;
  const init = function () {
    const lens = {
      a: 0.72,
      b: 0.98,
      Fx: 0.12,
      Fy: 0.3,
      scale: 1
    };
    const fov = {
      x: 1,
      y: 1
    };

    distorter = FisheyeGl({
      lens,
      fov,
      selector,
      video
    });
    distorter.run();
  };
  onUnmounted(() => {
    if (distorter) {
      distorter.destroy();
    }
  });
  return { init, distorter };
}
