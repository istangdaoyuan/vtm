import { reactive, onMounted, onUnmounted, toRefs } from "vue";

interface SignaturePadState {
  ws: WebSocket | null;
  status: boolean;
  clickDone: boolean;
  signatureImageUrl: string;
}

export function useSignaturePad() {
  const state = reactive<SignaturePadState>({
    ws: null,
    status: false, // 设备状态
    clickDone: false,
    signatureImageUrl: ""
    // signatureImageUrl:
    //   "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAf0AAAENCAYAAAAFXryRAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAALiMAAC4jAXilP3YAAC7ZSURBVHhe7Z1Z0HVVeee5zGXu7Lv2SnMVctFtpUqpsqJFpdXqErur1Y5iSqFbSLcTOLYhohY4BWicBZkCQlTEQJhEEAEFZBA+kEkFFBkjIAEVtTvdz2+/30M2x3POe4Y9rv37Vf2Ldx++97xnWHs9az3rGfYRERERERERERERERERERERkV74w70SERGRwrkx9NqdH0VERKRUPhH62s6PIiIiUiovCu0J/UF1JSIiIkXCGf6PQy+prkRERKRYrgi9f+dHERERKZVzQ+fv/CgiIiKl8leh/xd6fXUlIiIixXJlCKP/vOpKREREiuXBEEZfRERECufR0G93fhQREZGSYaf/9M6PIiIiUjIPh36z86OIiIiUzM9DuvdFREQmwGOh3+38KCIiIiXz65DR+yIiIhPgVyGNvoiIyARwpy8iIjIRSNf7l50fRUREpGQw+u70RUREJoBGX0REZCJ4pi8iIjIR3OmLiIhMBI2+iIjIRNDoi4iITASNvoiIyETQ6IuIiEwE2upq9EVERCaARl9ERGQi0Etfoy8iIjIB3OmLiIhMhGyt+2+qKxERESmWx0MY/X2rKxERESmW+0MY/ddVVyIiIlIst4cw+h+prkRERKRYLg5h9M+vrkRERKRYjg1h9G+trkRERKRYXh3C6D9UXYmIiEix/HEIo0/qnoiIiBTMc0IY/X+prkRERKRoMPgYfhERESkcjb6IiMhE+D8hjL6leEVERAonO+09v7oSERGRYslOe0Tyi4iISMH8cwij/6rqSkRERIrln0IY/TdWVyIiIlIsd4cw+sdUVyIi4+bFe/8rInO4MoTRv6i6EhHphleE/ibE3HNd6NHQk6FMI25Dvw49Ebov9O3Q50KvCYlMhhND3Ay3VVciIs3yotDRoatCj4X+b2jWGA9BLDhuDL0rZAqzFMshIQb8z6srEZHteEno3BAGfp1d+9MhduFsQK4N4YW8MHRB6JzQu2t6xy7i35wR4nWgS0I8Hx4F4pj4O1mjZJHoScLv0JhMpBj+fYgBTuqeiMgmvD10Z2i3XTyLADKG2FGfHTow9IehvviDEJlLXw3h8v9daNHr5v+/L6QXQEYNg95SvCKyLq8MsXNeZCgRLvOrQ4eG2GCMAWqWHBv6SWiRp4L3xULB+iYySrJAj1X5RGQZ7HLZoeecMStc5jeH3hJiQ1ECB4QINsTQz3vPeAAOComMBs7zGbyc74uI1MH9/ulQFvKaFWfxe0Jj2clvAzv740KPhGY/BzwefE4ig4cblkF7enUlIrLPPi8L3RWaNW4It/ctodeHpgqLIebMX4Tqnw3G/wuhUjwdUiBfCTFYb6iuRGTKEK1O5H3dkKWIfD88pEF7NnxmPwvVPyuOOgj8ExkcmbbHjS4i0wTDlWW56/pl6MyQUeu7g3ckq5ymiAV4Z0hkMJBXy+DkbE5EpgWG6t5Q3VCh20P7hmR9mFP5/OqfJ4snvCQivfOcEIOSczoRmQak3M26pBHG6o9Csj1UI/xBqP75Phyy4I/0ThbVcGUvUjZ01MyMnboeDLFDlebB+N8Tqn/eLAY8MpHewPXEQNT9JFImGJgHQnXDg2h086aQtA/elYdC+dnjXSWQWqRz0s13VnUlIiXx8dBsdTlyzf9LSLrnbSE6/uV3Qdrf60IinZEtdk3bEykHUuu+F0rjgogm18D0D9/NqaF6v4JbQ7r8pROyxS7uPxEZP88NPR5Kg4JOC8mwwMjXI/1ZBBwZEmmVzNXnbF9Exg1u+7o7H1fyC0IyXIjop51vfmc/DPXZgVAKJ1vsssoUkfHyqVAaDkS3OI3HOMDlf14ov7vfhqZc6lhahMGWA80JQmScXBHK+xgZmDtODgzVWxafHBJpHGpFM8DIKRWR8cCi/cehNBK49t0hjhs2X7j48zu9KsT3LNIY2TrTXH2R8YBxqNfMJy7HIlvlcE4ov9ufhjT80hhZf9sWu1IqrwqVFNBGhP5ToTQK1NugrLaUxUdCGZjJAs8jWGmEzNW/rroSKYf9QjSUwgVObnQJzBr8G0NSLpzz5xEsZZQ1/LI12Vf/vupKpAxwj1KQpqRJEoOfpbORpVynAb0R0vA/EdLwy1b8rxCDiYIeIiXwxyEmSZqblALu+7rBPykk0wGvVVbxo5iaZ/yyMRSHYCCRGypSAh8LMaYvqa7GDzu7epW9D4VkepBhlYafCH+RjcgCPUikBNJ7RYGasYPBzwwbdGxIpsvLQhncdxEPiKzL80M5oYiUQL3S5JjdoBh8WuDm/YkHQ+TgUI6Jo3lAZB3+bSgHkEgp/CbEmH5XdTU+WKzcH8p785iQSELVRcYFu36OaEVWhsklJxbbO0op3BJiTJOuN0a+H8r70rK6Mg8yrhgfNOyxToOsRU4uuPpFSoAKk4xpdkJjS3GqV2OjEYvIPBjXdFJknBjYJ2uREwxnoSIlgAcrA57eywMj4W2hvB+v5wGRJbwwlOPlRB4QWYUcNJQrFSmFX4QY17jKxwD1BTIliwA+c7FlFepeLc/3ZSVyonljdSVSBheHGNeceQ4dDDxlVnm9FBai+p7IqlweYuzg7vd8X3aFwjwMGFOCpCRoM8u4RkOfCL8Zytfqbk024ZEQ4+e26kpkCVnt6/jqSqQM6uf6Q65iR1phGvyv8YDIBuAdyhr9R/KAyCIeDjFQiBoWKYmsZndXdTU8CJ7NhQl5+SLb8P4QY+l3Id38spA9IQbKNdWVSDkwphnbQ+wtgScim+gwSVsnQ5rgnhBj6rLqSmQOuBQZJLbXldKop8ANrQ4FvfDztRF/INIE+4bwHiHTsGUuNPFg4nmquhIph+eF0rCewAMDId2w6Bs8INIgV4cYW7dWVyIzHBBigJC6NwT+PMRC5OTqSmQ7MiV1KOf6LETyNRFEaz6+NA1HRYwvdvuMN5FnUd8N9VGy9CWht4fODDEJXhjCLWuusjTBz0KMbXKYh8BNobzfcMWKtMGPQoyx86srkRky1eOV1VXzsJig4t9nQ18N0QiFqmP8XX6+KvSa0NjqpMvwYdJLI0tXyT5hgZuvRbe+tMnrQoyzp6srkRmeCDFAmsjVrxt4oqepNEaUMu7VM0JHhfj/7uSlC14WSkP7YR7okTtDvA7c+7r1pW0w+Iw3Fpsiz4LdNoNjUZrH/nv1ir2iahjn7ift1aWhO0LkRaeB5/F3hOzeJ32TZ+ikM/UFtfV5DYjFr0jbnBtivH2ruhIJMOSkC1EYhMGRBhvlBIVYFFDeEcP+g73/ZeJC7OjfE2Ix0Lf7VGQetB5lHBPY1FfRkiy1y2twly9dQMoeY04X/0BgB4ybm0Y3pPB8JcT54yV79ZOaMMoEJD0QooIe/31o72Mprvl3iP9PLebHQllmd54w7hhxDDrXBDvxmlgMiJTCW0M55t/HAz2QcTPfq65EuiEbOenibxmM5ptDZ4VY4dPeE2OdZ+dt6ckQLUUx9v8UejDEguDboQtCp4f+JDTLgSF+fyhpeyJNUq/D/1Me6JiDQ3mPOvlKl5wdYtzp4t8Cdui4xOlKx5nJzSFWU+yS88ZeJIwq/5YFAF8Chpho9sNCPCdn5X3ssnHL52vU9SglkkdYfbjX8dbxt/G6iXRJxpL8prqSuaRRJ5L9ohCub3bNGQm5SPTtZofNToJ0HFZYh4fGkoqWO6EXVlciZfH5UN6reOG6ol4H4zgeEOkYqq0y/ijENnn2CzEZEOiDwc6bc574/5yb5w59TAZ9FXJRc0R1JVIW9Rz5LpuREJ/D3zRNT/qCTShjcJIuflbdnwzdHco0nrpyp45Rf2+IoLap3KgsaPgMiEMQKZH0ZuG56wLmjpxnruABkR7g2JgxOJn+KpxXY+i50XnjKXLKvxt6V8jgmp3YBD6XG6orkfLgCI4x3lWrXeYd/h6LDWugS59k9kjRdVNY3XAen6t7RBDdl0IvCsmzIWWQz8gWu1IqWY8cdQEbC/4W85BIn2Qw6YnVVUGwqz8llIELiBS2L4boPCSLOSaUn5dIifxjKOeFtr17fxXq6m+J7EZ6nchiKQK6tFElrr6rp/BMWw1kSuRNIT63rlyfIl1zSCjnhw/wQIvcG+Lv6DmTIZCpe6OuxUKJQWq+Y6TyRmaHf2rIXf36/FkoP0eREsmypOgfeKAl6nUvaBMtMgTI1WdMkrU2KkiTY/WcN9XvQlS9o5uWbA4BHvmZipRKjnFKVbfFOSH+hgVRZEhcF2JcnlBdjYBDQ1Syy5uW8rI8Js1QN/q2vZVSyd1Om01IMkugTW+CyLocFGJc9lGKei04i7gllAbp6pBGqXkoMpSfcdFpHTJpsgEJagMyg/L5mbtEhgJ1IxiXgz7X53w+g/No72oUbLvkZKXRl1Khy12O8zbONs8L8dy69mWIZBG2tY7D2RG2XXqW4JfMccVVRi18aZ9cYBHwJFIip4UY4+hIHmiYbGV9ZXUlMiwuDjE+z6yuVoTWrLeHnlNdNQtRr9eHeFEYIArpWK+6O7Jqk0GRUirEATHGES2nmyTdp4jiYCJDI0vyrl2KGjc7TWqa3PFTDjeDbMix9zyse/Lz77ILmUiXZL4yIjC4SejVwfOyYREZIixMGZ9o7Q31C0JN7PhZOLDi5mZhp2lEfn/kkcpR1ZVImaTRb7oQFalQPC+Nu0SGSmbBbeSNYsdP7h9u+U3AjfxEiBdAgE3bsQKynAdDfBcEUIqUSh5joSbJplVNHxuINMm5Icbp2dXVBrBaII1uXcNPEA2pA+hoHpDeuTXEYLimuhIpk/RooSaPEbOT56erK5FhQmA843SrEtEY/htDq7SP5BwhKwOxyzcNbzhcHuJ7oSOTSKncHWKcoybL5D4Z4jmJTxIZKnjUGad4vLYCw0/Tm2WR3xSueDTEH9wT0p0/LGi7yHdjpz0pmQtDjHN0AQ80RPYBcSMjQyfH6tZ9ajDqd4Y+U139K+zuvx7KqMHPhmR4ZBcyO+1JyRweSqNPFlJT5EQ61MwjjmApwXptdSVThjL2jNVG0rPZvZ8VYrfIWT83Vd4MnHlZ+GW4ZMoREikVduI5znHJN0U+5xArWhI3lfPw4GuvS+vkEftHqquGYFVJcR1c/reFTMUbPvXWoyKlQppxjvMm65Dnc66d/9wyp4d4XXhZCWLcNNtKyiGPci+prmSy1AuXbH3WIzJgMIA51puoLoqhz+cbChy3fif0oxAtyHnPVFQVyaNcCuHJhKlPXDbdkZL5dSjH+n/ggS3J1tRPVVf9wuL9myGKsPy3EH0AeG2fC4lAenUJrJeJkxOhRl9KhrTUHOuf54EteWmI56LAVV+QMo2xp33wETwQZE422VIiSW7wDNqWZyZCAy6lZLLbGKLGyLYQEMVz9VGNj+OJL4fqxh6Y2PE84No3PVpmycqU0iJjyN/NiVCjLyXzgVCO9Ud4YEsuC/FcH6+uugFDTkAWr/9Te6/rHBfiNbXRQljGDwtCxode3ZYgfRGXYpMVwNogJ8Kh5hqLNMEBoRzrTbg487igq8XyMSF29jT5WRSI+EDolJ0fRX4PxgdjliMgaYH7Q/8uRCTtkFNmciLU6EvJ1NP20LbQlpro+LZhsfLjEJUEl80jeAD+fudHkbnQ5I6xT7EqaQEqINGOGDfcmTwwUHIS1OhL6ZCjn+N9m6M33Oo8BznwbYELlpzqe0IY/mXgbaCZytDqBciw+FqIcWtX1V04OHRs6MWhdXbsHwwRWcsEweQw1BsyJ0HP9KV0Hg/leP8wD2wIKX88R5MlfRMW37RAJf2OOWQVKIi2/86PIgvJuBZSOmUBB4ZykiAFJlt03h5iFU5PgU+GXhN6RWjWcPLhvjdE69q/5IEBku+vkZrMIgPmplCOdwLxNoXzdZ6DRj5NgeeBDIPZiPzdoNzuP+78KLKUtGfYL1kAu/NfhfigOL+7KMTNyar6Q6GTQmeEmEz4IHHpkxbBjcsu4Csh+g+war85RK17fndI0ZM5Cb6xuhIpl9NCOd65VzflnBDP0UTk/jtCzB3Ux1/3rJX56aGQ1TRlFdiUMm77rC0xCnDpU9oyJwv0RIhdPP0F5uXDPjdE62E8ASwMMPwsGu4N0ZeARQHPQ+oNXgD+DZ6CPsjczaOqK5FyycI1iEC8TcHrx3Owc1qXPw3hGaQLKc1/rgq9IbQJnwhxFCCyCiwSGbfYL1kBalhzdpaTRl2/DDERsFKftwgAciQx9n8XytiAfUO4/fEW5HPnQoAAQAIB2ybLk7LwECkZKtjlPbtN5P3dIZ5jnTiYb4X4HVJ5Cew9PrRtVg9BftbWl3Vg3G+z4J0kBNrQSpgaxjmBzIqVFOd99XNyUm4w5LgY+f8YeXJuMfx1uMblR8UtXH4sFlgEsKBoolHILBw98JrtviRToH6fbmp08dzx+4sW+F2At6CJyoIyLfDsUrFRtgC3/LWhemRwXXzIrMjJob0hlPB7GHby+RHNMeZNQkwsLAIwyiw0zgs1GWnPxMHrtPuSTIGnQ3lvblqkJI/n+oTMIJrriKxDdl+UhuDM5KAQrn6qfuXkUhc798tD/LtM42N3T3GNx0LU8l5k1NnpU/Ob3TmFFv5TaFsyKMnuSzIFOD7LexEX+ybgqeP3+4LAPRYeOX+IrApGv8+xWzz0t/6HUO4M5ok0wG+ECPzjJiagbhWj/vYQHgaee5GXYBWo083r8JxHpgBZNHnvkY2zCXgL+nSRkiY85IJfMlxyMyodgJselxxBf7namid2++QQE9yHUWdRwMIBz8A8SP9LLwHBQuvm2788xN+lWplI6aRnCxEzswkY/T5blBJI2OQRn0yHTEGXjiAIMDt87RcibYedOmcsORHNii+JEpvkFbMbxwNANbHZnT1egr8OUaf7ztCqgX8sGvJviZQO90WOdzJXNgGDj+HvA+57GqeIbMIvQox9azt0CJG/5PHPQn1tFgGZ058T06yYcIgLYAFwS4jqXbPGncheAv9YMFwXWrYAYLGQz+1AkNKhuFaO900DmvhdPGt9QN10jvNENiFjWmyv2yG44N+z8+NS8ARQ0Y8vadkigOwAxFnlfw/NphER4btb5H8+lwNBSofFb/3+WTftjn/P79E9sw/Y5W8avyOSRt8Gax3CznyTACKCAsnxp4TiokUA5/IsAOj1PVsDYFnkf/6+Rl+mQP3+4b5aByZLfg8PWtfgpehrsSFlwPhh/NprpUOoCtZEehzHAZeGcPXnBFYXwYKcWbKyIwagvqPJyH8CmSglnL+j0ZcpUM/VX6e5DWQp3z6i5ynw9ZmdH0U2gjLwjN83V1fSGZwHNnl+zm6F8r6LFgDEAXC+/7FQHY4Q6qWFdfnIFMgqlIho/nXIDnurtr1tEgqA6dqXbaBnDOPXXisdQ2W+1+782Di4ACkBXN/N1MURAF293h+iKiCTSP4/d/oyBb4fyjFPMCxesFXP9jPl75DqqjteHCIeSGQbTgkxfgkIlQ6hJC8FNtqGBQCTGoY+J7m6OP/PhjtIoy9TgBoYOeY5aqNpDWedqxh+gmL5va7z5HmNNteRbaH/C+PXXisd88UQK64uoeIf5znLMgEYEJb2lNJ5WyjHfFaixKD+MLSb4c9eFS6QZYzgoWL8YgukQzgPJCinL3ZbANAA6H0hkRJZlKtPK2vOPJedm2cMjItjGSNZgZW4FukQKudRkncIsADICXBWTIgcD2zajUxkiMzm6tcLV3E/EHOzyPA/FOJ3RMZIppwSFCod8obQTTs/DoKc/EjlW+QB4OyfYkGrBjyJDJn6GH8lD9TgmmC/eYafHVKfdfdFtiErsG5aglo25KUhauQPhZz86ueUnHuSx5//r657QuyIRMYAZa+P3fnxGerprfNa7FK8JCP762j0Zeww5vvsEjlJ2EHQk3so5OQ3LziJegIEHdIdMP9dionz8yHPN2WokOrGrn028v2uUI5jSljPI6P66+5/il1p9GXMMOY1+j1AsZyhuMpz8tstIhm3Jx6K/PcpXKUcV6xb0lSkTfBWkds+7z6rp+3ReWwRBP0RvJeufo2+jB3mayQdww6C4jhDICe/VSvyMYmeHGK1mL+bYgL9dMizf+kLPE+UyZ116ddhAZtjdrcJkH+7J4Thx72faX4iY4T6LIx76RhS9mbL4vZFTn6b5B4fFFp09k9zIFP/pEsomkNfCdpLL4NFaX2s0hNjGcSwYPhpfd1XL32RJsBTxZiXjqHl7Xd3fuydnPi2KTjC2T9Fh+ad/bOTMvVP2obS0nSYXLWDWN1T9U4e2AXGL7+j0Zcxo9HvCYKDMJBDICe+bYx+Hc5BaT06r/xvpv412XBIpg1u9ytCV4fqQXe7cW8ox+WiYL5Z/jnEuBYZKxr9Hvl5iHSiPqm7OduIwjf1T9rk4BDHSO+urtYjm4+gJ3lgBfhbnInasETGika/R74d+h87P/YGEfcMgLZTOJal/pHJYOqfrEPu7vEo7XYev4gc+6lVxh9Gn0A+yvWu24tfZAgwfhnv0gOcQfZdjvdvQwwAevx3BdHQ7PLrEy7K1D/+v8gittndz1IffxxL7UYafbg1hPE3U0XGhEa/R4g0fmDnx96gzjgD4NLqqlvY/Z8empf6x+6ftCvP/iVhR7/t7n6WemW+E3hgF4jeT6O/f4ixS6Gtvo/pRFZFo98znCX2uVMgEpkB0HdkPWf/PwvlBFwXudEfCOn+ny4fDbFAbmJ3X+eaUI6zH/DALtBwJ40+MG4J7MP4r5o1INInGv2e6bNID54Gvnzc6kMxqLn7z2CTWTHpugCYDhhSSuaeF1onMn9Vjgzl2FolmG9ecR6CUTH6yLRUGToa/Z7ps0hPRi/3fcSwCCZ8dmLzUv8Q56suAMqE6pDcG2R+HMADLbGot/4iGHPz8vRZQPM4Y1XDL0PG6P2e6bNIDyVz+fKPrq6GDbupRW1/EUVZ3hKScYOxJ7iVdNYuouPxHtTHUdbYXwQ7/UXFeTjXZ7fP/2cRIDJEcqxLT/RVpIfJjS8eIzq26ONcACzyAPww5G5rXLDjvjjUlbGvg6HOsfNmHljCMqMPvA/uqcdDbRxHiGwL45wxLz1CulzX0b/p2h9SX/9NYAGw6AiAx64KWQBouFAjn9Q33PiH80APUGUvxwxHCsvgKIyqksv46xDPRVqqR08yNBibs3Ep0jEU6Tls58fOyImO6ONSoPkPvdPnLQBIATw3ZGpV/2AIPxi6P3Rn6A2hPqn31sdLtIxFZ/qzEHjI85FeKDIUCJRmXOKJkh4hDenynR874cMhvvhlfcTHzrtC7LTmxQA8GjoqJN3BEdKfhc4Oket+WWgo5960ic6xsVsE/30hFpCrcGOI5+y7AJdIQlAsY5IFt/QIZ3+rTiTbwi6Lv8UXjxuydDA29PengEpO7Ck8AhQlohyrtAOG/Vsh3InskOlxv1uwXNfgIaqPiWUwWe7m3q9DSi7PS5Mpkb7h/mM8Xl9dSa/g5uwiX/+sEF86+e5TA+OOAZrn/sdtu1sQl6wO3qu7Q/n5cuzyR6EhkkGtqWUBeIyTdRboLLKz6BQVJkX6hKNkxiJHndIzXwj93c6PrYHRw92NSI2aMu8LzasASAe1b4Sm/vlsArt6zrLZ0fNZsrv/UmgMpZTrx0DLej9g9NfZ6QPeJoJ1ee5P8IBIT2TPk3dWV9Ir1BJv84ydHQdn2U48z4bAvvNDGPuc9FNM8CwOjMBeDLvkz4WIas/PjT71BOqNiXoE/2d4YAGc6a9r9IFxxiIIL5PppNIXjF3GuHUkBgLNb/5y58fG2RPiyzaaeDEYePLEc/JPsQuk+5+T9Q7sXNN9n0clVPkiz36sk8ntofy+SfNcBN6hTYw+vCDEWLJ4j/QBm5cc425kBgJuRdKHmibTh9iN+WXvDrsyoszn9f6nqMUUg/847qC2A/n09c8Dw19C2uc5oXxPZBcsAqO/TY5z5vA/EvJelC7ZL8TYY4EuA4Id5X/e+bERPhvii+67m99Yyfr/89z/VGfrq6BM22DkKc98SwgjV3/fHHswrkpqe4yXJ9/fskmR976N0YdcYNxcXYl0wydDjDvmLRkQlPHkTLQJyFXnS2aSsijN9uDexxNTD/pCLAiICxjzZ/zy0Kkhdu557pfCCNJ2lh19qQtH3O3197wIvGXbGn3IzIYTqyuR9uFolzHHxlIGBsU8/mbnx41hh8p5KxpqqtRYwfAdH8rAyLpo/DP0vurs4gm04wyeVf/sIgYjz/s4LcQidCrUP4Pn88AcMPqrVOTbDcbQUyH+FnUCRNomG6ydUF3JoCAimsll0yIm5BkzoTCZv4oHpDUwinRJnM39xyMwBIOJgT8khIGnsMy8Ywp2rvRgYNc55UJF9cY7i1Ka1i3OswzOWDOwzxRRaZsc3wdWVzI42IlRSGFdCA5K1yER1tINfO6cCxM7kYYDXRTqyiWOi5qKW5S4JRhtnoHHyLC7p7HMoaGSzuW3ZZXGOxS1asroA3ET/D1iBQzsk7ZgDsqx7QJzwBBAduTOjyvBpHFHiC+W89mSYNAeE2JXTQQ57md2pzRIYZHDzxSeQPw/xGdBFTQqHSK8Hl3sZN8aqqf+sZNrOhWTM3h25tR355hh1kWfovww6Winh6bkqt8ExlJ+bosa7zRt9OF7If7m16srkebh3meM4ZGUAYObnkl91cmaVDK+WKqglQQGv178ZVbzDB7Gjt0T9QkIXKF9K/9lUcAOmJQpFlWfCu0faoO/DXE+nq/p6tC6sCqn/SxlMwnwnA2yq4v3xXvm3/I7XXkYSmGVxjuMw6aNPt8Tz8k4tg20tAFZRoxr5kUZOOxMMVx0R5oHC4MjQpwhfyf0H0OlQRU0Buy1oaYM2b6hd4S+HCIynednYcDC6YshmvQQrY93AC/Bpn8X7wsllvPMH3fuPPh3/C3+LVG2GXSzTNzAdGckql43/fbUG+9ggOfR5Jl+nex+RiyObn5pGjx9jC+8fjICMPwYdXaKJ4XIt/z7EB28mICY+AkKKpG3hxisTaUxLuPFocNCFKH5aog8aj53XL1ZqIeb5pIQeeqIM/RZHRei1HGK58NY8BwsyvDc8B1S+Y2+1vUAskVi58n3T4lY3fTtsErjHY6W2uqIyX3M32XsiTRJtnm+sLqS0fAXoTNCpFzglp5CZD5GkcE6FEPHUcCHQiy+aJc6T7jX+Z5YHLBrJ76Ac/dVjDticUBXOlLmOLuX7qgfFc1rvNPWTh/Y4RP/oZtfmobAXsa06aEyaMiVZqASVT1kOHd/U4hIfWIGZiP4l4l/S88FvAGLjnCkO9jF53czr/EOMRNt7fTh4BB/m4WFbn5piszkobGbyGChAA4DlR3zEOBcnwA5MiPIDODcfV4Q4Tzx71i8kIbJGfyU8+GHTD2Cf16ZXAL52jT68K0Qf39Z4x+RVcljqyYqSYq0CtH1DFbKCXcNbnXO5zOobl7O+yLxb+mffmXIILtxcVYov8d5ra43ba27Duzws1pfH2NfyiIj92+rrkQGTJ5DtXmej2uevHqCqMitX/XcPYUBwBDg2qfSlWly44az9PxuWbzNsk1r3XWg0BLeIV6DbXhlG/CUMp4/Xl2JDBiCmhisTZxDsXsi9Y6dHMV85rXN3U3svgiw4/wdN7+Ux24R/ETvN1F7fxWyDS/jbl4mgcgqsEhlHFmJTwYPZ1AM1nUDmvLsnZ74BNbVC+SsInZYBNhR9wD3qily06IepzEbwU8gX1dGH/J8n8WGgX2yLhh6xo/n+TIK0tW+G3UDn96BVcXNQC16Auxwz3v+LvUa/AST1mkzZW8eGPqsRskRksg6HBVi7FxfXYkMnFmjP7uDX9fAE3VNdPZ5IQy8OyeZBwWYcszMRtBjgLvc6QML0fRWERgqsirMk4wbqouKDJ6c6LIKWk7Eq4hz0FtClNQ1/13W4ZxQjiOCSet0Fcg3y0tDHDuwEDawT1aBTQ3lv5EbHBkc7OKpFkUXvXWK26A08LS2fVlIZBsYRzm2Zs9C+zL6QOlmXhM9F5zEZTey0BNjVmQQkAuPkc+c5FXEjp8GORp4aYsMfkrV6dPoQx49EOAnsgwalDFWqDci0gvsTg4N0SioHiG9m6hBr4GXLqmPP8pBJ10H8s3CPZSLZM/3ZRk5Tkz3lE5hkmJyYrKsT6SzYhFAFbtvhjLILqvgGVEvXVMv0lQPgnoo1KfRB9IIeV3cH+Zeyzwo880Y4ahUpHUw0qQ6kQqXE+c80UHv4tC8bmaQgXwUTBHpEs7Nc5zSNTEZgtEH2jrz2ojOFpnlCyHGh610pTVwv18Qquc4zxOTKWdMq5SrzYn3hdWVSHdk/3F0Fw/sZShGHzJ/f7aWgEiOjUUbKpG1Sbc9ncjSDb9ILAQ+GlrXTf+9EL9PHIBIl3wqlOO37iJtu7XuOhBrQDoWR2Om8UnChooxwdgQ2RoMPbXrdwvEI795E0Nf59MhnqvuXhXpAmo75FiuN94ZktGHo0O8RppFsRAXYZPEmPDoRzYG1z3uTlaOORHOCpcnzWjItW8K/i7PTWyASJcsarwzNKMP6RGzTK/ATSHGA5smkZVh0vtaKJvezFPu5p8bagN2LngU6jstka6oe7PybHRIZ/oJ90lWrLT/vmR5cjM7ZCWY3G4LLXLfE9REYZyuXIkZLyDSNfXKkJ/ngaDvPP1F1Pvvk64l0ySPpciMElkIBvykUD03uS4mP3b0q0TcN02uWs3Vl66htHPeA3fwQDBUow/23xfinxgDxkHJXNgR0DN+3q6e83vaMdarkfXBL0K8Hl1V0jWfC+X9QAYK0PwJozpUKG7F66WbpIF904MFKd+/2RzyLF4dooZ4Tmh1cTbIrn4oE8aPQ7wuW0NK12TlO5TpT9w3Qwvkq8N9mznaLABkOrwkxPeua1+e4a2h3DnPinr4rwsNjWxzemp1JdIt9XuE4FbukyEbfeAoLANwDeybDpeG+M7Prq5k0rBLrpcVTXGGTxncIZ+XkwLIa61XRRPpitka/LeGhm70Yb8Qx3Z4KGxWNQ2ybPnzqiuZJOxMCEDKSStFYN4RoTGAu5LXbNqe9AEpennffCa0JzTUQL5ZMrCPRYqBfWXDkS3fNWnUMlFw680W0+Gs75DQ2Mj4A3cs0jX1GvwUv+F6LEYf6LvPaycAUcrlhhDfs73zJwg742tCOVEhzvFfExor2THKNBTpmjwnRVS+Izgug/rGQD2w70wekCKxI+lEIU1jtsvdl0Njh3Q93gv99kW65JRQ3kt3hk7f+/OYMLCvbLJcOUdRMiEODNXd+Zzj7RsqhVzM9F03QKZFPVefY6aDQ78MjY0M7CM2xhzuskhv1MerK5kEp4VyYkL3hvqontcm2VTkmOpKpBuIgcn7imMy7itcqWMkO/KRyTOUOhyyPWzw+F6N2p8IWXYx9cVQidDSl/dHypRIV7w8lPdWGnu8TmMtC31liPdyc3UlYydd+7RWlglQdz3iunttqFRIOeJ9jtG1KuMl40nyHgPq779g58fRwQ7/JyHez2d5QEZNuvaJNZHCoW5+1sznLJ8SjKXzaIj3a4SqdAljLoUL9fuh/xoaK3gpmDMQ+d0yXrILqb1JJkDWpEeH8sAEyJzjj1VXIt2QTUwQbaVpUnVYaMyQwsumAc+ZhXvGCZkYjElrMEwAdhs5CU3pC89zffr9i3TF3aG83yjOc0aohCIox4d4Tw+HDOwbH/eF+P5Mw5wAXwnlJEQnsKmQ5/rkHIt0xf8O5f1GBP9JoVLOw88L8b5oxSvjITtAjqk6pGwBK3O+cJqBTI1sHOQZlnQFee2MOYRLHKOPSoHOgby366orGQOkZfOd4a2RCZAlFznXnxpXhHjv5utLl2TAFKIsNDvkUsC1n4b/6zwggyZ3+U+HPJaZCDn5XFJdTYs817+puhLphkdCed9dELosVBIYj3tCvL+SvBglkmORYksyEXLymWIUOxXReO9j6Gku5fDtUN53eJtKXHRi+GnNyns0OGyYfDDE90NsiUwEbsycfOjvPUWyDn9ppYZluJwQyvvu2lCpR2vcU0+FyOGfUpDwGKA+CXFcjEHbjE+MnHxOrq6mx3dDvH/qoot0wVtCed/dEqJQVKnQqAujT2S4hbCGA94lxt/l1ZVMitzpXlVdTQ+ONXj/Bh1JV9Rr8FPGdqxNd1aF4j0YfnP4hwGVExl77PT9PiZIRrBPNUczU6isRCVdwY43jT5V7MbcdGdVsivfnupK+iQ3esZaTBTq7OcEdD4PTBBSqCxMIV2S56nk6pMn/dJQ6Xw5xHueavzQEDglxHfwYHUlkyXr0KOzeWBiZKcwzxylK9jx5j13fWjs9fdX5bjQz0J42KRbmN84ZmGh6ecvz/TGRleHpnTWc1qI9/3O6kqkfeqtrFkATGmxTVdPugtaCbNbmNcZbwbvyTN8IsRKkIHBGfdUDD8pK7xnS4dKVxwYSqNP9P5doSnBseIPQnbl6wY+b3b4zO+lx4/ImmAA87yR4hpTyV/nhvBcX7qiXoOfYD4qo00NDNHtIY/V2ueOEGPtS9WVyAxMSNmM5rHQc0Olc2eI94vrUaRt6oWxqHvOAnuKsMm4NaThb4+sr09qqCl6shB2+A+FGCxEt78pVDK0N+W9nlpdibRPNrvCszZVow8YJeIaNPztYH19WRlWhQR9MGDQN/Y+ViKZujjFboPSD5zjM+Y4Wpp6D3oKxmj4m8f6+rIRR4ayHSi1tLlBS4PFDJMvrlaRLvhoiHsKfYcHJs7rQ5Qltg9GMzwvZH192RjO+dkF5yR1Tqg0uEEw/CJdQBR13k+k8MlOlThTyprB+vrSCKT15a6fM/+S0j+I3ud9iXRF3kvWiPhXWAAZW7Mdh4YYV3guDd6TrcFt9KMQg4q8z4NCJaDRl64hO4YxZ776s6FY2OE7P8qaEBeRQaKlzM0yEE4MMbDQeaGxryjJl9boS5dg7O01//twro97mg59sh65IWPhJNI4TFi/CTHIaOIw5prOj4c80xcZBuxYCez7k+pKViE3Yk+GdOtLa7AqvybEYMNojrXqU+azisgwIGaImvFTKBC2LWQ/MP9y5GpDHemE94dy1z/G1L6syucKWWQ4YPDpBKrhXwzNizI9zz750imcUd4QYvAhmmqMJcKfTme85v2qKxEZCmn45ffhs/lViLmL2CqRXuCsn90+AxF3E2Vuhw7uMV7vUdWViMiwweBnAPLNPCDSNxj7zEWmvviQI5XxUvA6rY4mIkPn+SHy8JmzfhLyWFIGA4F+dZc/XbWG2s2OmAT6m4vIeDg59Oc7P04CNk+5meIIVYMvg4SmNveH0vhfHxpalCkthe2rLzIucHNfGKI8eElVQudBVVSi9JlDr+IBkaFDlShy4tP4Xxci+nQI/Dxk0x2RccIO+PbQe6qr8vhmiDkTo388D4iMCVJL2FnnIGbnjzegT9jlY/hFZJxwnEjvePL7/5QHCgDvxX0h5kpS8w4IiYyWI0JZgxxhdCnw0/Xun3MxMg1urK5EZMxg8DH8x4bG3K6XtrgZsPdAyHoFUgykzOGaS+OPCKrDjdXFTfvBEH+TXYKIlMHbQswrY6zjT7fBPL+/iAdESgQD/+4Q/ftzwLft/v+LENGwtqIUKQ/c42eGOBMfSvzQMtjdPxxi7sOdT6tckUnAAuAjoTz7RxT9IVL3wNC2HgAmgOwbgGsfb4OIlAnGk+PDoRp/spn2hHKuuy1E4yGRScLql8I5mZ+aYiVMsxzSATnzqovOf4jiFfn/H9r7WH0h8dPQUGsHiEizEEeE8b8ixLzSNxxBUL8k5yO65Lm7F6mBm5/zLlbC3Lz1QMBVxUKBQJ8hVwkUkfb4n6F7QveGSPPr4miPv/Gq0Bkh/jYexpyTmMdoXCYiIiItQQrcZSFSdvkvCwDK3G5KGvbDQl8N0cETT+NvQ/WNB6J2/gWhvtOVRUREJgXGmt3/pSE8iBhlIv/vCLEzp5cI+mSIyngEB94S4riQTKP6rn1WHEPSfwTvJJUDiUkyaFhERGRA7B96dQhjf1II44/YwVMG94chjgpZJGD8eYyd+4mhN4aGVmpcRERERERERGRX9tnn/wP9K4Kkh7DMDAAAAABJRU5ErkJggg==" // 签字图片地址
  });
  const wsPromise = new Promise<void>((resolve, reject) => {
    state.ws = new WebSocket("ws://127.0.0.1:29999");
    state.ws.onopen = function (event) {
      resolve();
      console.log("SUCCES:", "签字笔服务链接成功");
    };
    state.ws.onmessage = (msg) => {
      onMessage(msg);
    };
    state.ws.onclose = function (e) {
      console.log("签字笔断开: " + e.code + " " + e.reason + " " + e.wasClean);
    };
    state.ws.onerror = function (event) {
      console.error("WebSocket error observed:", event);
      reject(event);
    };
  });

  function onMessage(msg: MessageEvent) {
    try {
      var obj = JSON.parse(msg.data);
      var msgID = obj.msgID;
      var HWPenSign = obj.HWPenSign;
      if (msgID == "0") {
        switch (HWPenSign) {
          case "HWGetStatus":
            state.status = obj.DeviceStatus == 1;
            break;
          case "HWInitialize":
            break;
          case "HWGetSign":
            state.signatureImageUrl = obj.message;
            state.clickDone = true;
            break;
          case "HWClearSign":
            state.signatureImageUrl = ""; // 清除签名图片地址
            break;
          default:
            console.log("INFO: 签字笔消息", obj.message);
        }
      } else {
        console.log("INFO: 签字笔消息", obj.message);
      }
    } catch (e) {
      console.error("Error parsing message data:", msg.data);
    }
  }

  async function sendHWInit() {
    await wsPromise;
    if (state.ws) {
      state.ws.send(`{
        "HWPenSign": "HWInitialize",
        "nLogo": "标题",
        "hideTitle":"1",
        "width": "650",
        "height": "320",
        "nOrgX": "215",
        "nOrgY": "1212",
        "bordercolor": "ffffff",
        "backcolor": "ffffff",
        "fingerFap": "0",
        "key": "213976028BD977A3648CABBC0C9A33F9",
        "setText": [
            {
                "text_x": 20,
                "text_y": 30,
                "text_t": "ESP500Test"
            },
            {
                "text_x": 20,
                "text_y": 50,
                "text_t": "TextTest"
            },
            {
                "text_x": 20,
                "text_y": 70,
                "text_t": "helloworld"
            }
        ]
    }`);
    }
  }

  async function closePad() {
    await wsPromise;
    if (state.ws) {
      state.ws.send(`{
        "HWPenSign": "HWFinalize"
      }`);
    }
  }
  function sendHWFinalize() {
    state.signatureImageUrl = ""; // 清除签名图片地址
    closePad();
  }
  async function affirmClick() {
    await wsPromise;
    if (state.ws) {
      state.ws.send(`{
        "HWPenSign": "HWClickOkButton"
      }`);
    }
  }

  function againSing() {
    if (state.signatureImageUrl) {
      state.signatureImageUrl = ""; // 清除签名图片地址
      sendHWInit();
    } else {
      if (state.ws) {
        state.ws.send(`{
          "HWPenSign": "HWClearSign"
        }`);
      }
    }
  }

  function setImage(img: string) {
    state.signatureImageUrl = img;
  }

  // 在组件卸载时关闭WebSocket连接
  onUnmounted(() => {
    sendHWFinalize();
    if (state.ws) {
      state.ws.close();
    }
  });

  // 返回状态和方法给组件
  return {
    ...toRefs(state), // 将reactive对象转换为ref对象，以便在模板中使用
    sendHWFinalize,
    closePad,
    sendHWInit,
    againSing,
    setImage,
    affirmClick
  };
}
