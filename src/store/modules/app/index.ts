import { defineStore } from "pinia";
import { Tween, Easing } from "@tweenjs/tween.js";
import eventbus from "../../../utils/eventbus";
import { SimpleQueue } from "./utils";
import { apiPostFile, apiPostDoubleRecord, apiGetTipList } from "../../../api";
type position = {
  width: number;
  height: number;
  left: number;
  top: number;
};
const simpleQueue = new SimpleQueue(200);
const basePos = {
  width: 15.845,
  height: 27.324
};
const positionEnum: {
  [key: string]: any;
} = {
  init: {
    width: window.innerWidth,
    height: window.innerHeight,
    zoom: 1,
    left: 0,
    top: 0
  },
  // 450  776
  // 数字人居中位置
  center: {
    zoom: 3,
    left: 11.5,
    top: -1.8
  },
  mp3: {
    zoom: 1,
    left: 8,
    top: 52
  },
  hide: {
    zoom: 0,
    left: 8,
    top: 52
  },
  top: {
    zoom: 1.5,
    left: 23.4,
    top: -8.451
  },
  readCard: {
    zoom: 1.5,
    left: 15.563,
    top: 82
  },
  // 密码输入弹窗
  dialog: {
    zoom: 1.5,
    left: 22.8,
    top: -9.8
  }
};

const sleep = (time: number) => {
  return new Promise((resolve) => setTimeout(resolve, time));
};
let runTimeCount = 0;
interface AppState {
  /** 主体内容全屏 */
  humanFocusDelaySync: boolean;
  humanInteachMp3: boolean;
  isSaying: boolean;
  humanServerRun: boolean;
  humanPosition: position;
  // 流程的所有数据
  idCardInformation: any;
  headShot: string;
  salePassword: string;
  saleDepartAddressInfo: any;
  phoneNumber: string;
  customerInformation: any;
  riskTestResult: {
    score: number;
    level: string;
  };
  businessQuestions: any;
  openAccountData: object;
  storeManageBank: object;
  manageMoney: string[];
  signVideoUrl: string;
  resultImg: string;
  doubleRecordVideoUrl: string;
  tipList: Process[];
  bottomIsShow: boolean;
  inHumanBodyStatus: boolean;
  isInActiveOpenAccount: boolean;
  isInhumanInteach: boolean;
}
type ProcessStepContent = {
  id: string;
  step: string;
  content: string;
};

type Process = {
  process:
    | "身份认证"
    | "信息录入"
    | "业务申请"
    | "协议签署"
    | "见证双录"
    | "业务提交";
  stepContents: ProcessStepContent[];
};

const baseInfo = {
  userName: "",
  nationality: "",
  sex: "",
  nation: "",
  idCardType: "",
  emailAddress: "@qq.com",
  idCardNo: "",
  idCardValidity: "",
  idCardAddress: "",
  issuingAuthority: ""
};
let leisureTime: any;
export const useAppStore = defineStore("app-store", {
  state: (): AppState => ({
    // 见证双录的时候是否在说话
    isSaying: false,
    humanFocusDelaySync: true,
    humanInteachMp3: false,
    humanServerRun: false,
    humanPosition: JSON.parse(JSON.stringify(positionEnum.init)),
    // 流程中的所有信息
    // 身份证相关
    idCardInformation: {
      ...JSON.parse(JSON.stringify(baseInfo))
    },
    // 头像地址
    headShot: "",
    // 市场密码
    salePassword: "",
    // 开户营业部地址信息
    saleDepartAddressInfo: {
      address: "",
      id: "",
      name: "",
      sortNo: "",
      index: 0
    },
    // 验证的手机号
    phoneNumber: "",
    // 开户营业部客户信息
    customerInformation: {
      countyLevelCity: "",
      beneficiary: "",
      badRecord: "",
      chinaPeople: "",
      userName: "",
      idCardType: "",
      idCardNo: "",
      emailName: "",
      emailAddress: "",
      idCardAddress: "",
      province: "",
      city: "",
      district: "",
      addressDetail: "",
      postalCode: "",
      secondContactName: "",
      secondContactPhone: "",
      secondContactRelation: "",
      educationBackground: "",
      occupation: "",
      annualIncome: ""
    },
    // 风险测试，得分/等级
    riskTestResult: {
      score: 0,
      level: "C3"
    },
    businessQuestions: [],
    // 证券账户开户和基金账户开户
    openAccountData: {
      // shAnewOpen: false,
      // shAtransAccount: false,
      // szAnewOpen: false,
      // szAtransAccount: false,
      // shMoneynewOpen: false,
      // shMoneytransAccount: false,
      // szMoneynewOpen: false,
      // szMoneytransAccount: false
    },
    // 存管银行
    storeManageBank: {
      bankCardNo: "",
      bankName: "",
      bankId: "",
      bankCardPassword: ""
    },
    manageMoney: [],
    signVideoUrl: "",
    resultImg: "",
    doubleRecordVideoUrl: "",
    tipList: [],
    bottomIsShow: true,
    inHumanBodyStatus: true,
    isInActiveOpenAccount: false,
    isInhumanInteach: false
  }),
  actions: {
    changeIsSaying(value: boolean) {
      this.isSaying = value;
    },
    changeIsInhumanInteach(value: boolean) {
      this.isInhumanInteach = value;
    },
    /**
     * 打断数字人说话
     */
    breakHumanSay() {
      eventbus.$emit("breakHumanSay");
    },
    changeHumanInteach(value: boolean) {
      if (value) {
        this.setHumanPosition("hide", { useTween: false });
      } else {
        this.setHumanPosition("center", { useTween: false });
      }
    },
    changeHumanInteachMp3(value: boolean) {
      this.humanInteachMp3 = value;
      if (value) {
        this.setHumanPosition("mp3", { useTween: false });
      }
    },
    // 播放音频
    async playAudioInHumanInteach(text: string) {
      await new Promise((resolve) => {
        simpleQueue.enqueue(resolve);
      });
      this.changeHumanInteachMp3(true);
      eventbus.$emit("breakHumanSay");
      eventbus.$emit("say", text);
    },
    // 停止播放音频
    async stopAudioInHumanInteach() {
      await new Promise((resolve) => {
        simpleQueue.enqueue(resolve);
      });
      this.changeHumanInteachMp3(false);
      eventbus.$emit("breakHumanSay");
    },
    /**
     * 设置人物的位置
     * @param status - 位置的状态
     * @param useTween - 是否使用补间动画
     */
    // setHumanPosition(status: keyof typeof positionEnum) {
    //   const data = positionEnum[status];

    //   this.humanPosition = {
    //     width: remToPx(data.width || basePos.width * data.zoom),
    //     height: remToPx(data.height || basePos.height * data.zoom),
    //     left: remToPx(data.left || NaN),
    //     top: remToPx(data.top || NaN),
    //     bottom: remToPx(data.bottom || NaN),
    //     right: remToPx(data.right || NaN)
    //   };
    //   console.log({
    //     width: data.width || basePos.width * data.zoom,
    //     height: data.height || basePos.height * data.zoom,
    //     left: data.left,
    //     top: data.top
    //   });
    // },
    async setHumanPosition(
      status: keyof typeof positionEnum,
      {
        // 传入的参数
        // 是否使用补间动画
        useTween = true,
        // 动画时长
        duration = NaN,
        time = 500
      }: { useTween?: boolean; duration?: number; time?: number } = {}
    ) {
      runTimeCount++;
      const data = JSON.parse(JSON.stringify(positionEnum[status]));
      // console.log(data, "newPosition data");
      const newPosition = {
        width: data.width || remToPx(basePos.width * data.zoom),
        height: data.height || remToPx(basePos.height * data.zoom),
        left: remToPx(data.left === undefined ? NaN : data.left),
        top: remToPx(data.top === undefined ? NaN : data.top)
      };
      // debugger;
      console.log("INFO:", "改变数字人大小位置信息", newPosition);
      const oldPosition = JSON.parse(JSON.stringify(this.humanPosition));
      const nowRunCount = runTimeCount;
      if (!isNaN(duration)) {
        await sleep(duration);
      }
      const sleepTimerIsNotNew = nowRunCount !== runTimeCount;
      if (sleepTimerIsNotNew) {
        return;
      }
      console.log(useTween);
      if (useTween) {
        const tween = new Tween(oldPosition)
          .to(newPosition, time) // 500毫秒的动画时长
          .easing(Easing.Quadratic.Out) // 缓动函数
          .onUpdate(() => {
            // 更新 Vue 实例的数据
            // this.$nextTick(() => this.updatePosition());
            this.humanPosition = {
              ...oldPosition
            };
          })
          .start();

        function animate(time: number) {
          requestAnimationFrame(animate);
          tween.update(time);
        }

        requestAnimationFrame(animate);
      } else {
        this.humanPosition = newPosition;
      }

      // console.log({
      //   width: data.width || basePos.width * data.zoom,
      //   height: data.height || basePos.height * data.zoom,
      //   left: data.left,
      //   top: data.top
      // });
    },
    /**
     * 重载页面
     * @param duration - 重载的延迟时间(ms)
     */
    changeHumanFocus(value: boolean) {
      clearTimeout(leisureTime);
      console.log(
        "error: 识别到人体状态变化，进行定时器设置，接收状态为",
        value
      );
      if (!value) {
        const delay = this.isInhumanInteach
          ? 60000 * 10
          : this.isInActiveOpenAccount
          ? 60000 * 2
          : 20000;
        leisureTime = setTimeout(() => {
          console.log("error", "没有识别到人体超过的阈值，回到首页去");
          eventbus.$emit("setAllBodyVisbile", value);
          setTimeout(() => (this.humanFocusDelaySync = value), 0);
        }, delay);
      } else {
        this.humanFocusDelaySync = value;
        eventbus.$emit("setAllBodyVisbile", value);
      }
    },
    changeHumanBodyUseHumanAnswer(value: boolean) {
      this.inHumanBodyStatus = value;
    },
    changeInActiveOpen(value: boolean) {
      // 是不是在开户
      this.isInActiveOpenAccount = value;
    },
    changeHumanServerRun(value: boolean) {
      this.humanServerRun = value;
    },
    changeidCardInformation(value: any) {
      this.idCardInformation = value;
      this.customerInformation.userName = value.userName;
      this.customerInformation.idCardNo = value.idCardNo;
      this.customerInformation.idCardAddress = value.idCardAddress;
      this.customerInformation.idCardType = value.cardType;
    },
    changedepartmentInformation(value: any) {
      this.customerInformation.choosedIndex = value;
    },
    changeHeadShot(value: string) {
      this.headShot = value;
    },

    changePhoneNumber(value: string) {
      this.phoneNumber = value;
    },
    changeCustomerInformation(value: object) {
      this.customerInformation = value;
    },
    changeSaleDepartAddressInfo(value: object) {
      this.saleDepartAddressInfo = value;
    },
    changeRiskTestResult(value: any) {
      this.riskTestResult = value;
    },
    changeOpenAccountData(value: object) {
      this.openAccountData = value;
    },
    changeManageMoney(value: string[]) {
      this.manageMoney = value;
    },
    changeStoreManageBank(value: object) {
      this.storeManageBank = value;
    },
    changeSignVideo(value: string) {
      this.signVideoUrl = value;
    },
    changeResultImg(value: string) {
      this.resultImg = value;
    },
    changeSalePassword(value: string) {
      this.salePassword = value;
    },
    changeDoubleRecordVideoUrl(value: string) {
      this.doubleRecordVideoUrl = value;
    },
    replaceTipsText(text: string) {
      const name =
        this.customerInformation.userName || this.idCardInformation.userName;
      text = text.replace(/C\*/g, this.riskTestResult.level);
      text = text.replace("{{姓名}}", name);
      const sex = this.idCardInformation.sex === "男" ? "先生" : "女士";
      text = text.replace("{{称谓}}", sex);
      return text;
    },
    async uploadVideo(id: string, url: string, name: string) {
      await apiPostDoubleRecord({
        fileName: `${name}-双录视频`,
        fileUrl: url,
        idInfoId: id
      });
    },
    async getTipList() {
      const res = await apiGetTipList();
      this.tipList = res.data.data;
    },
    resetStore() {
      const oldList = JSON.parse(JSON.stringify(this.tipList));
      const oldPosition = JSON.parse(JSON.stringify(this.humanPosition));
      this.$reset();
      this.$patch({
        tipList: oldList,
        humanPosition: oldPosition
      });
      this.businessQuestions = [];
      this.manageMoney = [];
    },
    // 小键盘显示 bottom消失
    changeBottomStatus(value: boolean) {
      this.bottomIsShow = !value;
    },
    sayAndShow(step: string, process: string, animate = "") {
      eventbus.$emit("breakHumanSay");
      const result = this.getVoiceContent(step, process) || { content: "" };
      eventbus.$emit("say", result.content, animate);
      return result.content;
    },
    getVoiceContent(step: string, process: string) {
      const currentItem = this.tipList.find((item) => item.process === step);
      if (!currentItem) {
        // throw new Error("未找到对应的流程");
        console.log("Error:未找到对应的流程");
        return;
      }
      const pageData = currentItem.stepContents;
      const result = pageData.find((item) => item.step === process);
      if (!result) {
        console.log("Error:未找到对应的流程");
        return;
      }
      return result;
    },
    saveQuestions(questions: any) {
      this.businessQuestions = questions;
    }
  }
});

function remToPx(rem: number) {
  const remSize = (window as any).rem;
  if (isNaN(rem)) {
    return NaN;
  }

  return rem * remSize;
}
