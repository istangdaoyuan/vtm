export class SimpleQueue {
  queue: Function[]; // 任务队列
  isRunning: boolean; // 标志当前是否有任务正在执行
  time: number; // 任务执行间隔时间
  constructor(time: number) {
    this.queue = []; // 用于存储任务的数组
    this.isRunning = false; // 标志当前是否有任务正在执行
    this.time = time || 200; // 任务执行间隔时间
  }

  // 向队列添加任务
  enqueue(task: Function) {
    this.queue.push(task);
    this.start(); // 尝试开始执行任务
  }

  // 开始执行任务
  start() {
    if (!this.isRunning && this.queue.length > 0) {
      this.isRunning = true; // 标记为正在执行任务
      this.execute();
    }
  }

  // 实际执行任务，并安排下一个任务
  execute() {
    if (this.queue.length === 0) {
      this.isRunning = false; // 标记为没有任务正在执行
      return;
    }

    const task = this.queue.shift(); // 取出队列中的第一个任务
    if (task) {
      task(); // 执行任务
    }

    setTimeout(() => {
      this.isRunning = false; // 完成当前任务，标记为没有任务正在执行
      this.start(); // 尝试开始下一个任务
    }, this.time); // 设置200ms后尝试执行下一个任务
  }
}

// 使用队列
// const queue = new SimpleQueue();
//
// // 添加几个任务到队列
// queue.enqueue(() => console.log("Task 1 completed"));
// queue.enqueue(() => console.log("Task 2 completed"));
// queue.enqueue(() => console.log("Task 3 completed"));
