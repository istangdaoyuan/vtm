import server from "../utils/request";

//风险测试
const surveyList = "/user/vtm/account-demo/survey/list";
export function apiGetSurveyList(data: object) {
  return server()({
    url: surveyList,
    method: "get",
    data: data
  });
}

// 获取身份证信息
const cardInfo = "/user/vtm/account-demo/id-card-info";
export function apiGetCardInfo(data: object) {
  return server()({
    url: cardInfo,
    method: "get",
    data: data
  });
}

// 获取开户营业部列表
const bankDepartList = "/user/vtm/account-demo/banking-dept/list";
export function apiGetBankDepartList(data: object) {
  return server()({
    url: bankDepartList,
    method: "get",
    data: data
  });
}

// 获取开户营业部列表
const bankDepositList = "/user/vtm/account-demo/bank/list";
export function apiGetBankDepositList(params: object) {
  return server()({
    url: bankDepositList,
    method: "get",
    params: params
  });
}
// 获取当前银行卡信息
const bankInfo = "/user/vtm/account-demo/bank/bin";
export function getbankInfo(params: object) {
  return server()({
    url: bankInfo,
    method: "get",
    params: params
  });
}

// 获取协议
const protocolList = "/user/vtm/account-demo/agreement-sign/list ";
export function apiGetProtocolList(data: object) {
  return server()({
    url: protocolList,
    method: "get",
    data: data
  });
}
// 获取金融产品账户开户产品内容
const getFunds = "/dict-config/list";
export function apiGetDict(params: object) {
  return server()({
    url: getFunds,
    method: "get",
    params: params
  });
}

// 提交风险测试卷答案
const postSurveyAnswer = "/user/vtm/account-demo/survey/submit";
export function apiPostSurveyAnswer(data: object) {
  return server()({
    url: postSurveyAnswer,
    method: "post",
    data: data
  });
}

// 文件上传
const postFile = "/file-manage/upload";
export function apiPostFile(data: object) {
  return server()({
    url: postFile,
    method: "post",
    data: data,
    headers: {
      "Content-Type": "application/form-data"
    }
  });
}

// 添加双录
const postDoubleRecord = "/user/vtm/account-demo/double-record";
export function apiPostDoubleRecord(data: object) {
  return server()({
    url: postDoubleRecord,
    method: "post",
    data: data
  });
}

// 提交全流程所有信息
const putAllInformation = "/user/vtm/account-demo/id-info";
export function apiPutAllInformation(data: object) {
  return server()({
    url: putAllInformation,
    method: "put",
    data: data
  });
}

const getProvincesAndCities = "/area/list";
export function apiGetProvincesAndCities(params: object) {
  return server()({
    url: getProvincesAndCities,
    method: "get",
    params: params
  });
}

const getTiplist = "/user/vtm/account-demo/tip-voice/list";
export function apiGetTipList() {
  return server()({
    url: getTiplist,
    method: "get"
  });
}

// 投教内容展示
export function getInvestorMenuList(data: {
  level: number;
  parentId?: number;
}) {
  return server()({
    url: "/menuinfo/list",
    method: "post",
    data
  });
}

export function getInvestorContent(id: number) {
  return server()({
    url: "/contentConfig/page",
    method: "post",
    data: {
      menuId: id
    },
    params: {
      page: 0,
      pageSize: 100
    }
  });
}
