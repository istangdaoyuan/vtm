import { createApp } from "vue";
import "virtual:uno.css";
import naive from "naive-ui";
import router from "./routes/index";
import { setupStore } from "./store";
import initHuamn from "./utils/human-socket";
import { cwd } from "node:process";
import { readFile } from "node:fs/promises";
import App from "./App.vue";
// import { useArrayMico } from "./samples/node-api";
import axios from "axios";
import useService from "./utils/request";
import "./styles/base.less";
import "./styles/global.less";
import "./styles/reset.less";

const init = () => {
  const app = createApp(App);
  // console.log = customLog(); // 重写console.log
  // store plugin: pinia
  setupStore(app);
  app.use(router); //注册路由
  app.use(naive);
  app.config.globalProperties.$socket = initHuamn();
  app.provide("$axios", useService());
  // 设置根元素字体大小
  function setRem() {
    const baseSize = 16; // 基础字体大小
    const baseWidth = 1080; // 设计稿的宽度
    const scale = document.documentElement.clientWidth / baseWidth; // 根据窗口宽度计算缩放比例
    (window as any).rem = baseSize * Math.min(scale, 2); // 设置全局rem
    document.documentElement.style.fontSize =
      baseSize * Math.min(scale, 2) + "px"; // 设置根元素字体大小
  }

  // 初始化时设置一次
  setRem();

  // 当窗口大小改变时重新设置
  window.addEventListener("resize", setRem);
  app.mount("#app").$nextTick(() => {
    postMessage({ payload: "removeLoading" }, "*");
  });
};

// 判断是否是开发环境
if (process.env.NODE_ENV === "development") {
  readFile(cwd() + "/config.json", "utf-8")
    .then(async (data) => {
      (window as any).config = JSON.parse(data);
      const curHuman = await getHumanConfig(window.config.backUrl);
      (window as any).config.human = curHuman;
      init();
    })
    .catch((err) => {
      console.log(err);
    });
} else {
  const { ipcRenderer } = require("electron");
  ipcRenderer.on("configdata", async (event, message) => {
    console.log(message);
    if (message) {
      (window as any).config = JSON.parse(message);
      const curHuman = await getHumanConfig(window.config.backUrl);
      (window as any).config.human = curHuman;
      init();
    } else {
      console.log(message);
    }
  });

  ipcRenderer.send("getConfig");
}

async function getHumanConfig(pre: string) {
  const url = pre + "/security-account/figure/list";
  const res = await axios.get(url);
  console.log(res);
  const data = res.data.data || [];
  const cur = data.find((item: any) => item.isChoose);
  return cur ? cur.figure : "caitong_normal_blue";
}

// 定义一个自定义的日志函数
function customLog() {
  const log = console.log;
  return (type: string, ...args: any[]) => {
    if (typeof type === "object") {
      args.unshift(type);
      type = "INFO";
    }
    const timestamp = new Date().toLocaleString(); // 使用本地时间和格式
    const formattedArgs = args.map((arg) => {
      if (typeof arg === "object") {
        if (arg === Array.isArray(arg)) {
          // 如果是数组，直接返回数组字符串
          return JSON.stringify(arg, null, 2);
        }
        try {
          // 尝试序列化对象，如果失败则返回默认字符串
          return JSON.stringify(arg, getCircularReplacer());
        } catch (error) {
          return "Unable to serialize object";
        }
      } else {
        // 对于非对象类型，直接转换为字符串以确保正确显示
        return String(arg);
      }
    });
    // 使用展开运算符确保数组中的每个元素都作为单独的参数传递给log函数
    if (
      type &&
      typeof type === "string" &&
      type.toLowerCase().includes("error")
    ) {
      // 如果日志类型为"error"，则以红色文本输出
      log(`%c[${timestamp}] [${type}]`, "color: red;", ...formattedArgs);
    } else {
      // 对于其他类型的日志，正常输出
      log(`[${timestamp}] [${type}]`, ...formattedArgs);
    }
  };
}

// 辅助函数来处理JSON.stringify的循环引用问题
function getCircularReplacer() {
  const seen = new WeakSet();
  return (key: string, value: any) => {
    if (typeof value === "object" && value !== null) {
      if (seen.has(value)) {
        return;
      }
      seen.add(value);
    }
    return value;
  };
}
