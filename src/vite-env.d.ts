/// <reference types="vite/client" />

declare module "*.vue" {
  import type { DefineComponent } from "vue";
  const component: DefineComponent<{}, {}, any>;
  export default component;
}
interface Window {
  config: {
    arrayServer: string;
    readCardWs: string;
    urlPre: string;
    user: string;
    pythonUrl: string;
    backUrl: string;
    backSocket: string;
    pullPort: number;
    pushPort: number;
    width: number;
    height: number;
    useUeTransparent: boolean;
    human: string;
    feAlphaConfig: {
      similarity: string;
      smoothness: string;
      spill: string;
    };
    hardware: {
      mac: string;
      wideAngle: string;
      baseCamera: string;
      audioRecoredId: string;
      audioSay: string;
    };
  };
}
