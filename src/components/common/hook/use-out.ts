// 引入remote模块，用于调用主进程中的功能
// const { ipcRenderer } = require("electron");
import { ipcRenderer } from "electron";

export function useOut() {
  // 初始化点击次数计数器
  let clickCount = 0;
  // 设置连续点击退出的次数阈值
  const exitClickThreshold = 5;
  // 设置时间窗口（10秒）
  const timeLimit = 10000; // 单位是毫秒

  // 初始化一个变量来保存定时器的ID
  let timerId: any = null;

  // 获取图片元素，假设它的ID是'imageId'
  const imageElement = document.getElementById("imageId");

  const click = () => {
    // 如果是第一次点击，设置一个10秒的定时器
    if (clickCount === 0) {
      timerId = setTimeout(() => {
        // 10秒后重置点击次数和定时器
        clickCount = 0;
        clearTimeout(timerId);
        timerId = null;
      }, timeLimit);
    }
    console.log("click event", clickCount);

    // 增加点击次数
    clickCount += 1;

    // 检查点击次数是否达到了阈值
    if (clickCount >= exitClickThreshold) {
      // 达到阈值，调用Electron的API来关闭窗口
      console.log("click event done  out!", clickCount);

      ipcRenderer.send("close-all-click");
    }
  };
  return { click };
}
