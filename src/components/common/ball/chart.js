import * as d3 from "d3";
// import getPossiton from "./data";
import createLiner from "./defs";

// 财通证券、财通基金、财通资管、财通私募、财通
// 资产基金、财通资管公募、沪市TA、深市TA
let map = {
  财通证券: "嘉实基金",
  嘉实基金: "财通证券",
  财通基金: "南华基金",
  南华基金: "财通基金",
  财通资管: "南方基金",
  南方基金: "财通资管",
  财通私募: "汇添富",
  汇添富: "财通私募",
  资产基金: "华安基金",
  华安基金: "资产基金",
  财通资管公募: "建信基金",
  建信基金: "财通资管公募",
  沪市TA: "诺安基金",
  诺安基金: "沪市TA",
  深市TA: "华宝基金",
  华宝基金: "深市TA"
};

function changeArrPos(arr) {
  // 使用map调换位置
  let result = arr.map((item) => {
    if (map[item]) {
      const newItem = map[item];
      if (!newItem) debugger;
      return newItem;
    }
    return item;
  });
  return result;
}

// 假设没有圆球被选中
let selectedCircle = [];
const colorIds = [
  "linearGradient-white",
  "linearGradient-blue",
  "linearGradient-green",
  "linearGradient-orange",
  "linearGradient-red"
];
// 定义最小和最大半径
// 在SVG元素内创建defs元素
const coefficient = 0.15; // 半径透镜效果
const coefficientPosA = 0.0005; // 坐标透镜效果A  // 透镜的弯曲效果
const coefficientPosB = 0.004; // 坐标透镜效果B

const InitR = 60; //初始半径 最大形态
const InitInterval = InitR * 2; // 初始间隔

let centerCircle = null; // 中心圆
let CenterX = 0; // 中心点x
let CenterY = 0; // 中心点y
const totalNumver = 99; // 总图像数量
const transverseMax = 10; // 初始横向最多多少个
function remToPx(rem) {
  const remSize = window.rem;
  if (isNaN(rem)) {
    return NaN;
  }
  // console.log(rem * remSize, "rem * remSize;");
  return rem * remSize;
}

// 透镜-距离与半径转换
function LensConvertDistanceRadius(distance, orgRadius) {
  let realDistance = distance - InitR;

  // 当距离超过3个InitR时，开始平滑减小半径
  if (realDistance >= 3 * InitR) {
    // 使用指数衰减公式实现平滑过渡
    // return orgRadius * Math.exp(-coefficient * (realDistance - 2 * InitR));
    let radius = orgRadius - coefficient * realDistance * 1.2;
    return Math.max(radius, 0);
  }

  // 如果距离没有超过3个InitR，使用原始公式
  let change = coefficient * realDistance;
  let radius = orgRadius - change;
  return Math.max(radius, 0); // 确保半径不会变成负数
}

// 透镜-距离与位置转换
function LensConvertDistanceChangePosValue(ChangeRVaule, distance) {
  if (distance < InitR) {
    return 0;
  }
  let realDistance = distance - InitR;
  let change =
    realDistance * realDistance * coefficientPosA +
    ChangeRVaule * realDistance * coefficientPosB;
  return change;
}
class Circle {
  constructor(r, x, y, gridX, gridY, name) {
    this.r = r;
    // TODO 写死的偏移
    this.x = x - 352;
    this.name = name;
    this.y = y - 355;
    const color = colorIds[Math.floor(Math.random() * colorIds.length)];
    this.colorFill = color;
    // console.log(this.colorFill, "this.colorFill");
    this.textColor = color === "linearGradient-white" ? "#004DA1" : "#ffffff";
    this.gridX = gridX; // 坐标编号
    this.gridY = gridY; // 坐标编号
  }

  getCenterX() {
    if (this.AmimateX) {
      return this.AmimateX + this.r;
    }
    return this.x + this.r;
  }
  getCenterY() {
    if (this.AmimateY) {
      return this.AmimateY + this.r;
    }
    return this.y + this.r;
  }

  getChangePosResult(ChangeR, centX, centY) {
    let circleCenterX = this.getCenterX();
    let circleCenterY = this.getCenterY();
    let isAddX = true;
    if (circleCenterX > centX) {
      isAddX = false;
    }
    let isAddY = true;
    if (circleCenterY > centY) {
      isAddY = false;
    }
    let changeX = LensConvertDistanceChangePosValue(
      this.r - ChangeR,
      Math.abs(circleCenterX - centX)
    );
    let changeY = LensConvertDistanceChangePosValue(
      this.r - ChangeR,
      Math.abs(circleCenterY - centY)
    );
    if (!isAddX) {
      changeX = -changeX;
    }
    if (!isAddY) {
      changeY = -changeY;
    }
    return [this.x + changeX, this.y + changeY];
  }

  getChangePos(centX, centY) {
    let circleCenterX = this.getCenterX();
    let circleCenterY = this.getCenterY();
    // 计算出距离
    let dx = circleCenterX - centX;
    let dy = circleCenterY - centY;
    let distance = Math.sqrt(dx * dx + dy * dy);
    // 如果焦点在圆内
    if (distance <= this.r) {
      return [this.r, this.x, this.y];
    }
    // 半径变换公式
    let ChangeR = LensConvertDistanceRadius(distance, this.r);
    let resultPos = this.getChangePosResult(ChangeR, centX, centY);
    let changeX = resultPos[0];
    let changeY = resultPos[1];
    let radius = ChangeR;
    // const isOverRight = changeX + radius > +centX;
    // console.log(radius, "  + 250");
    // if (isOverRight) radius = 0;
    // const isOverTop = changeY - radius < -centY;
    // if (isOverTop) radius = 0;
    // const isOverLeft = changeX - radius < -centX;
    // if (isOverLeft) radius = 0;
    // const isBottom = changeY + radius > +centY;
    // if (isBottom) radius = 0;
    return [radius, changeX, changeY];
  }
}
export default class Chart {
  constructor(options) {
    const { el, data, value, callBack } = options;
    const margin = { top: 0, right: 0, bottom: 0, left: 0 };
    // 配置数据
    this.config = {
      margin,
      width: 500,
      height: 500
    };
    this.circlesData = changeArrPos(JSON.parse(JSON.stringify(data)));
    // console.log(this.circlesData);
    selectedCircle = value;
    this.callBack = callBack;
    this.init(el);
  }

  /**
   *  初始化容器
   * @param {*} query  DOM 选择器
   */
  init(query) {
    // 根元素
    const { width, height } = this.config;
    const that = this;
    // 创建画布 svg
    const rootSvg = d3
      .select("#svg")
      .attr("viewBox", `0,0,${width},${height}`)
      .attr("width", remToPx(40))
      .attr("height", remToPx(40));
    const circleGroup = rootSvg.append("g").attr("class", "circleGroup");
    createLiner(rootSvg);
    function InitCircle(gridX, gridY) {
      let x = gridX * InitInterval;
      let y = gridY * InitInterval;
      var circle = new Circle(
        InitR,
        x,
        y,
        gridX,
        gridY,
        that.circlesData.pop()
      );
      return circle;
    }

    // 最大的X Y 坐标 取初始大小
    let maxCircleX = 0;
    let maxCircleY = 0;

    // 创建一个空数组
    var circleList = [];
    // 初始化对象
    for (let n = 0; n < totalNumver; n++) {
      let gridX = n % transverseMax;
      let gridY = Math.floor(n / transverseMax);
      var circle = InitCircle(gridX, gridY);
      circleList.push(circle);
      if (maxCircleX < circle.x) {
        maxCircleX = circle.x;
      }
      if (maxCircleY < circle.y) {
        maxCircleY = circle.y;
      }
    }
    CenterX = 250 + 60;
    CenterY = 250 + 60;
    // console.log(CenterX, CenterY, "CenterX, CenterY");
    // // 更新中心点坐标
    // 错开对象
    circleList.forEach(function (circle, index) {
      // 横向错开
      if (circle.gridX % 2 != 0) {
        return;
      }
      // 错开一半间距
      circle.y = circle.y + InitInterval / 2;
    });

    // 绘制每个网格点
    // 绘制每个网格点
    this.circles = circleGroup
      .selectAll("circle")
      .data(circleList)
      .enter()
      .append("g") // 创建一个组元素来包含圆形和文本
      .each(function (d) {
        const group = d3.select(this);

        // 添加圆形
        group
          .append("circle")
          .attr("cx", (d) => {
            const [radius, x, y] = d.getChangePos(CenterX, CenterY);
            d.cacheRadius = radius; // 存储半径
            d.cacheX = x; // 存储计算后的x坐标
            d.cacheY = y; // 存储计算后的y坐标
            return x;
          })
          .attr("cy", (d) => d.cacheY)
          .attr("r", (d) => 0)
          .attr(
            "stroke",
            d.colorFill.indexOf("white") > 0 ? "#B4B4B4" : "white"
          )
          .attr("stroke-width", (d) => (isInclude(selectedCircle, d) ? 3 : 0))
          .attr("fill", (d) =>
            isInclude(selectedCircle, d)
              ? `url(#${d.colorFill}-select)`
              : `url(#${d.colorFill})`
          );

        const textG = group
          .append("g")
          .attr("x", d.cacheX)
          .attr("y", d.cacheY)
          .attr("style", "user-select: none")
          .attr("transform", `translate(${d.cacheX},${d.cacheY})`);

        textG
          .append("text")
          .attr("x", 0)
          .attr("y", 0)
          .attr("text-anchor", "middle")
          .attr("dominant-baseline", "central")
          .attr("font-size", (d) => 0)
          .attr("fill", (d) =>
            getTextColor(d.colorFill, isInclude(selectedCircle, d))
          )
          .each(function (d) {
            let text = d3.select(this);
            let words = d.name.split(""); // 将文字拆分成单个字符
            // console.log(words, "words");
            let line = [];
            let lineNumber = 0;
            let lineHeight = 1.1; // 行高
            let x = text.attr("x");
            let y = text.attr("y");
            let tspan = text
              .text(null)
              .append("tspan")
              .attr("x", x)
              .attr("y", y);

            words.forEach(function (word, index) {
              line.push(word);
              tspan.text(line.join(""));
              if (line.length === 4 && words.length > 4) {
                // 当需要换行时，调整第一行和后续行的位置
                if (lineNumber === 0) {
                  text.selectAll("tspan").attr("dy", "-0.55em"); // 将第一行向上移动
                }
                line = [];
                tspan = text
                  .append("tspan")
                  .attr("x", x)
                  .attr("y", y)
                  .attr("dy", ++lineNumber * lineHeight - 0.55 + "em")
                  .text(word);
              }
            });
          })
          .attr("dominant-baseline", "central") // 垂直居中
          .attr("opacity", 1); // 初始透明度设置d为0，用于动画效果
      });

    const self = this;
    // // // 圆球点击事件处理器
    // this.circles.on("dblclick", function (event, d) {
    //   self.onCircleClick.call(this, event, d);
    //   console.log("this.circles.on('click");
    //   self.callBack.update(selectedCircle);
    // });

    //     // 定义一个变量来存储上次点击的时间
    // let lastClickTime = 0;
    // const doubleClickThreshold = 300; // 双击的时间间隔阈值，单位为毫秒

    // this.circles.on("click", function (event, d) {
    //     const currentTime = new Date().getTime(); // 获取当前时间的时间戳
    //     if (currentTime - lastClickTime < doubleClickThreshold) {
    //         // 如果两次点击的时间间隔小于阈值，则视为双击
    //         self.onCircleClick.call(this, event, d);
    //         console.log("Double click detected");
    //         self.callBack.update(selectedCircle);
    //     }
    //     lastClickTime = currentTime; // 更新上次点击时间
    // });

    // 定义一个变量来存储定时器
    let pressTimer = null;
    const longPressDuration = 500; // 长按识别的持续时间，单位为毫秒

    this.circles.on("mousedown touchstart", function (event, d) {
      // console.log("mousedown touchstart");
      const element = this;
      // 清除之前的定时器
      if (pressTimer !== null) {
        clearTimeout(pressTimer);
      }
      // 设置一个新的定时器
      pressTimer = setTimeout(function () {
        // 长按事件触发的逻辑
        self.onCircleClick.call(element, event, d);
        // console.log("Long press detected");
        self.callBack.update(selectedCircle);
      }, longPressDuration);
    });
    this.circles.on("mouseup touchend mouseleave touchcancel", function () {
      // 如果用户在长按完成前释放了按键或移动了鼠标，清除定时器
      if (pressTimer !== null) {
        clearTimeout(pressTimer);
        pressTimer = null;
      }
    });

    const zoom = d3
      .zoom()
      .scaleExtent([1, 1]) // 设置缩放范围
      .translateExtent([
        [-600, -600],
        [1100, 1100]
      ]) // 设置平移范围
      .on("zoom", (event) => {
        // 设置平移距离为offset的倍数
        // console.log(event.transform, "event.transform");
        if (pressTimer !== null) {
          clearTimeout(pressTimer);
          pressTimer = null;
        }
        let { x, y, k } = event.transform;
        circleGroup.attr("transform", `translate(${x},${y}) scale(${k})`);
        console.time();
        circleList.forEach(function (circle, index) {
          circle.AmimateX = circle.x + x;
          circle.AmimateY = circle.y + y;
        });

        // 可选：如果需要对每个圆形进行额外的位置和大小调整
        circleGroup.selectAll("g").each(function (d) {
          const group = d3.select(this);
          const [radius, newX, newY] = d.getChangePos(CenterX, CenterY);

          // 更新圆形的半径和位置
          group
            .select("circle")
            .attr("cx", newX)
            .attr("cy", newY) // 直接使用已计算的y坐标
            .attr("r", radius); // 直接使用已存储的半径

          // 更新圆形的半径和位置
          group
            .select("g")
            .attr("x", newX)
            .attr("y", newY)
            .attr("transform", `translate(${newX},${newY})`)
            .select("text")
            .attr("text-anchor", "middle")
            .attr("dominant-baseline", "central")
            .attr("font-size", radius < 20 ? 0 : `${radius / 2.5}px`);
        });

        console.timeEnd();
      });
    // 应用缩放行为到根SVG元素
    rootSvg.call(zoom);
    this.circles
      .selectAll("circle")
      .transition()
      .duration(500) // 动画持续时间为500ms
      .delay((d) => 100) // 使用圆球的name属性来决定延迟
      .attr("r", (d) => {
        return d.cacheRadius;
      })
      .ease(d3.easeCubicInOut); // 使用缓动函数使动画更平滑
    this.circles
      .selectAll("text")
      .transition()
      .duration(500) // 动画持续时间为500ms
      .delay((d) => 100) // 使用圆球的name属性来决定延迟
      .attr("font-size", (d) => {
        return d.cacheRadius < 20 ? 0 : `${d.cacheRadius / 2.5}px`;
      })
      .ease(d3.easeCubicInOut); // 使用缓动函数使动画更平滑
  }

  onCircleClick(event, d) {
    // 如果之前有选中的圆球，只重置之前选中的圆球
    const g = d3.select(this);
    if (!isInclude(selectedCircle, d)) {
      // 更新当前点击的圆球的选中效果，并保存为选中圆球
      // 如果没有选中的圆球

      g.select("circle")
        .attr("fill", `url(#${d.colorFill}-select)`)
        .attr("stroke-width", 3);

      g.select("text").attr("fill", (d) => getTextColor(d.colorFill, true));
      selectedCircle.push(d);
    } else {
      selectedCircle = selectedCircle.filter((item) => item.name !== d.name);
      g.select("circle")
        .attr("fill", `url(#${d.colorFill})`)
        .attr("stroke-width", 0);
      g.select("text").attr("fill", (d) => getTextColor(d.colorFill, false));
    }
  }
}

function isInclude(arr, obj) {
  for (var i = 0; i < arr.length; i++) {
    if (arr[i].name === obj.name) {
      return true;
    }
  }
  return false;
}

function getTextColor(fill, isCheck) {
  // console.log(fill, isCheck, "fill, isCheck");
  if (fill.indexOf("white") > 0) {
    if (isCheck) {
      return "#7E9EC9";
    }
    return "#004DA1";
  }
  return "#ffffff";
}
