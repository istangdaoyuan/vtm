import * as d3 from "d3";
// import getPossiton from "./data";
// import createLiner from "./defs";
// 假设没有圆球被选中
let selectedCircle = [];

// 定义最小和最大半径
export default class Chart {
  constructor(options) {
    const { el, data, value, callBack } = options;
    const margin = { top: 0, right: 0, bottom: 0, left: 0 };
    this.callBack = callBack;
    // start 做全局变量使用
    this.currentItem = null;
    this.lastYear = null;
    this.zoomK = 1;
    // end 做全局变量使用
    // 配置数据
    this.config = {
      margin,
      // 表头高度
      headerLineHeight: 24,
      lineHeight: 20,
      //折线行高
      lineChartHeight: 28,
      // 左侧文字宽度
      leftTextWidth: 25,
      // 格子数量
      rowCount: 25,
      // 总行数
      lineCount: 18,
      //麻醉用药行数
      toplineCount: 10,
      // 折线总行数
      lineChartCount: 11,
      rowWidth: 28,
      preRow: 110,
      afterRow: 90,
      width: 500 - margin.left - margin.right,
      height: 500 - margin.top - margin.bottom,
      // 点图行高
      pointLineHeight: 80
    };
    // 存储D3对象
    this.element = {
      updateLine: []
    };
    // this.circlesData = getPossiton(JSON.parse(JSON.stringify(data)));
    selectedCircle = value;
    this.init(el);
  }

  onCircleClick(event, d) {
    // 如果之前有选中的圆球，只重置之前选中的圆球
    console.log(d, "onCircleClick");
    if (!isInclude(selectedCircle, d)) {
      // 更新当前点击的圆球的选中效果，并保存为选中圆球
      // 如果没有选中的圆球
      d3.select(this).attr("stroke-width", 3);
      selectedCircle.push(d);
    } else {
      selectedCircle = selectedCircle.filter((item) => item.name !== d.name);
      d3.select(this).attr("stroke-width", 0);
    }
  }

  /**
   *  初始化容器
   * @param {*} query  DOM 选择器
   */
  init(query) {
    // 根元素
    const { width, height, margin, preRow, afterRow } = this.config;
    // 创建画布 svg
    const rootSvg = d3
      .select("#svg")
      .attr(
        "viewBox",
        `0,0,${width + margin.right},${height + margin.top + margin.bottom}`
      );
    const circleGroup = rootSvg.append("g").attr("class", "circleGroup");
    // 在SVG元素内创建defs元素

    // createLiner(rootSvg);

    // 定义网格大小和偏移量
    const offset = 100; // 网格元素之间的偏移量
    const gridSizeX = 10; // 网格大小
    const gridSizeY = 10; // 网格大小
    console.log(gridSizeX, gridSizeY, "gridSizeY", width, height);
    const centerPos = [250, 250];
    // 一共99个元素
    // 凸透镜参数
    let lensCenterX = 5; // 凸透镜中心点的 x 坐标
    let lensCenterY = 5; // 凸透镜中心点的 y 坐标

    // 生成网格点数据
    const gridData = generateCoordinates(99);
    console.log(gridData);

    const computedOffsetAmount = (distance) => {
      if (distance === 0) {
        return 0;
      }
      return distance / 0.7;
      // return offset / (distance * 140);
    };
    const computedDistance = (d) => {
      const dx = d.x - lensCenterX;
      const dy = d.y - lensCenterY;
      const distance = Math.sqrt(dx * dx + dy * dy);
      return { distance, dx, dy };
    };

    // 绘制每个网格点
    this.circles = circleGroup
      .selectAll("circle")
      .data(gridData)
      .enter()
      .append("circle")
      .attr("pos", (d) => d.x + "," + d.y)
      .attr("cx", function (d) {
        const { distance, dx } = computedDistance(d);
        const offsetAmount = computedOffsetAmount(distance);
        const value = (d.x + dx * offsetAmount) * offset;
        return value - 250; // 250 视口的一半
      })
      .attr("cy", function (d) {
        const { distance, dy } = computedDistance(d);
        const offsetAmount = computedOffsetAmount(distance);
        const value = (d.y + dy * offsetAmount) * offset;
        return value - 250;
      })
      .attr("r", function (d) {
        const dx = d.x - lensCenterX;
        const dy = d.y - lensCenterY;
        const distance = Math.floor(Math.sqrt(dx * dx + dy * dy));
        const offsetAmount =
          distance === 0 ? 1.5 : offset / (distance * offset * 0.8);
        return (offset / 2) * offsetAmount;
      })
      .attr("fill", "black");

    //   // 画圆球
    //   this.circles = circleGroup
    //     .selectAll('circle')
    //     .data(this.circlesData)
    //     .enter()
    //     .append('circle')
    //     .attr('cx', (d) => d.x)
    //     .attr('cy', (d) => d.y)
    //     .attr('r', 0) // 初始半径设置为0
    //     .attr('stroke', 'white')
    //     .attr('stroke-width', (d) => {
    //       if (isInclude(selectedCircle,d)) {
    //         return 3
    //       }
    //       return 0
    //     })
    //     .attr('fill', (d) => `url(#${d.randomColorIds})`) // 使用线性渐变的ID

    //   // 缩放事件
    const zoom = d3
      .zoom()
      .scaleExtent([0.5, 8]) // 设置缩放范围
      // .translateExtent([
      //   [-100, -100],
      //   [this.config.width + 100, this.config.height + 100],
      // ]) // 设置平移范围
      .on("zoom", (event) => {
        circleGroup.attr("transform", event.transform);
        //   const { x, y } = event.transform;
        //   // 初始原点中心坐标为 250, 250  根据x, y 计算出新的中心坐标
        //   const newCenterX = 250 - x;
        //   const newCenterY = 250 - y;
        //   const xChange = Math.round(x / offset);
        //   const yChange = Math.round(y / offset);
        //   lensCenterX = Math.min(Math.max(0, gridSizeX / 2 - xChange), gridSizeX);
        //   lensCenterY = Math.min(Math.max(0, gridSizeY / 2 - yChange), gridSizeY);
        //   console.log(x, y, lensCenterX, lensCenterY, "=====new data");
        //   // 更新圆球的位置
        //   this.circles
        //     .transition()
        //     .duration(0)
        //     .attr("cx", function (d) {
        //       const dx = d.x - lensCenterX;
        //       const dy = d.y - lensCenterY;
        //       const distance = Math.sqrt(dx * dx + dy * dy);
        //       const offsetAmount = distance === 0 ? 0 : 50 / (distance * 40);
        //       return (d.x + dx * offsetAmount) * offset + offset / 2;
        //     })
        //     .attr("cy", function (d) {
        //       const dx = d.x - lensCenterX;
        //       const dy = d.y - lensCenterY;
        //       const distance = Math.sqrt(dx * dx + dy * dy);
        //       const offsetAmount = distance === 0 ? 0 : 50 / (distance * 40);
        //       return (d.y + dy * offsetAmount) * offset + offset / 2;
        //     })
        //     .attr("r", function (d) {
        //       const dx = d.x - lensCenterX;
        //       const dy = d.y - lensCenterY;
        //       const distance = Math.sqrt(dx * dx + dy * dy);
        //       const offsetAmount = distance === 0 ? 2 : 50 / (distance * 40);
        //       return (offset / 2) * offsetAmount;
        //     })
        //     .ease(d3.easeCubicInOut);
      });

    // 应用缩放行为到根SVG元素
    rootSvg.call(zoom);
    //   // Adding text to each circle
    const texts = circleGroup
      .selectAll("text")
      .data(this.circlesData)
      .enter()
      .append("text")
      .attr("x", (d) => d.x)
      .attr("y", (d) => d.y)
      .attr("text-anchor", "middle") // 水平居中
      .attr("font-size", "36px")
      .attr("fill", (d) => d.color) // 根据小球颜色选择合适的文字颜色
      .each(function (d) {
        let text = d3.select(this);
        let words = d.name.split(""); // 将文字拆分成单个字符
        let line = [];
        let lineNumber = 0;
        let lineHeight = 1.1; // 行高
        let x = text.attr("x");
        let y = text.attr("y");
        let tspan = text.text(null).append("tspan").attr("x", x).attr("y", y);

        words.forEach(function (word, index) {
          line.push(word);
          tspan.text(line.join(""));
          if (line.length === 4 && words.length > 4) {
            // 当需要换行时，调整第一行和后续行的位置
            if (lineNumber === 0) {
              text.selectAll("tspan").attr("dy", "-0.55em"); // 将第一行向上移动
            }
            line = [];
            tspan = text
              .append("tspan")
              .attr("x", x)
              .attr("y", y)
              .attr("dy", ++lineNumber * lineHeight - 0.55 + "em")
              .text(word);
          }
        });
      })
      .attr("dominant-baseline", "central") // 垂直居中
      .attr("opacity", 0); // 初始透明度设置d为0，用于动画效果

    //   // 添加动画效果
    //   texts
    //     .transition()
    //     .duration(1000) // 动画持续时间
    //     .attr('opacity', 1) // 最终透明度

    //   const self = this
    //   // 圆球点击事件处理器
    //   this.circles.on('click', function (event, d) {
    //     self.onCircleClick.call(this, event, d)
    //     console.log("this.circles.on('click");
    //     self.callBack.update(selectedCircle)
    //   })
    //   // 圆球点击事件处理器
    //   texts.on('click', (event, d) => {
    //     // 如果之前有选中的圆球，只重置之前选中的圆球
    //     this.circles
    //       .filter((circleData) => circleData === d)
    //       .each(function (item, d) {
    //         console.log("texts.on('click");
    //         self.onCircleClick.call(this, event, item)
    //         self.callBack.update(selectedCircle)
    //       })
    //   })

    //   // 设置每个层级的动画延迟时间
    //   const delays = [
    //     0,
    //     100, // 第二层延迟200ms
    //     200, // 第三层延迟400ms
    //     300, // 第四层延迟600ms
    //     400, // 第四层延迟600ms
    //     500, // 第四层延迟600ms
    //     600, // 第四层延迟600ms
    //     700, // 第四层延迟600ms
    //     800, // 第四层延迟600ms
    //   ]
    //   this.circles
    //     .transition()
    //     .duration(500) // 动画持续时间为500ms
    //     .delay((d) => delays[d.level]) // 使用圆球的name属性来决定延迟
    //     .attr('r', (d) => {
    //       const distance = Math.sqrt(
    //         Math.pow(d.x - 250, 2) + Math.pow(d.y - 250, 2)
    //       ) // 假设视口中心为(250, 250)
    //       return calculateRadius(d, distance, 1) // 初始k值为1
    //     })
    //     .ease(d3.easeCubicInOut) // 使用缓动函数使动画更平滑
  }
}
function calculateRadius(d, distance, k) {
  // 根据距离确定半径的缩放比例
  return d.r; // 中心最大圆球，半径为原始值
  // if (distance < 60 * k) {
  // } else {
  //   return Math.max(0, d.r * k - distance / 20) // 假设你有一个基于距离的半径调整公式
  // }
}

function isInclude(arr, obj) {
  for (var i = 0; i < arr.length; i++) {
    if (arr[i].name === obj.name) {
      return true;
    }
  }
  return false;
}

function generateCoordinates(N) {
  // 计算每行的元素数量，假设为一个平方根的近似值
  let elementsPerRow = Math.ceil(Math.sqrt(N));

  let coordinates = [];

  for (let i = 0; i < N; i++) {
    // 计算x和y坐标
    let x = Math.floor(i / elementsPerRow);
    let y = i % elementsPerRow;

    // 将坐标添加到数组中
    coordinates.push({ x, y });
  }

  return coordinates;
}
