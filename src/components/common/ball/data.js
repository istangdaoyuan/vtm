let width = 500; // 布局的宽度
let height = 500; // 布局的高度
let radius = 80; // 每个圆的半径
let spacing = 5; // 圆之间的额外间距

export default function getPossiton(data) {
  let center_x = width / 2;
  let center_y = height / 2;

  let positions = [
    {
      x: center_x,
      y: center_y,
      r: radius + 4,
      level: 0,
      name: data.shift(),
      randomColorIds: "linearGradient-white",
      color: "#004DA1"
    }
  ]; // 从中心圆开始
  const colorIds = [
    "linearGradient-white",
    "linearGradient-blue",
    "linearGradient-green",
    "linearGradient-orange",
    "linearGradient-red"
  ];
  // 为每个周围圆的层次计算位置
  for (let level = 1; level <= 7; level++) {
    let levelRadius = (radius + spacing) * level * 2;
    let numCircles = level * 6; // 逐层增加圆的数量
    // console.log(numCircles);
    let angleStep = (Math.PI * 2) / numCircles;
    // 打印 angleStep 的角度
    let radians60 = 30 * (Math.PI / 180);
    for (let i = 0; i < numCircles; i++) {
      let angle = angleStep * i;
      // angle 是否为 60°
      let needAdd = false;

      const distance = needAdd ? levelRadius - 10 * level : levelRadius;
      let x = center_x + distance * Math.cos(angle);
      let y = center_y + distance * Math.sin(angle);
      const name = data.shift();
      if (!name) break;
      const randomColor =
        level == 1
          ? colorIds[Math.max(Math.floor(Math.random() * colorIds.length), 1)]
          : colorIds[Math.floor(Math.random() * colorIds.length)];
      positions.push({
        x: Math.round(x),
        y: Math.round(y),
        r: radius,
        level: level,
        name,
        color: randomColor === "linearGradient-white" ? "#004DA1" : "#ffffff",
        // randomColorIds: randomColor
        randomColorIds: needAdd ? "linearGradient-blue" : "linearGradient-red"
      });
    }
  }
  // console.log(positions);
  return positions;
}
