export default function create(rootSvg) {
  const defs = rootSvg.append("defs");
  createSingle(defs, {
    id: "linearGradient-blue",
    color: ["#2DC1FF", "#1473FF"]
  });
  createSingle(defs, {
    id: "linearGradient-blue-select",
    color: ["#A9E6FF", "#68A2F8"]
  });

  createSingle(defs, {
    id: "linearGradient-white",
    color: ["#ffffff", "#e2f9ff"]
  });
  createSingle(defs, {
    id: "linearGradient-white-select",
    color: ["#E9F4FF", "#e2f9ff"]
  });
  createSingle(defs, {
    id: "linearGradient-green",
    color: ["#9256FF", "#533AE4"]
  });
  createSingle(defs, {
    id: "linearGradient-green-select",
    color: ["#D1B8FF", "#A091F5"]
  });
  createSingle(defs, {
    id: "linearGradient-orange",
    color: ["#FFA838", "#FE8332"]
  });
  createSingle(defs, {
    id: "linearGradient-orange-select",
    color: ["#FFF7F0", "#FFB27F"]
  });
  createSingle(defs, {
    id: "linearGradient-red",
    color: ["#F76B4C", "#FF2760"]
  });
  createSingle(defs, {
    id: "linearGradient-red-select",
    color: ["#FFC3B6", "#FF8FAC"]
  });

  // 在defs中添加filter
  var filter = defs.append("filter").attr("id", "whiteOverlay");

  // 在filter中添加feFlood
  filter
    .append("feFlood")
    .attr("flood-color", "white")
    .attr("flood-opacity", "0.5")
    .attr("result", "whiteOverlay");

  // 在filter中添加feBlend
  filter
    .append("feBlend")
    .attr("in", "SourceGraphic")
    .attr("in2", "whiteOverlay")
    .attr("mode", "normal");
}

function createSingle(
  defs,
  options = {
    id: "linearGradient",
    color: ["#2DC1FF", "#1473FF"]
  }
) {
  // 创建渐变
  const gradientWhite = defs
    .append("linearGradient")
    .attr("id", options.id) // 渐变的ID，用于引用
    .attr("x1", "0%") // 渐变开始的x坐标
    .attr("y1", "0%") // 渐变开始的y坐标，0%代表顶部
    .attr("x2", "0%") // 渐变结束的x坐标
    .attr("y2", "100%"); // 渐变结束的y坐标，100%代表底部

  // 定义渐变从开始到结束的颜色
  gradientWhite
    .append("stop")
    .attr("offset", "0%") // 渐变开始的位置
    .attr("stop-color", options.color[0]); // 白色

  gradientWhite
    .append("stop")
    .attr("offset", "100%") // 渐变结束的位置
    .attr("stop-color", options.color[1]); // 蓝色
}
