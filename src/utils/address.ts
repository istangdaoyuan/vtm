export function extractAddressBeforeDistrict(fullAddress: String) {
  // 定义省、市、区（县）的关键字
  var provinceKeywords = ["省", "自治区", "特别行政区"];
  var cityKeywords = ["市", "自治州", "盟"];
  var districtKeywords = ["区", "县", "市辖区", "市辖县"];

  // 构建正则表达式
  var regex = new RegExp(
    `(.+?)(${provinceKeywords.join("|")})?(.+?)(${cityKeywords.join(
      "|"
    )})?(.+?)(${districtKeywords.join("|")})`
  );

  // 匹配地址
  var match = fullAddress.match(regex);

  // 输出结果
  if (match && match.length >= 7) {
    // 匹配成功，组合省、市、区（县）之前的地址
    var addressBeforeDistrict =
      (match[1] || "") +
      (match[2] || "") +
      (match[3] || "") +
      (match[4] || "") +
      (match[5] || "") +
      (match[6] || "");
    return addressBeforeDistrict.trim();
  } else {
    // 没匹配到
    return "";
  }
}

// 拆分地址
export function splitAddress(fullAddress: string) {
  // 使用正则表达式匹配省市区和详细地址
  const regex =
    /^(.*?省|.*?自治区|.*?特别行政区)?(.*?市|.*?自治州)?(.*?区|.*?县|.*?市辖区)?\s*(.*)$/;
  const matches = fullAddress.match(regex);

  // matches数组中的元素依次是整个匹配、省份、城市、区县、详细地址
  const [, province, city, district, detail] = matches || [];

  // 返回拆分后的地址信息
  return {
    province: province ? province.trim() : "",
    city: city ? city.trim() : "",
    district: district ? district.trim() : "",
    detail: detail ? detail.trim() : ""
  };
}
