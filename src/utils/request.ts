import axios, { AxiosInstance } from "axios";
import { useToast } from "vue-toast-notification";
import "vue-toast-notification/dist/theme-sugar.css";
let service: AxiosInstance;
function useService() {
  if (service) {
    return service;
  }
  const $toast = useToast();

  service = axios.create({
    // 读取配置文件
    // baseURL: "http://49.235.128.65:18086", //测试环境
    baseURL: window.config.backUrl, //测试环境
    timeout: 150000 // 请求超时时间
  });

  //请求拦截

  service.interceptors.request.use(
    (config) => {
      config.headers["Authorization"] =
        "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c2VySWQiOjE3MjM4OTg4NzMyNDk4OTQ0MDF9.g6BCMzatD7nFSM6xgQOHB66KOQhz6l_tIq_G1goQ3p4";
      return config;
    },
    (error) => {
      Promise.reject(error);
    }
  );

  // 响应拦截
  service.interceptors.response.use(
    (response) => {
      const res = response.data;
      if (res.code !== 0) {
        // $toast.open({
        //   message: res.message,
        //   type: "info",
        //   position: "top"
        // });
        return Promise.reject("error");
      } else {
        return response;
      }
    },
    (error) => {
      console.log(error); // for debug
      // $toast.open({
      //   message: "<h1>连接超时,请联系管理员</h1>",
      //   type: "info",
      //   position: "top",
      //   duration: 5 * 1000
      // });
      return Promise.reject(error);
    }
  );
  return service;
}
export default useService;
