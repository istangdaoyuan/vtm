/*
 * @Author: tangdy tdy853839625@qq.com
 * @Date: 2024-07-10 13:40:34
 * @LastEditors: tangdy tdy853839625@qq.com
 * @LastEditTime: 2024-07-10 14:14:39
 * @FilePath: /vtm-electron/src/utils/eventbus.ts
 * @Description: 这是默认设置,请设置`customMade`, 打开koroFileHeader查看配置 进行设置: https://github.com/OBKoro1/koro1FileHeader/wiki/%E9%85%8D%E7%BD%AE
 */
type Listener = (...args: any[]) => void;
type EventMap = { [event: string]: Listener[] };
// backMainPage    => 返回主页
class EventBus {
  private events: EventMap = {};

  $on(event: string, listener: Listener) {
    if (!this.events[event]) {
      this.events[event] = [];
    }
    this.events[event].push(listener);
  }

  $off(event: string, listener?: Listener) {
    if (!this.events[event]) return;
    this.events[event] = this.events[event].filter((l) => l !== listener);
    if (!listener) {
      this.events[event] = [];
    }
  }

  $emit(event: string, ...args: any[]) {
    if (!this.events[event]) return;
    console.log(this.events[event].length, "this.events[event].length", event);
    for (const listener of this.events[event]) {
      listener(...args);
    }
  }
}

export default new EventBus();
