import { useAppStore } from "../store";
import eventbus from "./eventbus";
export default function initHuamn() {
  const store = useAppStore();
  const url = "ws://localhost:8765";
  // 创建 WebSocket 实例
  const socket = new WebSocket(url);
  const send = (msg: string) => {
    socket.send && socket.send(msg);
  };
  // WebSocket 事件处理
  socket.onopen = function (event) {
    console.log("INFO: 人体识别启动", event);
    store.changeHumanServerRun(true);
    // 启动监测
    send(JSON.stringify({ command: "start" }));
    // 获取状态消息
    send(JSON.stringify({ command: "get_status" }));
    // 启动
    // send(JSON.stringify({ command: 'start' }))
    // 关闭
    // send(JSON.stringify({ command: 'stop' }))
    eventbus.$on("stop-human-camera", () => {
      // 无需关闭
      // send(JSON.stringify({ command: "stop" }));
    });
    eventbus.$on("start-human-camera", () => {
      send(JSON.stringify({ command: "start" }));
    });
  };
  let palyNobadyTextTimer: any;
  let nowStatus = false;
  socket.onmessage = function (event) {
    console.log("error: 人体识别数据接受", event.data);
    const data = JSON.parse(event.data);
    if (nowStatus == data.status) return;
    console.log("error: 人体识别数据接受改变====>", event.data);
    nowStatus = data.status;
    if (data.status == "false") {
      store.changeHumanFocus(false);
      palyNobadyTextTimer = setTimeout(() => {
        store.changeHumanBodyUseHumanAnswer(false);
        eventbus.$emit("body-out");
      }, 2000);
    } else if (data.status == "true") {
      clearTimeout(palyNobadyTextTimer);
      store.changeHumanBodyUseHumanAnswer(true);
      eventbus.$emit("body-in");
      console.log(
        "error: 识别到人体为 true 是否在开户流程：",
        store.isInActiveOpenAccount
      );
      store.changeHumanFocus(true);
      // if (!store.isInActiveOpenAccount) {
      // }
    }
  };

  socket.onerror = function (event) {
    console.error("人体识别 错误", event);
    store.changeHumanServerRun(false);
  };

  socket.onclose = function (event) {
    console.log("人体识别关闭", event);
  };

  return {
    send
  };
}
