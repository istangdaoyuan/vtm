// 判断是否是手机号
export function verifyPhoneNumber(value: string) {
  const reg =
    /^((13[0-9])|(14[5-7])|(15[0-3,5-9])|(17[0,3,5-8])|(18[0-9])|166|198|199|(147))\d{8}$/;
  return reg.test(value);
}

// 判断是否是手机号6位验证码
export function verifySmsNumber(value: string) {
  const reg = /^[0-9]{6}$/;
  return reg.test(value);
}

// 根据身份证号返回年龄
export function getAge(idCardNumber: string) {
  // 获取身份证中的出生日期
  const birthdateString = idCardNumber.substring(6, 14);

  // 将出生日期字符串转换为日期对象
  const birthdate = new Date(
    parseInt(birthdateString.substring(0, 4)),
    parseInt(birthdateString.substring(4, 6)) - 1,
    parseInt(birthdateString.substring(6, 8))
  );

  // 获取当前日期
  const currentDate = new Date();

  // 计算年龄
  let age = currentDate.getFullYear() - birthdate.getFullYear();

  // 如果当前日期在生日之前，年龄减一
  if (
    currentDate.getMonth() < birthdate.getMonth() ||
    (currentDate.getMonth() === birthdate.getMonth() &&
      currentDate.getDate() < birthdate.getDate())
  ) {
    age--;
  }

  return age;
}

export function throttle<T extends (...args: any[]) => any, This = any>(
  func: T,
  limit: number
): (this: This, ...funcArgs: Parameters<T>) => void {
  let lastFunc: number;
  let lastRan: number;

  return function (this: This, ...args: Parameters<T>) {
    const context = this; // Now correctly typed as `This`
    if (!lastRan) {
      func.apply(context, args);
      lastRan = Date.now();
    } else {
      clearTimeout(lastFunc);
      lastFunc = window.setTimeout(() => {
        // Use arrow function to keep `this` context
        if (Date.now() - lastRan >= limit) {
          func.apply(context, args);
          lastRan = Date.now();
        }
      }, limit - (Date.now() - lastRan));
    }
  };
}
