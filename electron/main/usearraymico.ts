const net = require("net");
const axios = require("axios");
const querystring = require("querystring");
const uuid = require("uuid");
const crypto = require('crypto');

// 创建一个 TCP 客户端
const client = new net.Socket();
let needWait = false;
let retryCount = 0;

export async function useArrayMico(
  sendDataFn: (data: any) => void,
  config: any
) {
  try {
    // 连接到服务器
    client.connect(12345, config.arrayServer, () => {
      console.log("INFO: ", "Connected to server. 连接上阵列麦克风服务");
      initQidongqi();
    });
  } catch (error) {
    console.log(error);
  }

  // 接收到数据的处理
  client.on("data", async (data: any) => {
    try {
      const serializeData = JSON.parse(data);
      console.log("INFO: ", "整列麦克风原始数据", data);
      if (serializeData.msg) {
        const msgData = JSON.parse(serializeData.msg);
        if (msgData.event === "ASR") {
          sendDataFn(JSON.stringify(msgData.param));
        }
      } else if (serializeData.event === "ReqInitSdk") {
        initQidongqi();
      }
    } catch (error) {
      console.log(error);
    }
  });

  // 连接关闭的处理
  client.on("close", () => {
    console.log("阵列麦克风关闭Connection closed by server.");
  });

  // 错误处理
  client.on("error", (err: { message: string }) => {
    console.error("Error: 整列麦克风错误");
    console.error(err);
    // 重新打开连接
    retryCount++;
    if (retryCount < 5) {
      setTimeout(() => {
        client.connect(12345, config.arrayServer, () => {
          console.log("INFO: ", "Connected to server. 连接上阵列麦克风服务");
          initQidongqi();
        });
      });
    }
  });

  async function initQidongqi() {
    const { token } = await getToken();
    if (needWait) {
      await new Promise((resolve) => setTimeout(resolve, 1000));
    }
    // 发送消息到服务器
    const data = {
      aliyun: {
        aliServerIP: `{
          event: "Init",
          param: {
            aliServerIP: "",
            deviceID: "011013ab",
            deviceInfo: { city: "", manufactor: "", vendor: "" },
            dns: "8.8.8.8",
            gateway: "10.11.11.1",
            ip: "10.11.11.20",
            mask: "255.255.255.0",
            nlsInfo: {
              appkey: "${config.appkey}",
              gateway_inner: false,
              sr_model: "",
              token: "${token}",
              url: "${config.url}"
            },
            options: {
              auto_start_dialog: true,
              enable_auto_wakeup: false,
              enable_face_event: true,
              private_net: false
            },
            ota: { product_key: "", product_secret: "", url: "" }
          }
        }`
      },
      msgProtoId: "2"
    };
    console.log(data);
    client.write(JSON.stringify(data));
    needWait = true;
    setTimeout(() => {
      needWait = false;
    }, 1000);
    console.log("INFO: 发送初始化数据到阵列麦克风", JSON.stringify(data));
  }
  return {
    client
  };
}

async function hmacSHA1(key: string | undefined, text: string | undefined) {
  const encoder = new TextEncoder();
  const encodedKey = encoder.encode(key);
  const encodedText = encoder.encode(text);

  const cryptoKey = await crypto.subtle.importKey(
    "raw",
    encodedKey,
    { name: "HMAC", hash: "SHA-1" },
    false,
    ["sign"]
  );

  const signature = await crypto.subtle.sign("HMAC", cryptoKey, encodedText);
  const signatureArray = Array.from(new Uint8Array(signature));
  return btoa(String.fromCharCode.apply(null, signatureArray));
}

class AccessToken {
  static _encodeText(text: string | number | boolean) {
    return encodeURIComponent(text)
      .replace(/\+/g, "%20")
      .replace(/\*/g, "%2A")
      .replace(/%7E/g, "~");
  }

  static _encodeDict(dic: {
    [x: string]: any;
    AccessKeyId?: any;
    Action?: string;
    Format?: string;
    RegionId?: string;
    SignatureMethod?: string;
    SignatureNonce?: any;
    SignatureVersion?: string;
    Timestamp?: string;
    Version?: string;
  }) {
    const keys = Object.keys(dic).sort();
    const dicSorted: any = {};
    keys.forEach((key) => {
      dicSorted[key] = dic[key];
    });
    let encodedText = querystring.stringify(dicSorted);
    return encodedText
      .replace(/\+/g, "%20")
      .replace(/\*/g, "%2A")
      .replace(/%7E/g, "~");
  }

  static async createToken(accessKeyId: string, accessKeySecret: string) {
    const parameters = {
      AccessKeyId: accessKeyId,
      Action: "CreateToken",
      Format: "JSON",
      RegionId: "cn-shanghai",
      SignatureMethod: "HMAC-SHA1",
      SignatureNonce: uuid.v1(),
      SignatureVersion: "1.0",
      Timestamp: new Date().toISOString().replace(/\..+/, "") + "Z",
      Version: "2019-02-28"
    };

    const queryString = AccessToken._encodeDict(parameters);
    // console.log("规范化的请求字符串: %s", queryString);

    // const stringToSign =
    //   'GET' +
    //   '&' +
    //   AccessToken._encodeText('/') +
    //   '&' +
    //   AccessToken._encodeText(queryString)
    // console.log('待签名的字符串: %s', stringToSign)

    // const secretedString = crypto
    //   .createHmac('sha1', accessKeySecret + '&')
    //   .update(stringToSign)
    //   .digest()
    // let signature = Buffer.from(secretedString).toString('base64')
    // console.log('签名: %s', signature)

    // signature = AccessToken._encodeText(signature)
    // console.log('URL编码后的签名: %s', signature)

    // const fullUrl = `http://nls-meta.cn-shanghai.aliyuncs.com/?Signature=${signature}&${queryString}`
    // console.log('url: %s', fullUrl)
    const stringToSign =
      "GET" +
      "&" +
      AccessToken._encodeText("/") +
      "&" +
      AccessToken._encodeText(queryString);
    // console.log("待签名的字符串: %s", stringToSign);

    const signature = await hmacSHA1(accessKeySecret + "&", stringToSign);
    // console.log("签名: %s", signature);

    const encodedSignature = AccessToken._encodeText(signature);
    // console.log("URL编码后的签名: %s", encodedSignature);

    const fullUrl = `http://nls-meta.cn-shanghai.aliyuncs.com/?Signature=${encodedSignature}&${queryString}`;
    // console.log("url: %s", fullUrl);

    try {
      const response = await axios.get(fullUrl);
      if (response.status === 200) {
        const rootObj = response.data;
        const key = "Token";
        if (rootObj.hasOwnProperty(key)) {
          const token = rootObj[key]["Id"];
          const expireTime = rootObj[key]["ExpireTime"];
          return { token, expireTime };
        }
      }
      // console.log(response.data);
      return { token: null, expireTime: null };
    } catch (error) {
      console.error(error);
      return { token: null, expireTime: null };
    }
  }
}

// 使用示例
async function getToken() {
  const accessKeyId = "LTAI5t7J7hT4vtZdqr9wvSdq";
  const accessKeySecret = "kCDDHUxbF7qKexfkKRwU8oIRJCU1f3";
  const { token, expireTime } = await AccessToken.createToken(
    accessKeyId,
    accessKeySecret
  );
  // console.log("token: %s, expire time(s): %s", token, expireTime);
  if (expireTime) {
    console.log(
      "token有效期的北京时间：%s",
      new Date(expireTime * 1000).toLocaleString("zh-CN", {
        timeZone: "Asia/Shanghai"
      })
    );
  }
  return { token, expireTime };
}
