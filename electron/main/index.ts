import {
  app,
  globalShortcut,
  BrowserWindow,
  shell,
  ipcMain,
  screen
} from "electron";
import { release } from "node:os";
import { join } from "node:path";
import fs from "fs";
import path from "path";
// import { useArrayMico } from "./usearraymico";

// The built directory structure
//
// ├─┬ dist-electron
// │ ├─┬ main
// │ │ └── index.js    > Electron-Main
// │ └─┬ preload
// │   └── index.js    > Preload-Scripts
// ├─┬ dist
// │ └── index.html    > Electron-Renderer
//
process.env.DIST_ELECTRON = join(__dirname, "..");
process.env.DIST = join(process.env.DIST_ELECTRON, "../dist");
process.env.VITE_PUBLIC = process.env.VITE_DEV_SERVER_URL
  ? join(process.env.DIST_ELECTRON, "../public")
  : process.env.DIST;

// Disable GPU Acceleration for Windows 7
if (release().startsWith("6.1")) app.disableHardwareAcceleration();
// Set application name for Windows 10+ notifications
if (process.platform === "win32") app.setAppUserModelId(app.getName());

if (!app.requestSingleInstanceLock()) {
  app.quit();
  process.exit(0);
}

// Remove electron security warnings
// This warning only shows in development mode
// Read more on https://www.electronjs.org/docs/latest/tutorial/security
// process.env['ELECTRON_DISABLE_SECURITY_WARNINGS'] = 'true'

let win: BrowserWindow | null = null;
// Here, you can also use other preload
const preload = join(__dirname, "../preload/index.js");
const url = process.env.VITE_DEV_SERVER_URL;
const indexHtml = join(process.env.DIST, "index.html");

async function createWindow() {
  // 获取主显示器的分辨率
  const { width: screenWidth, height: screenHeight } =
    screen.getPrimaryDisplay().workAreaSize;

  // 应用的原始尺寸
  const originalWidth = 1980;
  const originalHeight = 3520;

  // 计算缩放比例
  const widthRatio = screenWidth / originalWidth;
  const heightRatio = screenHeight / originalHeight;
  const scale = Math.min(widthRatio, heightRatio);

  // 设置新的窗口尺寸
  const newWidth = Math.round(originalWidth * scale);
  const newHeight = Math.round(originalHeight * scale);

  win = new BrowserWindow({
    title: "Main window",
    icon: join(process.env.VITE_PUBLIC, "favicon.ico"),
    frame: false, // 设置为 false 可以隐藏窗口装饰
    fullscreen: process.env.NODE_ENV === "development" ? false : true, // 启动时全屏
    width: newWidth,
    height: newHeight,
    // width: originalWidth,
    // height: originalHeight,
    webPreferences: {
      zoomFactor: 1.0,
      webSecurity: false,
      preload,
      // Warning: Enable nodeIntegration and disable contextIsolation is not secure in production
      // Consider using contextBridge.exposeInMainWorld
      // Read more on https://www.electronjs.org/docs/latest/tutorial/context-isolation
      nodeIntegration: true,
      contextIsolation: false
    }
  });

  if (process.env.VITE_DEV_SERVER_URL) {
    win.loadURL(url);
    // Open devTool if the app is not packaged
    // win.webContents.openDevTools();
  } else {
    const resourceUrl = path.join(process.cwd(), "/resources/config.json"); // process.cwd()即为路径
    let config;
    fs.readFile(resourceUrl, "utf-8", (err, data) => {
      if (err) {
        console.log(err);
      } else {
        config = JSON.parse(data);
        if (config.isDebug) {
          win!.webContents.openDevTools();
        }
      }
    });
    win.loadFile(indexHtml);
  }

  // Test actively push message to the Electron-Renderer
  win.webContents.on("did-finish-load", () => {
    win?.webContents.send("main-process-message", new Date().toLocaleString());
  });

  // Make all links open with the browser, not with the application
  win.webContents.setWindowOpenHandler(({ url }) => {
    if (url.startsWith("https:")) shell.openExternal(url);
    return { action: "deny" };
  });
  // win.webContents.on('will-navigate', (event, url) => { }) #344
}

app.whenReady().then(createWindow);

app.on("window-all-closed", () => {
  win = null;
  if (process.platform !== "darwin") app.quit();
});

app.on("second-instance", () => {
  if (win) {
    // Focus on the main window if the user tried to open another
    if (win.isMinimized()) win.restore();
    win.focus();
  }
});

app.on("activate", () => {
  const allWindows = BrowserWindow.getAllWindows();
  if (allWindows.length) {
    allWindows[0].focus();
  } else {
    createWindow();
  }
});

app.on("ready", () => {
  if (process.env.NODE_ENV === "development") return;
  // 注册全局快捷键Ctrl+R或Cmd+R，阻止页面刷新
  const refreshShortcut =
    process.platform === "darwin" ? "Command+R" : "Ctrl+R";
  globalShortcut.register(refreshShortcut, () => {
    // 这里不执行任何操作，或者执行一些其他逻辑
    console.log("尝试刷新页面，但已被禁用");
  });
});

app.on("will-quit", () => {
  // 注销快捷键
  globalShortcut.unregisterAll();
});
// New window example arg: new windows url
ipcMain.handle("open-win", (_, arg) => {
  const childWindow = new BrowserWindow({
    webPreferences: {
      preload,
      nodeIntegration: true,
      contextIsolation: false
    }
  });

  if (process.env.VITE_DEV_SERVER_URL) {
    childWindow.loadURL(`${url}#${arg}`);
  } else {
    childWindow.loadFile(indexHtml, { hash: arg });
  }
});
// 监听渲染进程的请求
ipcMain.on("getConfig", (event) => {
  // const exePath = app.getPath('exe')
  const resourceUrl = path.join(process.cwd(), "/resources/config.json"); // process.cwd()即为路径

  fs.readFile(resourceUrl, "utf-8", (err, data) => {
    if (err) {
      console.log(err);
      event.reply("configdata", err);
    } else {
      event.reply("configdata", data);
    }
  });
});

ipcMain.on("close-all-click", () => {
  console.log("close-all-click 触发");
  app.exit();
});
