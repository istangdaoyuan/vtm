const fs = require("fs").promises;
const { exec } = require("child_process");

const moveFile = async (source, destination) => {
  try {
    await fs.rename(source, destination);
    console.log(`${source} was moved to ${destination}`);
    // 文件移动完成后执行构建命令
    exec("npm run build:actual", (error, stdout, stderr) => {
      moveFileBack("./public/leisure.mp4", "./release/leisure.mp4");
      if (error) {
        console.error(`exec error: ${error}`);
        return;
      }
      console.log(`stdout: ${stdout}`);
      console.error(`stderr: ${stderr}`);
    });
  } catch (err) {
    console.error(err);
  }
};

const moveFileBack = (source, destination) => {
  fs.rename(source, destination, (err) => {
    if (err) throw err;
    console.log(`${source} was moved back to ${destination}`);
  });
};

moveFile("./release/leisure.mp4", "./public/leisure.mp4");
